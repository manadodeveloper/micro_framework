<?php 
    /**
     * ArrayHelper
     * Berfungsi untuk manipulasi array
     * digunakan untuk core\template
     */
    
    namespace helper;
    trait PhpToJsHelper
    {

        /**
         * Berfungsi generate sweetalert js
         * sweetalert(Array _options)
         * @param (Array)
         * _options =   [
         *                  'type'      => 'success', //success atau error
         *                  'message'   => '',
         *                  'onclose'   => '',
         *              ]
         * @return output string sweetalert js script 
         */
    	public static function sweetalert(
                $_options =     [
                                    "type"      => "success",
                                    "message"   => "",
                                    "onclose"   => "",
                                    
                                ])
    	{
            $sound_success     = BASE_URL .'assets/sound/success.mp3'; 
            $sound_error       = BASE_URL .'assets/sound/error.mp3';


            $message = $_options['message']=="msg.desc" ? $_options['message']: "'".$_options['message']."'" ;
            $sound = $_options['type']=='success' ? $sound_success : $sound_error;
            $timer = isset($_options['timer']) ? "timer   : ".$_options['timer']."," : "";
            $output = 
                "
                                    Swal.fire
                                    ({
                                        type    : '".$_options['type']."',
                                        title   : '',
                                        html    : {$message},
                                        {$timer}
                                        onOpen  : () => 
                                            {
                                                var zippi = new Audio('{$sound}')
                                                zippi.play();
                                            },
                                        onClose : () => 
                                            {
                                                ".$_options['onclose']."
                                            }
                                    });
                ".PHP_EOL;

            return $output;
    	}

        /**
         * Berfungsi generate ajax js
         * ajax(Array _options)
         * @param (Array)
         * _options =   [
         *                  'url'           => 'success', //success atau error
         *                  'data'          => '',
         *                  'debug'         => '',
         *                  'onsuccess'     => '',
         *                  'onerror'       => '',
         *              ]
         * @return output string ajax js script 
         */
        public static function ajax(
            $_options =   
                [
                    'url'           => '', //success atau error
                    'debug'         => false,
                    'onsuccess'     => '',
                    'onerror'       => '',
               ])
        {
            $check = "//";
            $uncheck = "";
            if($_options['debug'])
            {
                $check = "";
                $uncheck = "//";
            }

            $output = "

                    $.ajax
                    ({
                        'type'      : 'POST',
                        'url'       : ".$_options['url'].",
                        'cache'     : false,
                        'data'      : ".$_options['data'].",
                        {$uncheck}'dataType'  : 'json', 
                        'complete'  :
                            function(data,status)
                            {
                                return false;
                            },
                        'success'  :
                            function(msg,status)
                            {
                                {$check}alert(msg);
                                if(msg.status == 'success')
                                {
                                    ".$_options['onsuccess']."
                                }
                                else
                                {
                                    ".$_options['onerror']."
                                }
                            },
                       error:
                            function(msg,textStatus, errorThrown)
                            {
                                Swal.fire({
                                    type: 'error',
                                    title: '',
                                    text: 'terjadi kesalahan: ' + textStatus,
                                    timer: 1200,
                                    onOpen: () => {
                                        var zippi = new Audio('".BASE_URL."assets/sound/success.mp3')
                                        zippi.play();
                                    },
                                    onClose: () => {
                                    }
                                });
                            }
                    }); //end ajax
                ".PHP_EOL;
            return $output;
        }
    }
