<?php 
    /**
     * ArrayHelper
     * Berfungsi untuk manipulasi array
     */
    
    namespace helper;
    trait ArrayHelper
    {

        /**
         * transform(Array _index,Array _data)
         * Berfungsi merubah index data ke index yang dimaksut
         * _index   = [index_sumber => index_tujuan]
         * _data    = _data[index_sumber]
         *
         * @param tidak ada
         * @return output array
         * output = output[index_tujuan]
         */
    	public function transform($_array_index = [],$_array_data = [])
    	{
            $output = [];
            foreach ($_array_index as $key => $value) 
            {
                if(isset($_array_data[$key]))
                {
                    $output[$value] = $_array_data[$key];
                }
            }
            return $output;
    	}

        /**
         * toCSV(String _filename,Array _data)
         * Berfungsi merubah index data ke index yang dimaksut
         * _index   = [index_sumber => index_tujuan]
         * _data    = _data[index_sumber]
         *
         * @param tidak ada
         * @return output array
         * output = output[index_tujuan]
         */
        public function toCSV( $_filename, $_data=[],$_title = [])
        {
               
            ob_clean();
            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename={$_filename}");
            $output = fopen("php://output", "wb");
            if(isset($_title) )
                foreach ($_title as $row)
                    fputcsv($output, $row); // here you can change delimiter/enclosure
            if(isset($_data))
                foreach ($_data as $row)
                    fputcsv($output, $row); // here you can change delimiter/enclosure
            fclose($output);
        }
    }
