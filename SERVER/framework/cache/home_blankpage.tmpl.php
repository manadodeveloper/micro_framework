<?php class_exists('core\Template') or exit; ?>
<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * template
 * Berfungsi sebagai template utama setelah berhasil login
 */


    use core\Auth as Auth;
    use core\Controller ;

    //THEME
    $THEME_BACKGROUND = "bg3";
    $HEADER_BACKGROUND = "blue";
    $NAVBAR_BACKGROUND = "blue2";
    $SIDEBAR_BACKGROUND = "white";

    if(Auth::status())
    {
        if(Auth::user()['theme'] == 'light')
        {
            $THEME_BACKGROUND = "bg3";
            $HEADER_BACKGROUND = "blue";
            $NAVBAR_BACKGROUND = "blue2";
            $SIDEBAR_BACKGROUND = "white";
        }
        elseif(Auth::user()['theme'] == 'dark')
        {   
            $THEME_BACKGROUND = "dark";
            $HEADER_BACKGROUND = "dark2";
            $NAVBAR_BACKGROUND = "dark";
            $SIDEBAR_BACKGROUND = "dark2";
        }
    }
    $controller = new Controller();
    $config_system = $controller->config('system.cfg');

?>
<!DOCTYPE html>
<html lang='en' class='topbar_open'>
    <head>

        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title><?php echo $config_system['name'] ?></title>
        <link rel='shortcut icon' type='image/png' href='<?php echo BASE_URL ?>assets/img/icon.ico' />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/bootstrap.min.css'>
        

        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/apps.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/sweetalert2.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/basar_component.css'>
    </head>
    <body data-background-color='<?= $THEME_BACKGROUND; ?>'>
        <div class='wrapper'>
            <div class='main-header'>
                <!-- begin logo section-->
                <div class='logo-header' data-background-color='<?=$HEADER_BACKGROUND?>'>
                    
                    <a href='<?php echo BASE_URL ?>' class='logo'>
                        <img src='<?php echo BASE_URL ?>assets/img/logo.svg' alt='navbar brand' class='navbar-brand'>
                    </a>
                    <button class='navbar-toggler sidenav-toggler ml-auto' type='button' data-toggle='collapse' data-target='collapse' aria-expanded='false' aria-label='Toggle navigation'>
                        <span class='navbar-toggler-icon'>
                            <i class='icon-menu'></i>
                        </span>
                    </button>
                    <button class='topbar-toggler more'><i class='icon-options-vertical'></i></button>
                    <div class='nav-toggle'>
                        <button class='btn btn-toggle toggle-sidebar'>
                            <i class='icon-menu'></i>
                        </button>
                    </div>
                </div>
                <!-- end logo section-->

                <!-- begin navbar section -->
                <nav class='navbar navbar-header navbar-expand-sm' data-background-color='<?=$NAVBAR_BACKGROUND?>'>
                    
                    <div class='container-fluid'>

                        <!-- begin header section-->
                        <section class='content-header text-white  fw-bold'>
                            <div class='row '>
                                <div class='col-lg-6 col-md-6 col-12'>

                                    <h1>
                                        
                                    </h1>
                                </div>
                                <div class='col-lg-6 col-md-6 col-12'>

                                    <div class='button-action'>
                                        
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- begin header section-->
                    </div>
                </nav>
                <!-- end  navbar section -->
            </div>

            <!-- begin  sidebar section -->
            <div class='sidebar sidebar-style-2' data-background-color='<?=$SIDEBAR_BACKGROUND?>'>           
                <div class='sidebar-wrapper scrollbar scrollbar-inner'>
                    <div class='sidebar-content'>
                        <div class='user'>
                            <div class='avatar-sm float-left mr-2'>
                                <img src='<?php echo BASE_URL.Auth::user()['avatar'] ?>' alt='...' class='avatar-img rounded-circle'>
                            </div>
                            <div class='info'>
                                <a data-toggle='collapse' href='#collapseExample' aria-expanded='true'>
                                    <span>
                                        <?php echo Auth::user()['full_name'] ?>
                                        <span class='user-level'>
                                            <?php echo implode(" , ",Auth::role()) ?>
                                        </span>
                                        <span class='caret'></span>
                                    </span>
                                </a>

                                <div class='clearfix'></div>

                                <div class='collapse in' id='collapseExample'>
                                    <ul class='nav'>
                                        <li>
                                            <a href='<?php echo BASE_URL ?>index.php?page=mode'>
                                                <span class='link-collapse'>Mode Tema</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='<?php echo BASE_URL ?>index.php?page=user&action=ganti_password'>
                                                <span class='link-collapse'>Ganti Password</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- begin menu section  -->
                        <?php echo Auth::menu() ?>
                        <!-- end menu section  -->
                    </div>
                </div>
            </div>
            <!-- end  sidebar section -->
            
            <!-- begin main section -->
            

    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>
                <div class='row'>

                    <div class='col-md-12'>
                        <div class='card'>
                            <div class='card-header'>
                                <div class='card-title'>Card Title</div>
                                <div class='card-category'>Card Category</div>
                            </div>
                            <div class='card-body'>
                                Card Body
                            </div>
                            <div class='card-footer'>
                                <hr>
                                Card Footer
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


            <!-- end main section -->


        </div>

        <!-- begin footer section -->

        

        <!-- end footer section -->


        <!-- begin js section -->

        <!-- Fonts and icons -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/webfont/webfont.min.js'></script>
        <script>
                WebFont.load({
                        google: {'families':['Lato:300,400,700,900']},
                        custom: {'families':['Flaticon', 'Font Awesome 5 Solid', 'Font Awesome 5 Regular', 'Font Awesome 5 Brands', 'simple-line-icons'], urls: ['<?php echo BASE_URL ?>assets/css/fonts.min.css']},
                        active: function() {
                                sessionStorage.fonts = true;
                        }
                });
        </script>

        <!--   Core JS Files   -->
        <script src='<?php echo BASE_URL ?>assets/js/core/jquery.3.2.1.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/core/popper.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/core/bootstrap.min.js'></script>

        <!-- jQuery UI -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'></script>

        <!-- jQuery Scrollbar -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js'></script>

        <!-- Sweet Alert -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/sweetalert/sweetalert2.min.js'></script>

        <!-- Atlantis JS -->
        <script src='<?php echo BASE_URL ?>assets/js/apps.min.js'></script>

        
        

        <!-- end js section -->
    </body>
</html>




