<?php class_exists('core\Template') or exit; ?>
<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * template
 * Berfungsi sebagai template utama setelah berhasil login
 */


    use core\Auth as Auth;
    use core\Controller ;

    //THEME
    $THEME_BACKGROUND = "bg3";
    $HEADER_BACKGROUND = "blue";
    $NAVBAR_BACKGROUND = "blue2";
    $SIDEBAR_BACKGROUND = "white";

    if(Auth::status())
    {
        if(Auth::user()['theme'] == 'light')
        {
            $THEME_BACKGROUND = "bg3";
            $HEADER_BACKGROUND = "blue";
            $NAVBAR_BACKGROUND = "blue2";
            $SIDEBAR_BACKGROUND = "white";
        }
        elseif(Auth::user()['theme'] == 'dark')
        {   
            $THEME_BACKGROUND = "dark";
            $HEADER_BACKGROUND = "dark2";
            $NAVBAR_BACKGROUND = "dark";
            $SIDEBAR_BACKGROUND = "dark2";
        }
    }
    $controller = new Controller();
    $config_system = $controller->config('system.cfg');

?>
<!DOCTYPE html>
<html lang='en' class='topbar_open'>
    <head>

        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title><?php echo $config_system['name'] ?></title>
        <link rel='shortcut icon' type='image/png' href='<?php echo BASE_URL ?>assets/img/icon.ico' />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/bootstrap.min.css'>
        
	<style type="text/css">
		

		.mid-content{
			height: 100%;
			width: 100%;
				margin: 10% 0 0 0;
				text-align: center;
				float: none
		}
		.page-wrapper.toggled .page-content {
		    height: 90%;
		    overflow: hidden;
		}


		.carousel-inner,.carousel,.item,.container,.fill {
			height:100%;
			width:100%;
			background-position:center center;
			background-size:cover;
		}
		.slide-wrapper{
			display:inline;
		}
		.slide-wrapper .container{
			padding:0px;
		}

		/*------------------------------ vertical bootstrap slider----------------------------*/

		.carousel-inner> .item.next ,  .carousel-inner > .item.active.right{ transform: translateY(100%); -webkit-transform: translateY(100%); -ms-transform: translateY(100%);
		-moz-transform: translateY(100%); -o-transform: translateY(100%);  top: 0;left:0;}
		.carousel-inner > .item.prev ,.carousel-inner > .item.active.left{ transform: translateY(-100%); -webkit-transform: translateY(-100%);  -moz-transform: translateY(-100%);
		-ms-transform: translateY(-100%); -o-transform: translateY(-100%); top: 0; left:0;}
		.carousel-inner > .item.next.left , .carousel-inner > .item.prev.right , .carousel-inner > .item.active{transform:translateY(0); -webkit-transform:translateY(0);
		-ms-transform:translateY(0);-moz-transform:translateY(0); -o-transform:translateY(0); top:0; left:0;}

		/*------------------------------- vertical carousel indicators ------------------------------*/
		.carousel-indicators{
		position:absolute;
		top:0;
		bottom:0;
		margin:auto;
		height:20px;
		right:10px; left:auto;
		width:auto;
		}
		.carousel-indicators li{display:block; margin-bottom:5px; border:1px solid #00a199; }
		.carousel-indicators li.active{margin-bottom:5px; background:#00a199;}
		/*-------- Animation slider ------*/

		.animated{
			animation-duration:3s;
			-webkit-animation-duration:3s;
			-moz-animation-duration:3s;
			-ms-animation-duration:3s;
			-o-animation-duration:3s;
			visibility:visible;
			opacity:1;
			transition:all 0.3s ease;
		}
		.carousel-img{   
			 display: inline-block;
				margin: 0 auto;
				width: 100%;
				text-align: center;
			}
		.item img{
			margin:auto;
			visibility:hidden; 
			opacity:0; 
			transition:all 0.3s ease; 
			-webkit-transition:all 0.3s ease; 
			-moz-transition:all 0.3s ease; 
			-ms-transition:all 0.3s ease; 
			-o-transition:all 0.3s ease;
		}
		.item2 .carousel-img img , .item1.active .carousel-img img{
			max-height:300px;
		}
		.item2.active .carousel-img img.animated{visibility:visible; opacity:1; transition:all 1s ease; -webkit-transition:all 1s ease; -moz-transition:all 1s ease; -ms-transition:all 1s ease; -o-transition:all 1s ease;
		animation-duration:2s; -webkit-animation-duration:2s; -moz-animation-duration:2s; -ms-animation-duration:2s; -o-animation-duration:2s; animation-delay:0.3s ; -webkit-animation-delay:0.3s;
		-moz-animation-delay:0.3s;-ms-animation-delay:0.3s; }
		.item .carousel-desc{color:#333; text-align:center;}
		.item  h2{font-size:50px; animation-delay:1.5s;animation-duration:1s; }
		.item  p{animation-delay:2.5s;animation-duration:1s; width:50%; margin:auto;}

		.item3 .carousel-img img , .item2.active .carousel-img img{max-height:300px;}
		.item3.active .carousel-img img.animated{visibility:visible; opacity:1; transition:all 0.3s ease; animation-duration:3s; animation-delay:0.3s}
		.item3 h2 , item2.active h2{visibility:hidden; opacity:0; transition:all 5s ease;}
		.item3.active h2.animated{visibility:visible; opacity:1;  animation-delay:3s;}

		.item .fill{padding:0px 30px; display:table; }
		.item .inner-content{display: table-cell;vertical-align: middle;}
		.item1 .col-md-6{float:none; display:inline-block; vertical-align:middle; width:49%;}



		.item1 .carousel-img img , .item1.active .carousel-img img{
			max-height:500px;
		}
		.item1.active .carousel-img img.animated{visibility:visible; opacity:1; transition:all 0.3s ease; animation-duration:2s; animation-delay:0.3s}
		.item1 h2 , item3.active h2{visibility:hidden; opacity:0; transition:all 5s ease; }
		.item.item1.carousel-desc{text-align:left;}
		.item1.active h2.animated{visibility:visible; opacity:1;  animation-delay:1.5s}
		.item1 p , item3.active p{visibility:hidden; opacity:0; transition:all 5s ease; width:100%;  }
		.item1.active p.animated{visibility:visible; opacity:1;  animation-delay:2.5s;}

		@media(max-width:991px)
		{
			.item .carousel-desc , .item.item1 .carousel-desc{text-align:center;}
			.item .carousel-desc p {width:80%;}
			.item1 .col-md-6{width:100%; text-align:center;}
		}
		@media(max-width:768px)
		{
		.item .carousel-img img, .item.active .carousel-img img{max-height:155px;}
		.item  h2{font-size:30px; margin-top:0px;}
		.item .carousel-desc p{width:100%; font-size:12px;}
		}
		@media(max-width:480px)
		{
		.item  h2{font-size:30px;}
		.item .carousel-desc p{width:100%;}
		}

	</style>


        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/apps.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/sweetalert2.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/basar_component.css'>
    </head>
    <body data-background-color='<?= $THEME_BACKGROUND; ?>'>
        <div class='wrapper'>
            <div class='main-header'>
                <!-- begin logo section-->
                <div class='logo-header' data-background-color='<?=$HEADER_BACKGROUND?>'>
                    
                    <a href='<?php echo BASE_URL ?>' class='logo'>
                        <img src='<?php echo BASE_URL ?>assets/img/logo.svg' alt='navbar brand' class='navbar-brand'>
                    </a>
                    <button class='navbar-toggler sidenav-toggler ml-auto' type='button' data-toggle='collapse' data-target='collapse' aria-expanded='false' aria-label='Toggle navigation'>
                        <span class='navbar-toggler-icon'>
                            <i class='icon-menu'></i>
                        </span>
                    </button>
                    <button class='topbar-toggler more'><i class='icon-options-vertical'></i></button>
                    <div class='nav-toggle'>
                        <button class='btn btn-toggle toggle-sidebar'>
                            <i class='icon-menu'></i>
                        </button>
                    </div>
                </div>
                <!-- end logo section-->

                <!-- begin navbar section -->
                <nav class='navbar navbar-header navbar-expand-sm' data-background-color='<?=$NAVBAR_BACKGROUND?>'>
                    
                    <div class='container-fluid'>

                        <!-- begin header section-->
                        <section class='content-header text-white  fw-bold'>
                            <div class='row '>
                                <div class='col-lg-6 col-md-6 col-12'>

                                    <h1>
                                        
                                    </h1>
                                </div>
                                <div class='col-lg-6 col-md-6 col-12'>

                                    <div class='button-action'>
                                        
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- begin header section-->
                    </div>
                </nav>
                <!-- end  navbar section -->
            </div>

            <!-- begin  sidebar section -->
            <div class='sidebar sidebar-style-2' data-background-color='<?=$SIDEBAR_BACKGROUND?>'>           
                <div class='sidebar-wrapper scrollbar scrollbar-inner'>
                    <div class='sidebar-content'>
                        <div class='user'>
                            <div class='avatar-sm float-left mr-2'>
                                <img src='<?php echo BASE_URL.Auth::user()['avatar'] ?>' alt='...' class='avatar-img rounded-circle'>
                            </div>
                            <div class='info'>
                                <a data-toggle='collapse' href='#collapseExample' aria-expanded='true'>
                                    <span>
                                        <?php echo Auth::user()['full_name'] ?>
                                        <span class='user-level'>
                                            <?php echo implode(" , ",Auth::role()) ?>
                                        </span>
                                        <span class='caret'></span>
                                    </span>
                                </a>

                                <div class='clearfix'></div>

                                <div class='collapse in' id='collapseExample'>
                                    <ul class='nav'>
                                        <li>
                                            <a href='<?php echo BASE_URL ?>index.php?page=mode'>
                                                <span class='link-collapse'>Mode Tema</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='<?php echo BASE_URL ?>index.php?page=user&action=ganti_password'>
                                                <span class='link-collapse'>Ganti Password</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- begin menu section  -->
                        <?php echo Auth::menu() ?>
                        <!-- end menu section  -->
                    </div>
                </div>
            </div>
            <!-- end  sidebar section -->
            
            <!-- begin main section -->
            


    <!-- BEGIN PAGE CONTENT -->
        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>
                        <div id='myCarousel' class='carousel slide' data-ride='carousel'>
                            <div class='carousel-inner'>
                                <div class='carousel-item active'>
                                    <div class='item item1 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-4'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/presenter.png' alt='sofa' class='w-100 img img-responsive' />
                                                        </div>
                                                    </div>
                                                    <div class='col-md-8'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                <h2  mx-auto'>Selamat datang  <?php echo Auth::user()['full_name'] ?></h2>
                                                                <p  mx-auto'>Selamat berkarya, jadikan perkerjaan anda berkah. Kontribusi anda akan sangat bermanfaat bagi perusahaan. Semoga aktifitas kita hari ini dicatat sebagai amal kebaikan. amien</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                                <div class='carousel-item'>

                                    <div class='item item2 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-12'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/app.png' alt='sofa' class='img img-responsive' class='w-100 img img-responsive'/>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-12'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                

                                                                <h2>Simple Usefull Powerfull</h2>
                                                                <p>Sistem ini dibuat sesederhana mungkin yang ditujukan untuk membantu perkerjaan administrasi komputer dan layanan helpdesk. </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                    
                                </div>
                                <div class='carousel-item'>

                                    
                                    <div class='item item3 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-12'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/helpdesk.png' alt='sofa' class='img img-responsive' class='w-100 img img-responsive'/>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-12'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                <h2>Anda Butuh Bantuan?</h2>
                                                                <p>Silahkan menghubungi administrator jika anda mengalami kesulitan dalam mengoperasikan aplikasi sistem ini.</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                    
                                </div>

                                <a class='carousel-control-prev' href='#myCarousel' role='button' data-slide='prev'>
                                    <span class='carousel-control-prev-icon' aria-hidden='true'></span>
                                    <span class='sr-only'>Previous</span>
                                </a>
                                <a class='carousel-control-next' href='#myCarousel' role='button' data-slide='next'>
                                    <span class='carousel-control-next-icon' aria-hidden='true'></span>
                                    <span class='sr-only'>Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  	<!-- END PAGE CONTENT -->


            <!-- end main section -->


        </div>

        <!-- begin footer section -->

        

        <!-- end footer section -->


        <!-- begin js section -->

        <!-- Fonts and icons -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/webfont/webfont.min.js'></script>
        <script>
                WebFont.load({
                        google: {'families':['Lato:300,400,700,900']},
                        custom: {'families':['Flaticon', 'Font Awesome 5 Solid', 'Font Awesome 5 Regular', 'Font Awesome 5 Brands', 'simple-line-icons'], urls: ['<?php echo BASE_URL ?>assets/css/fonts.min.css']},
                        active: function() {
                                sessionStorage.fonts = true;
                        }
                });
        </script>

        <!--   Core JS Files   -->
        <script src='<?php echo BASE_URL ?>assets/js/core/jquery.3.2.1.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/core/popper.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/core/bootstrap.min.js'></script>

        <!-- jQuery UI -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'></script>

        <!-- jQuery Scrollbar -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js'></script>

        <!-- Sweet Alert -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/sweetalert/sweetalert2.min.js'></script>

        <!-- Atlantis JS -->
        <script src='<?php echo BASE_URL ?>assets/js/apps.min.js'></script>

        
        
	<script type="text/javascript">
		
		// scroll slides on mouse scroll 
		$('#myCarousel').bind('mousewheel DOMMouseScroll', function(e){

	        if(e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
	            $(this).carousel('prev');
	        }
	        else{
	            $(this).carousel('next');
	        }
	    });

		//scroll slides on swipe for touch enabled devices 

	 	$("#myCarousel").on("touchstart", function(event){
	        var yClick = event.originalEvent.touches[0].pageY;
	    	$(this).one("touchmove", function(event){
		        var yMove = event.originalEvent.touches[0].pageY;
		        if( Math.floor(yClick - yMove) > 1 ){
		            $(".carousel").carousel('next');
		        }
		        else if( Math.floor(yClick - yMove) < -1 ){
		            $(".carousel").carousel('prev');
		        }
			});
		    $(".carousel").on("touchend", function(){
		            $(this).off("touchmove");
		    });
		});

		//to add  start animation on load for first slide 
		$(function(){
			$.fn.extend({
				animateCss: function (animationName) {
					var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
					this.addClass('animated ' + animationName).one(animationEnd, function() {
						$(this).removeClass(animationName);
					});
				}
			});
			$('.item1.active img').animateCss('slideInDown');
			$('.item1.active h2').animateCss('zoomIn');
			$('.item1.active p').animateCss('fadeIn');
					 
		});

	 
	 	$("#myCarousel").on('slide.bs.carousel', function () {
			$.fn.extend({
				animateCss: function (animationName) {
					var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
					this.addClass('animated ' + animationName).one(animationEnd, function() {
						$(this).removeClass(animationName);
					});
				}
			});
		
	// add animation type  from animate.css on the element which you want to animate

			$('.item1 img').animateCss('slideInDown');
			$('.item1 h2').animateCss('zoomIn');
			$('.item1 p').animateCss('fadeIn');

			
			$('.item2 img').animateCss('zoomIn');
			$('.item2 h2').animateCss('zoomIn');
			$('.item2 p').animateCss('fadeIn');
			
			$('.item3 img').animateCss('fadeInLeft');
			$('.item3 h2').animateCss('fadeInDown');
			$('.item3 p').animateCss('fadeIn');
	    });
	</script>


        <!-- end js section -->
    </body>
</html>










