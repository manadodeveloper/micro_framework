<?php class_exists('core\Template') or exit; ?>
<?php

        
    /**
     * -------------------BEGIN CONFIG-------------------
     */ 

    /**
     * judul halaman
     */ 
    $main_title = "Tambah Wilayah";


    /**
     * URL 
     */ 
    $URL =  [
                "store"    => "/wilayah/create",//alamat memasukan data
                "index"    => "/wilayah",//jika sudah berhasil, maka halaman akan dialihkan
            ];
    $swal_success   =   [
                            "type"      => "success", //success atau error
                            "message"   => "data berhasil dimasukan",//string boleh berupa tag html
                            "onclose"   => "document.location='".$URL['index']."';",//harus diakhiri dengan semicolon ;
                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                        ];
    $swal_error     =   [
                            "type"      => "error", //success atau error
                            "message"   => "msg.desc",
                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                        ];
    $js_ajax        =   [
                            "url"       => "'{$URL['store']}'",//url untuk store data
                            "data"      => '$("#form-create").serialize()',//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert($swal_success),
                            "onerror"   => core\Template::sweetalert($swal_error),
                        ];

    /**
     * masukan configurasi komponen disini 
     */ 
    $controller = new core\Controller;
    $model = $controller->model("WilayahModel");
    $wilayah = $model->pluck();
    $component_wilayah_parent   = [
                                    "id"        => 'wilayah-parent',
                                    "name"      => 'wilayah-parent',
                                    "class"     => ' select2 form-control ',
                                    "prefix"    =>   [""=>"--tanpa induk--"],
                                    "data"      =>   $wilayah,
                                    "selected"  => "",
                                  ];//end array status
    $component_wilayah_id   = [
                                    "name"      => 'wilayah-id',
                                    "class"     => 'form-control',
                              ];
    $component_wilayah_name     = [
                                    "name"      => 'wilayah-nama',
                                    "class"     => 'form-control',
                                  ];

    /**
     * -------------------END CONFIG-------------------
     */ 
?> 
<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * template
 * Berfungsi sebagai template utama setelah berhasil login
 */


    use core\Auth as Auth;
    use core\Controller ;

    //THEME
    $THEME_BACKGROUND = "bg3";
    $HEADER_BACKGROUND = "blue";
    $NAVBAR_BACKGROUND = "blue2";
    $SIDEBAR_BACKGROUND = "white";

    if(Auth::status())
    {
        if(Auth::user()['theme'] == 'light')
        {
            $THEME_BACKGROUND = "bg3";
            $HEADER_BACKGROUND = "blue";
            $NAVBAR_BACKGROUND = "blue2";
            $SIDEBAR_BACKGROUND = "white";
        }
        elseif(Auth::user()['theme'] == 'dark')
        {   
            $THEME_BACKGROUND = "dark";
            $HEADER_BACKGROUND = "dark2";
            $NAVBAR_BACKGROUND = "dark";
            $SIDEBAR_BACKGROUND = "dark2";
        }
    }
    $controller = new Controller();
    $config_system = $controller->config('system.cfg');

?>
<!DOCTYPE html>
<html lang='en' class='topbar_open'>
    <head>

        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title><?php echo $config_system['name'] ?></title>
        <link rel='shortcut icon' type='image/png' href='<?php echo BASE_URL ?>assets/img/icon.ico' />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/bootstrap.min.css'>
        
    <!-- Select2 -->
    <link href="<?php echo BASE_URL ?>assets/js/plugin/select2/css/select2.min.css" rel="stylesheet">


        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/apps.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/sweetalert2.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/basar_component.css'>
    </head>
    <body data-background-color='<?= $THEME_BACKGROUND; ?>'>
        <div class='wrapper'>
            <div class='main-header'>
                <!-- begin logo section-->
                <div class='logo-header' data-background-color='<?=$HEADER_BACKGROUND?>'>
                    
                    <a href='<?php echo BASE_URL ?>' class='logo'>
                        <img src='<?php echo BASE_URL ?>assets/img/logo.svg' alt='navbar brand' class='navbar-brand'>
                    </a>
                    <button class='navbar-toggler sidenav-toggler ml-auto' type='button' data-toggle='collapse' data-target='collapse' aria-expanded='false' aria-label='Toggle navigation'>
                        <span class='navbar-toggler-icon'>
                            <i class='icon-menu'></i>
                        </span>
                    </button>
                    <button class='topbar-toggler more'><i class='icon-options-vertical'></i></button>
                    <div class='nav-toggle'>
                        <button class='btn btn-toggle toggle-sidebar'>
                            <i class='icon-menu'></i>
                        </button>
                    </div>
                </div>
                <!-- end logo section-->

                <!-- begin navbar section -->
                <nav class='navbar navbar-header navbar-expand-sm' data-background-color='<?=$NAVBAR_BACKGROUND?>'>
                    
                    <div class='container-fluid'>

                        <!-- begin header section-->
                        <section class='content-header text-white  fw-bold'>
                            <div class='row '>
                                <div class='col-lg-6 col-md-6 col-12'>

                                    <h1>
                                        
    <?php echo $main_title ?>

                                    </h1>
                                </div>
                                <div class='col-lg-6 col-md-6 col-12'>

                                    <div class='button-action'>
                                        

    <button id='button-back' class='  btn btn-warning  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-caret-left'></i>
        </span>
        Kembali
    </button>
    <button id='button-store' class='btn btn-success  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-plus'></i>
        </span>
        Tambah 
    </button>   

                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- begin header section-->
                    </div>
                </nav>
                <!-- end  navbar section -->
            </div>

            <!-- begin  sidebar section -->
            <div class='sidebar sidebar-style-2' data-background-color='<?=$SIDEBAR_BACKGROUND?>'>           
                <div class='sidebar-wrapper scrollbar scrollbar-inner'>
                    <div class='sidebar-content'>
                        <div class='user'>
                            <div class='avatar-sm float-left mr-2'>
                                <img src='<?php echo BASE_URL.Auth::user()['avatar'] ?>' alt='...' class='avatar-img rounded-circle'>
                            </div>
                            <div class='info'>
                                <a data-toggle='collapse' href='#collapseExample' aria-expanded='true'>
                                    <span>
                                        <?php echo Auth::user()['full_name'] ?>
                                        <span class='user-level'>
                                            <?php echo implode(" , ",Auth::role()) ?>
                                        </span>
                                        <span class='caret'></span>
                                    </span>
                                </a>

                                <div class='clearfix'></div>

                                <div class='collapse in' id='collapseExample'>
                                    <ul class='nav'>
                                        <li>
                                            <a href='<?php echo BASE_URL ?>index.php?page=mode'>
                                                <span class='link-collapse'>Mode Tema</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='<?php echo BASE_URL ?>index.php?page=user&action=ganti_password'>
                                                <span class='link-collapse'>Ganti Password</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- begin menu section  -->
                        <?php echo Auth::menu() ?>
                        <!-- end menu section  -->
                    </div>
                </div>
            </div>
            <!-- end  sidebar section -->
            
            <!-- begin main section -->
            
    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>

                <div class='row'>

                    <div class='col-md-12'>
                        <div class='card bg-warning text-white'>
                            <div class='card-body'>
                                <div >
                                    <h4> Petunjuk Pengisian</h4>
                                    <b>kode wilayah</b> parent merupakan induk dari wilayah yang akan diisi. dibiarkan kosong jika wilayah yang akan diisi merupakan wilayah induk<br />
                                    <b>kode wilayah</b> diisi dengan 6 angka  berdasarkan kode dari masing masing wilayah.<br />
                                    <b>nama wilayah</b> diisi dengan alpanumerik yang merupakan nama dari wilayah tersebut.<br />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class='card'>
                            <div class='card-header'>
                                <div class='card-title'>Form Wilayah Kerja</div>
                            </div>
                            <div class='card-body'>
                                <div >
                                    
                                    <form id='form-create' action='' method='post'><div >
                                        <!-- /.box-header -->
                                        <div class='box-body'>
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                            <label>Wilayah Induk:</label>
                                                        <div class='input-group mb-3'>
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="fa fas fa-home"></i>
                                                                    </span>
                                                            </div>

                                                            <?php echo core\Form::select($component_wilayah_parent) ?>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='col-md-4'>
                                                    <div class="form-group">
                                                        <label>Kode Wilayah:</label>

                                                        <div class='input-group mb-3'>
                                                            <div class='input-group-prepend'>
                                                                <span class='input-group-text' >
                                                                    <i class='fa fas fa-key'></i>
                                                                </span>
                                                            </div>

                                                            <?php echo core\Form::text($component_wilayah_id) ?>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='col-md-8'>
                                                    <div class="form-group">
                                                            <label>Nama Wilayah:</label>
                                                        <div class='input-group mb-3'>
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="fa fas fa-text-width"></i>
                                                                    </span>
                                                            </div>

                                                            <?php echo core\Form::text($component_wilayah_name) ?>

                                                        </div>
                                                    </div>
                                                </div>
                                                

                                                
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


            <!-- end main section -->


        </div>

        <!-- begin footer section -->

        

        <!-- end footer section -->


        <!-- begin js section -->

        <!-- Fonts and icons -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/webfont/webfont.min.js'></script>
        <script>
                WebFont.load({
                        google: {'families':['Lato:300,400,700,900']},
                        custom: {'families':['Flaticon', 'Font Awesome 5 Solid', 'Font Awesome 5 Regular', 'Font Awesome 5 Brands', 'simple-line-icons'], urls: ['<?php echo BASE_URL ?>assets/css/fonts.min.css']},
                        active: function() {
                                sessionStorage.fonts = true;
                        }
                });
        </script>

        <!--   Core JS Files   -->
        <script src='<?php echo BASE_URL ?>assets/js/core/jquery.3.2.1.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/core/popper.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/core/bootstrap.min.js'></script>

        <!-- jQuery UI -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'></script>

        <!-- jQuery Scrollbar -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js'></script>

        <!-- Sweet Alert -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/sweetalert/sweetalert2.min.js'></script>

        <!-- Atlantis JS -->
        <script src='<?php echo BASE_URL ?>assets/js/apps.min.js'></script>

        
        
    <script src="<?php echo BASE_URL ?>assets/js/plugin/select2/js/select2.full.min.js"></script>
    <script >
        $(document).ready(function() 
        {
            if($('.select2').length)
            {
                $('.select2').select2();
            };
            if($('#button-back').length)
            {
                $('#button-back').click(function() 
                {
                    document.location="<?php echo $URL['index'] ?>";
                });
            };
            if($('#button-store').length)
            {
                $('#button-store').click(function() 
                {
                    <?php echo core\Template::ajax($js_ajax) ?>
                });
            };

        });
    </script>


        <!-- end js section -->
    </body>
</html>










