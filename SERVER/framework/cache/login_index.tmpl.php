<?php class_exists('core\Template') or exit; ?>
<!DOCTYPE html>
<html>
	<head>
		
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/bootstrap.min.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/apps.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/sweetalert2.css'>

        <style type="text/css">
			.login-page {
			    width: 100%;  
			    min-height: 100vh;
			    display: -webkit-box;
			    display: -webkit-flex;
			    display: -moz-box;
			    display: -ms-flexbox;
			    display: flex;
			    flex-wrap: wrap;
			    justify-content: center;
			    align-items: center;
			    padding: 15px;
			    background: #9053c7;
			    background: -webkit-linear-gradient(-135deg, #c850c0, #4158d0);
			    background: -o-linear-gradient(-135deg, #c850c0, #4158d0);
			    background: -moz-linear-gradient(-135deg, #c850c0, #4158d0);
			    background: linear-gradient(-135deg, #c850c0, #4158d0);
			}
			.login-box {
			    width: 360px;
			    margin: 7% auto;
			}
			.login-box-body{
			    padding: 20px;
			    border-top: 0;
			    color: #666;
			    border-radius: 5px;
			    background-color: #ffffff;
			    margin-bottom: 30px;
			    -webkit-box-shadow: 2
			}
			.login-logo {
			    font-size: 35px;
			    text-align: center;
			    margin-bottom: 25px;
			    font-weight: 300;
			}
			.login-box-msg {
			    margin: 0;
			    text-align: center;
			    padding: 0 20px 20px 20px;
			}
			.has-feedback {
			    position: relative;
			}
    	</style>

	</head>
	<body class='hold-transition login-page'>

        <!-- /.login-box -->
	    <div class='login-box'>
	        <!-- /.login-box-body -->
	        <div class='login-box-body'>
	            <div class='login-logo'>
	                <b><?php echo $name ?></b>
	            </div>
	            <p class='login-box-msg'>Silahkan login untuk memulai</p>

	            <form action='' method='post' class='login-form'>

	                <div class='alert alert-danger display-hide' style='display:none'>
	                    <button class='close' data-close='alert'></button>
	                    <span  id='info'>
	                    </span>
	                </div>
	                <div class='form-group'>
	                    <div class='input-icon'>
	                        <input class='form-control' placeholder='Email' name='email' type='text'>
	                        <span class='input-icon-addon'>
	                            <i class='fa  fa-envelope'></i>
	                        </span>
	                    </div>
	                </div>
	                <div class='form-group'>
	                    <div class='input-icon'>
	                        <input type='password'  class='form-control' placeholder='Password' name='password' type='text'>
	                        <span class='input-icon-addon show-password'>
	                            <i class='fa fas fa-key'></i>
	                        </span>
	                    </div>
	                </div>
	                
	                <div class='row'>
	                    <div class='col-sm-8'>
	                    </div>
	                    <!-- /.col -->
	                    <div class='col-sm-4'>
	                        <button type='submit' class='btn btn-success btn-block btn-flat'>Sign In</button>
	                    </div>
	                    <!-- /.col -->
	                </div>
	            </form>
	        </div>
	        <!-- /.login-box-body -->
	    </div>
        <!-- /.ogin-box -->
		<!-- Fonts and icons -->
		<script src="<?php echo BASE_URL ?>assets/js/plugin/webfont/webfont.min.js"/></script>

		<script>
			WebFont.load({
			        google: {'families':['Lato:300,400,700,900']},
			        custom: {'families':['Flaticon', 'Font Awesome 5 Solid', 'Font Awesome 5 Regular', 'Font Awesome 5 Brands', 'simple-line-icons'], urls: ['<?php echo BASE_URL ?>assets/css/fonts.min.css']},
			        active: function() {
			                sessionStorage.fonts = true;
			        }
			});
		</script>

		<!--   Core JS Files   -->
		<script src='<?php echo BASE_URL ?>assets/js/core/jquery.3.2.1.min.js'></script>
		<script src='<?php echo BASE_URL ?>assets/js/core/popper.min.js'></script>
		<script src='<?php echo BASE_URL ?>assets/js/core/bootstrap.min.js'></script>

		<!-- jQuery UI -->
		<script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js'></script>
		<script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'></script>

		<!-- jQuery Scrollbar -->
		<script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js'></script>

		<!-- Sweet Alert -->
		<script src='<?php echo BASE_URL ?>assets/js/plugin/sweetalert/sweetalert2.min.js'></script>

        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-validation/js/jquery.validate.js'></script>

		<!-- Atlantis JS -->
		<script src='<?php echo BASE_URL ?>assets/js/apps.min.js'></script>
		<script>
			$(document).ready(function() 
			{
				$('.login-form input').keypress(function(e) 
				{
				    if (e.which == 13)
				    {
				        if ($('.login-form').validate().form()) 
				        {
				            $('.login-form').submit(); //form validation success, call ajax form submit
				        }
				        return false;
				    }
				});



		        $('.login-form').validate({
		            errorElement		: 'span', //default input error message container
		            errorClass			: 'help-block', // default input error message class
		            errorLabelContainer : '#info',
		            focusInvalid		: false, // do not focus the last invalid input
		            rules				: 
			            {
			            },

		            messages			: 
			            {
			            },

		            invalidHandler		: function(event, validator) 
			            { //display error alert on form submit   
			                $('.alert-danger', $('.login-form')).hide();
			            },

		            highlight 			: function(element) 
			            { // hightlight error inputs  

			                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			                $('.alert-danger', $('.login-form')).hide();
			            },

		            success 			: function(label) 
			            {
			                label.closest('.form-group').removeClass('has-error');
			                label.remove();
			                $('.alert-danger', $('.login-form')).hide();
			            },

		            errorPlacement 		: function(error, element) 
			            {
			                error.insertAfter(element.closest('.input-icon'));
			                $('.alert-danger', $('.login-form')).hide();
			            },

		            submitHandler 		: function(form) 
			            {
			                $('.alert-danger').hide();
			                $.ajax({
			                    type: 'POST',
			                    url: "/login",
			                    dataType:"json",
			                    data: $(".login-form").serialize(),
			                    cache:false,
			                    //jika complete maka
			                    complete:
			                    function(data,status)
			                    {
			                        return false;
			                    },
			                    success:
			                        function(msg,status)
			                        {
			                            //alert(msg);
			                            //jika menghasilkan result
			                            if(msg.status == 'success')
			                            {
			                                Swal.fire({
			                                    type: 'success',
			                                    title: '',
			                                    text: msg.desc,
			                                    timer: 1200,
			                                    onOpen: () => {
			                                        var zippi = new Audio('assets/sound/success.mp3')
			                                        zippi.play();
			                                    },
			                                    onClose: () => {
			                                        document.location='/home';
			                                    }
			                                })

			                            }
			                            else
			                            {
			                                Swal.fire
			                                ({
			                                    type: 'error',
			                                    title: '',
			                                    html: msg.desc,
			                                    onOpen: () => {
			                                        var zippi = new Audio('assets/sound/error.mp3')
			                                        zippi.play();
			                                    }
			                                });
			                            }
			                        },
			                   //untuk sementara tidak digunakan
			                   error:
			                        function(msg,textStatus, errorThrown)
			                        {

			                            Swal.fire
			                            ({
			                                type: 'error',
			                                title: '',
			                                text: 'Terjadi error saat proses kirim data',
			                                onOpen: () => 
			                                {
			                                    var zippi = new Audio('assets/sound/error.mp3')
			                                    zippi.play();
			                                }
			                            });
			                        }
			              });//end of ajax
			              return false;
			                //form.submit(); // form validation success, call ajax form submit
			            }
		        });//end validate


			});
		</script>
	</body>
</html>