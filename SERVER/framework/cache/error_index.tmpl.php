<?php class_exists('core\Template') or exit; ?>
<!DOCTYPE html>
<html>
    <head>
        
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/bootstrap.min.css'>
        <link rel='stylesheet' href='<?php echo BASE_URL ?>assets/css/apps.css'>
    </head>
    <body >
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>

                        <div class='col-md-12'>
                            <div class='card'>

                                <h5 class="card-header">Error</h5>
                                <div class='card-header'>
                                    <div class='card-category'>
                                        <?php if(isset($_SESSION['ALERT']['status'])): ?>
                                            <?php echo $_SESSION['ALERT']['desc'] ?>
                                        <?php elseif(isset($data['status'])): ?>
                                            <?php echo $data['desc'] ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <script src="<?php echo BASE_URL ?>assets/js/plugin/webfont/webfont.min.js"/></script>

        <script>
            WebFont.load({
                    google: {'families':['Lato:300,400,700,900']},
                    custom: {'families':['Flaticon', 'Font Awesome 5 Solid', 'Font Awesome 5 Regular', 'Font Awesome 5 Brands', 'simple-line-icons'], urls: ['<?php echo BASE_URL ?>assets/css/fonts.min.css']},
                    active: function() {
                            sessionStorage.fonts = true;
                    }
            });
        </script>

        <!--   Core JS Files   -->
        <script src='<?php echo BASE_URL ?>assets/js/core/jquery.3.2.1.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/core/popper.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/core/bootstrap.min.js'></script>

        <!-- jQuery UI -->
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js'></script>
        <script src='<?php echo BASE_URL ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js'></script>
    </body>
</html>