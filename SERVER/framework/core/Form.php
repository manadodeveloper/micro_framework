<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * Form
 * Berfungsi generate form 
 * 
 */    
    namespace core;
    class Form
    {
        public static function text($_options = [])
        {
            $attributs = "";
            $output = "";
            if(count($_options) > 0)
            {

                foreach ($_options as $key => $value) 
                {
                    $attributs .=  " $key='{$value}' ";
                }
            }
            $output = "<input type='text'  $attributs> ";
            return $output;
        }


		public static function select($_array)
		{
			$default =	[
                            "id"    => 'id',
                            "name"  => 'name',
                            "data"  =>   [
                                            "" => "--select--",
                                        ],
                            "selected" => "",
                        ];//end array status
            $multiple = "";
            $id = "";
            $class = "";
            $data = "";
            if(isset($_array['multiple']))
            {
            	$multiple = " multiple='multiple' ";
            }
            if(isset($_array['id']))
            {
            	$id = " id='{$_array['id']}' ";
            }
            if(isset($_array['name']))
            {
                $name = " name='{$_array['name']}' ";
            }
            if(isset($_array['class']))
            {
            	$class = " class='{$_array['class']}'  ";
            }
            if(isset($_array['prefix']))
            {
                foreach ($_array['prefix'] as $key => $value) 
                {
                    $data .= "<option value='{$key}' >$value</option>";
                }
            }
            foreach ($_array['data'] as $key => $value) 
            {
                $selected = "";
                if(isset($_array['selected']))
                {
                    if(is_array($_array['selected'])) //if array
                    {
                        if(in_array($key, $_array['selected']))
                        {
                            $selected = " selected='selected' ";
                        }
                    }
                    else //if string
                    {
                        if( strval($key) === strval($_array['selected']))
                        {
                            $selected = " selected='selected' ";
                        }
                    }
                }
            	$data .= "<option value='{$key}' {$selected}>$value</option>";
                $selected = "";
            }

            $output = 
                    "	
                        <select {$multiple} {$id} {$name} {$class}  >
							$data
						</select>
                    ";
			return $output;

		}

        public static function datatable($_array)
        {
            $head = "";
            $id= "";
            $name = "";
            $class = "";
            $selection = "";
            $data = "";
            $other_options = "";
            $default =  [
                            "id"    => 'id',
                            "name"  => 'name',
                            "class" => ' table ',
                            "head"  =>  [],//end head
                            "selection"  =>  [],//end selection
                        ];//end array status

            if(isset($_array['id']))
            {
                $id = " id='{$_array['id']}' ";
            }
            if(isset($_array['name']))
            {
                $name = " name='{$_array['name']}' ";
            }
            if(isset($_array['class']))
            {
                $class = " class='{$_array['class']}'  ";
            }
            if(isset($_array['other-options']))
            {
                $other_options = " {$_array['other-options']}  ";
            }
            $head .= "   <thead>
                            <tr> ";
            $selection .= "   <thead>
                            <tr> ";
            if(isset($_array['head']))
            {
                if(count($_array['head']) > 0)
                {
                    for ($i=0; $i < count($_array['head']) ; $i++) 
                    {

                        $style = "";
                        if(isset($_array['head'][$i]['style']))
                        {
                            $style = "style='{$_array['head'][$i]['style']}'";
                        } 
                        $head .= "<th $style>{$_array['head'][$i]['title']}</th>";
                        if(isset($_array['selection'][$i]))
                        {
                            $selection .= "<td >{$_array['selection'][$i]}</td>";
                        }
                    }
                }
            }
            if(isset($_array['data']))
            {
                if(count($_array['data']) > 0)
                {
                    for($i=0;$i<count($_array['data']);$i++)
                    {
                        $data .= "<tr >";
                        foreach ($_array['data'][$i] as $key => $value) 
                        {
                            $data .= "<td > $value </td>";
                        }
                        $data .= "</tr >";

                    }
                }
            }
            $head .= "      </tr>
                        </thead> ";
            $selection .= "      </tr>
                        </thead> ";

            $output =   "     
                            <table {$id} {$name} {$class}  {$other_options}>
                                {$head}
                                {$selection}
                                {$data}
                            </table>
                        ";

            return $output;

        }


    }