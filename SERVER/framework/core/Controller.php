<?php 
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * Controller
 * Berfungsi sebagai induk dari controller lain di app\controllers
 */
    
    namespace core;
    class Controller 
    {

        //variable error
        protected       $errors = [];


        // Seluruh konten / konten akhir. berisi konten utama + template
        private         $output_content;

        /**
         * view(String $_name,Array $_data = [])
         * Berfungsi untuk mengabil konten utama saja tanpa template
         * @param (String nama_file, Array data)
         * nama_file harus huruf kecil semua         
         * @return $this      
         */
        public function view($_name,$data = [])
        {
            $this->output_content = Template::view($_name,$data);
            return $this;
        }


        /**
         * render()
         * Berfungsi menampilkan output konten hasil akhir
         *
         * @param tidak ada
         * @return output konten -> konten akhir
         */
        public function render()
        {
            if(isset($this->output_content))
            {
                echo $this->output_content;
            }
        }


        /**
         * toJSON($_data = [])
         * Berfungsi untuk menulis konten akhir menjadi format json dengan parameter data array
         *
         * @param (Array data)
         * data beupa array   
         * array kemudian diubah menjadi json dan dimasukan kedalam variabel konten akhir   
         *
         * @return $this; 
         */
        public function toJSON($_data = [])
        {
            
            $this->output_content = json_encode($_data);
            return $this;
        }


        /**
         * config(String $_name)
         * Berfungsi untuk mendapatkan array config
         *
         * @param (String nama_config_file)
         * nama_config_file berupa string dan harus huruf kecil semua  
         * isi file berupa array
         *
         * @return Array; 
         */
        public  function config($_name)
        {
            return require(BASE_ROOT . '/app/config/' . strtolower($_name) . '.php');
        }


        /**
         * config(String $_name)
         * Berfungsi mengembalikan object instance
         *
         * @param (String $_name)
         * $_name berupa string dan harus huruf kecil semua  
         *
         * @return Object; 
         */
        public static function model($_name)
        {
            require_once(BASE_ROOT . '/app/models/' . $_name . '.php');
            return new $_name();
        }


        /**
         * redirect($_url)
         * Berfungsi untuk me-redirect halaman
         *
         * @param (URL alamat_url)
         * alamat_url bisa internal (parameter hanya query string) maupun eksternal (lengkap dengan http/https: port dan nama domain) 
         *
         * @return tidak ada; 
         */
        public  function redirect($_url,$_message = [])
        {   
            if(preg_match('/(http|https):\/\/[a-z0-9]+[a-z0-9_\/]*/',$_url))
            {
                header("Location: $_url");
            }
            else
            {
                if(substr($_url, 0,1) == "/")
                {
                    $_url = substr($_url, 1,strlen($_url));
                }
                $_url = str_replace("//","/", $_url);
                if(count($_message) > 0 )
                {
                    $_SESSION['ALERT'] = $_message;
                }
                header("Location:".BASE_URL."$_url");   
            }
        }

        
        /**
         * addError($_error)
         * Berfungsi menambah string error dan dimasukan kedalam array
         *
         * @param (String  keterangan_error)
         * keterangan_error hanya berisi string tidak boleh ada tag html maupun php 
         *
         * @return tidak ada; 
         */
        protected  function addError($_error)
        {
            $_error = strip_tags($_error);
            $this->errors[] = $_error; 
        }


        /**
         * error()
         * Berfungsi mengecek apakah ada error
         *
         * @param tidak ada
         *
         * @return false / array (jika ada error); 
         */
        public function error()
        {
            if(empty($this->errors)) return false;
            return $this->errors;

        }

    }