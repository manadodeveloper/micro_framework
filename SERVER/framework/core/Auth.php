<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * Auth
 * Memastikan bahwa user diautentifikasi dengan menggunakan table database
 */

	namespace core;
	class Auth
	{
		//untuk melihat apakah pengguna sudah login atau belum
		private static $status  = false;

		//table default untuk login autentifikasi user password
		private static $table = 'users';

		// query link untuk database
		public static $db;

		//untuk melihat data user yang sudah login
		private static $user = [];

		//jika ada error
		private static $error = "";

		//user yang sudah login mempunyai role / hak akses apa aja
		private static $roles ;

		public function __construct()
		{
			self::init();
		}
		public static function status()
		{
			if(isset($_SESSION['AUTH']))
			{
				self::$status = true;
			}
			return self::$status;
		}
		public static function logout()
		{
			if(isset($_SESSION['AUTH']))
			{
				session_destroy();
			}	
		}

		public static function attempt($_column = null,$_operator = 'AND')
		{
			$query_string = "";
			$contain_password_attempt = false;
			self::init();

			if($_column == null)
			{
				self::$status = false;
			}
			else
			{
				foreach ($_column as $key => $value) 
				{
					if(strtolower($key) != 'password')
					{
						self::$db =  self::$db->where($key,$value);	
					}
					else
					{
						//$_column['password'] <- diset kecil semua
						$_column[strtolower($key)] =$value;
						$contain_password_attempt = true;
					}
					
				}
				self::$user = self::$db->first()->fetch();
				
				//jika barisnya lebih dari 0
				if(count(self::$user) > 0)
				{
					//cek password 
					if($contain_password_attempt)
					{
						if(password_verify($_column['password'],self::$user[0]['password']))
						{
							//password valid.lanjut pengecekan status
							self::checkUserStatus();
						}
						else
						{
							self::$error = 'Password salah. Harap diketik ulang sesuai dengan password anda.';
							self::$status = false;
						}
					}
					else
					{
						self::checkUserStatus();
					}
				}
				else
				{
					self::$error = 'Kombinasi data login belum benar. Silahkan diulangi';
					self::$status = false;
				}

			}
		}

		public static function menu()
		{
			$html_menu = "";

	        $default_icon  = " fas fa fa-circle ";
			$html_menu_container = "";
			$html_menu_parent = "";
	        $html_menu_child = "";

	        $controller = new Controller();
	        $config_role = $controller->config('role.cfg');
	        $config_role_table =$config_role['table']; 

	        //mencari menu utama

			$data_main_menu = [];
			$data_menu_child = [];

			//yang dibutuhkan dengan conth format :
			// parent 1
			// 		child 1
			// 		child 2
			// 		child 3
			// parent 2
			// 		child 1
			// parent 3
			// 		child 1
			// 		child 2
			// parent 4
			//mendapatkan menu parent

			self::init();
	        
	        $data_main_menu = self::$db
	        						->from($config_role_table['model-has-role'])
	        						->join($config_role_table['role'],"`{$config_role_table['role']}`.`id` =  `{$config_role_table['model-has-role']}`.`role_id`")
	        						->join($config_role_table['role-has-permissions'],"`{$config_role_table['role-has-permissions']}`.`role_id` =  `{$config_role_table['role']}`.`id`")
	        						->join($config_role_table['permissions'],"`{$config_role_table['permissions']}`.`id` =  `{$config_role_table['role-has-permissions']}`.`permission_id`")
	        						->join($config_role_table['menu'],"`{$config_role_table['menu']}`.`menu_id` =  `{$config_role_table['permissions']}`.`menu_id`")
	        						//->where("`{$config_role_table['menu']}`.`menu_id` IS NOT NULL")
	        						->where([
	        								"`{$config_role_table['menu']}`.`menu_parent_id`"=>"0",
	        								"`{$config_role_table['model-has-role']}`.`model_id`"=>$_SESSION['USER']['id'],
	        							])//end where

	        						->fetch();
	        						
			self::$db->reset();
			if(count($data_main_menu)>0)
			{
				for($i=0;$i<count($data_main_menu);$i++)
				{
					$data_menu_child  = self::$db
		        						->from($config_role_table['model-has-role'])
		        						->join($config_role_table['role'],"`{$config_role_table['role']}`.`id` =  `{$config_role_table['model-has-role']}`.`role_id`")
		        						->join($config_role_table['role-has-permissions'],"`{$config_role_table['role-has-permissions']}`.`role_id` =  `{$config_role_table['role']}`.`id`")
		        						->join($config_role_table['permissions'],"`{$config_role_table['permissions']}`.`id` =  `{$config_role_table['role-has-permissions']}`.`permission_id`")
		        						->join($config_role_table['menu'],"`{$config_role_table['menu']}`.`menu_id` =  `{$config_role_table['permissions']}`.`menu_id`")
		        						//->where("`{$config_role_table['menu']}`.`menu_id` IS NOT NULL")
		        						->where([
		        								"`{$config_role_table['menu']}`.`menu_parent_id`"=>$data_main_menu[$i]['menu_id'],
		        								"`{$config_role_table['model-has-role']}`.`model_id`"=>$_SESSION['USER']['id'],
		        							])//end where

		        						->fetch();
					self::$db->reset();
					if(count($data_menu_child)>0)
					{
						$data_main_menu[$i]['child'] = $data_menu_child;
					}
				}
			}

	        return self::menuTemplate($data_main_menu);
		}

        /**
         * hasRole(String role)
         * Berfungsi membandingkan apakah mempunyai hak akses sesuai dengan parameter
         *
         * @param (String role)
         *
         * @return true jika punya, false jika tidak
         */
		public static function hasRole($_role)
		{
			if($_role != "")
			{
				if(in_array($_role, self::role()))
				{
					return true;
				}

			}
			return false;
		}

        /**
         * role()
         * Berfungsi mendapatkan semua hak akses pengguna yang login
         *
         * @param tidak ada  
         *
         * @return array role atau null jika tidak mempunyai hak akses;
         */
		public static function role()
		{
			if(isset(self::$roles))
			{
				return self::$roles;
			}
			else
			{
		        $controller = new Controller();
		        $config_role = $controller->config('role.cfg');
		        $config_role_table =$config_role['table']; 

		        $role = [];
				self::init();
				
				$role = self::$db
								->select("`{$config_role_table['role']}`.`name`")
		        				->from($config_role_table['model-has-role'])
        						->join($config_role_table['role'],"`{$config_role_table['role']}`.`id` =  `{$config_role_table['model-has-role']}`.`role_id`")
        						->where([
        								"`{$config_role_table['model-has-role']}`.`model_id`"=>$_SESSION['USER']['id'],
        							])
        						->fetch();

				self::$db->reset();
				

				if(count($role)>0)
				{
					for($i=0;$i<count($role);$i++)
					{
						self::$roles[] = $role[$i]['name'];

					}
				}
				return self::$roles;
			}
			return null;

		}

		public static function error()
		{
			return self::$error;
		}
		private static function init()
		{
			if(!isset(self::$db))
			{
				self::$db  =  new  DB();
				self::$db = self::$db->from(self::$table);	
			}
		}

		public static function user()
		{
			if(self::status())
			{
				return $_SESSION['USER'];
			}
			else
			{
				return null;
			}
		}
		private static function checkUserStatus()
		{
			if(self::$user[0]['status'] == '0')
			{
				self::$error = 'Akun belum diaktifkan. Harap segera hubungi admin untuk mengaktifkan.';
				self::$status = false;
			}
			elseif(self::$user[0]['status'] == '9')
			{
				self::$error = 'Sementara akun diblokir. Harap segera hubungi admin untuk mengaktifkan kembali.';
				self::$status = false;
			}
			elseif(self::$user[0]['status'] == '1')
			{
				//default photo
				if(self::$user[0]['avatar'] == "")
				{
					 self::$user[0]['avatar'] = "assets/img/no-photo.png";
				}
				$_SESSION['AUTH'] = true;
				$_SESSION['USER'] = self::$user[0];
				self::$status = true;
				self::$error = 'Sementara akun diblokir. Harap segera hubungi admin untuk mengaktifkan kembali.';
			}
		}
		private static function menuTemplate($_menu = [])
		{
        	$default_icon  = " fas fa fa-circle ";
            $html_menu_container = "";
            $html_menu_parent = "";
            $html_menu_child = "";

            if(count($_menu)>0)
            {
                foreach ($_menu as $key => $value)
                {
                    if($_menu[$key]['menu_icon'] == "")
                    {
                        $_menu[$key]['menu_icon']  = $default_icon ;
                    }
                    
                    //jika mempunyai turunan
                    if(isset($_menu[$key]['child']))
                    {
                        $html_menu_parent_link = "<a data-toggle='collapse'  href='#{$_menu[$key]['menu_id']}'>";
                        $carret = "<span class='caret'></span>";   
                        $html_menu_child =    "
                                                    <div class='collapse' id='{$_menu[$key]['menu_id']}'>
                                                        <ul class='nav nav-collapse'>
                                                ";
                        foreach ($_menu[$key]['child'] as $child_key => $child_value)
                        {

                             $html_menu_child .=   "
                                    <li>
                                        <a href='{$_menu[$key]['child'][$child_key]['menu_url']}'>

                                            <span  class='sub-item'>{$_menu[$key]['child'][$child_key]['menu_title']}</span>

                                        </a>
                                    </li>
                            ";

                        }
                        $html_menu_child .=   "
                                                    </ul>
                                                </div>
                                        ";
                    }
                    else
                    {
                        $html_menu_parent_link = "<a href='{$_menu[$key]['menu_url']}'>";
                        $carret="";
                    }
                    
                    $html_menu_container .= "
                                            <li class='nav-item'>
                                                {$html_menu_parent_link}
                                                    <i class='{$_menu[$key]['menu_icon']}'></i>
                                                    <p>{$_menu[$key]['menu_title']}</p>
                                                    $carret
                                                </a>
                                                {$html_menu_child}
                                            </li>
                                    ";  

                }
                $html_menu_container = 	"<ul class='nav nav-primary'>".
            								$html_menu_container.
            								"
			                                <li class='mx-4 mt-2'>
			                                    <a href='/logout' class='btn btn-danger btn-block'><span class='btn-label mr-2'> <i class='fa fa-power-off'></i> </span>Logout</a> 
			                                </li>
			                                ". 
                						"</ul>";
            }
            return $html_menu_container;
		}
	}