<?php
/**
 * TRIMS Framework
 *
 * @author 		basarudin
 * @copyright 	2020
 * 
 * Middleware
 * Berfungsi sebagai event sebelum menajalankan controller
 * disini menjalankan role role terbelih dahulu sebelum menjalankan controller
 * @source 		https://github.com/guliyevravan/mvc
 * 
 */

    namespace core;
    class Middleware 
	{
		private $controller;
		private $response = 	[
									'status'	=>	'error',
									'desc'		=>	'belum dieksekusi'
								];//end response
		//halaman yang akan dialihkan
		private $will_direct = "error";
		private $method_auth = false;

		public function __construct()
		{
			$controller = new Controller();
		}
		
		/**
		 * auth(Boolean $_bool = true)
		 * Berfungsi mengecek apakan request sudah login atau belum
		 *
		 * @param  (Boolean $_bool)
		 * @return Array response
		 */
		public function auth($_bool = true)
		{
			if(Auth::status() == $_bool)
			{
				//logic bisa dilanjutkan ke step selanjutnya / telah lolos untuk layer auth
				$this->response = ['status'=>'success','desc'=>'pengguna sudah login'];
			}
			else
			{
				$this->response = 	[
										'status'=>'error',
										'desc'=>'anda harus login untuk mengakses halaman ini',
									];//end response
				$this->will_direct = "login";
			}
			return $this->response;
		}

		public function hasRole($_roles = [])
		{	
		}


		/**
		 * hasPermission(Array $_permissions = [])
		 * Berfungsi untuk mengecek apakah user mempunyai permission
		 *
		 * @param  (Array Permission)
		 * @return Array response
		 */
		public function hasPermission($_permissions = [])
		{			
			if(Role::hasPermission($_permissions))
			{
				$this->response = 	[
										'status'=>'success',
										'desc'=>'lolos dari dari layer permissions',
									];//end response
			}
			else
			{
				$this->will_direct = "error";
				$this->response = 	[
										'status'=>'error',
										'desc'=>'anda tidak memiliki permissi <b>'.implode(" , ", $_permissions).'</b> dari halaman ini',
									];//end response
			}
			return $this->response;
		}

		/**
		 * setResponse(Array $_permissions = [])
		 * Berfungsi untuk mengecek apakah user mempunyai permission
		 *
		 * @param  (Array $_response)
		 * @return tidak ada
		 */
		public function setResponse($_response = [])
		{
				$this->response = 	$_response;
		}

		/**
		 * hasMethod(Object $_object,String $_method)
		 * Berfungsi untuk mengecek apakah user mempunyai permission
		 *
		 * @param  (Object $_object,String $_method)
		 * @return Boolean
		 */
		public function hasMethod($_object , $_method)
		{
			if(method_exists($_object,$_method))
			{
				$this->response = 	[
										'status'=>'success',
										'desc'=>'object '.get_class($_object)."  mempunyai method ". $_method,
									];//end response
				return true;
			}
			else
			{
				$this->response = 	[
										'status'=>'error',
										'desc'=>'object '.get_class($_object)." tidak  mempunyai method ". $_method,
									];//end response
				return false;
			}
		}
		public function closure()
		{
			if(isset($this->will_direct))
			{
				if(!isset($controller))
				{
					$controller = new Controller();
				}	
				$controller->redirect($this->will_direct,$this->response);
			}
			die;
		}
	}