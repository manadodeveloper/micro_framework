<?php 
/**
 * TRIMS Framework
 *
 * @author 		basarudin
 * @copyright 	2020
 * 
 * Controller
 * Berfungsi sebagai induk dari controller lain di app\controllers
 * @source 		https://github.com/guliyevravan/mvc
 */

	namespace core;
	class Router 
	{
		
		private static $_instance = null; //instance dari class Router
		public static $middleware = [];
		public static $_pagefound ;


		/**
		 * cleanURL($_url = null)
		 * Berfungsi membersihkan query string. termasuk ".." jika ada indikasi untuk naik folder
		 *
		 * @param  (URL url)
		 * @return url
		 */
		private static  function cleanURL($_url = null)
		{
			$_url = rtrim($_url, '/'); // Memnghilangkan / di akhir url
			$_url = str_replace('..', '', $_url); //Menghilangkan tanda .. untuk upfolder
			$_url = filter_var($_url, FILTER_SANITIZE_URL); // filter url yang mengancam
			return $_url;
		}//end method cleanURL

		/**
		 * parseURL()
		 * Berfungsi memisahkan querystring dari domain
		 *
		 * @param  tidak ada
		 * @return String
		 */
		private static function parseURL()
		{
			//  operasi URL 
			$request_uri    = self::cleanURL($_SERVER['REQUEST_URI']);
			$script_name    = $_SERVER['SCRIPT_NAME'];
			$dirname        = dirname($_SERVER['SCRIPT_NAME']);
			
			if($dirname != '/' && $dirname != '\\')
			{
				$request_uri    = substr($request_uri, strlen($dirname));
			}

			return $request_uri;
		}//end method parseURL

		/**  As of PHP 5.3.0  */
		/**
		 * __callStatic($name, $arguments)
		 * __callStatic memanggil method yang belum didefinisikan sebagai static
		 * memanggil get/post method contoh Route::get atau Route::post
		 *
		 * @param  ($_name, $_arguments)
		 * @return String
		 */
		public static function __callStatic($_name, $_arguments)
		{
			// Mendapatkan method name GET/POST
			$method = strtoupper($_name);

			//  mengirimkan method sebagai argumen. dieksekusi di method run
			$_arguments[] = $method;
			call_user_func_array([__CLASS__, 'run'], $_arguments);
		}//end method __callStatic

		/**
		 * __call($name, $arguments)
		 * __callc memanggil method yang belum didefinisikan sebagai instance
		 * memanggil get/post method contoh Route::get atau Route::post
		 *
		 * @param  ($_name, $_arguments)
		 * @return String
		 */
		public function __call( $_name, $_arguments )
		{
			$method = strtoupper($_name);
			return call_user_func_array( [__CLASS__, 'run'], $_arguments );
		}//end method __call


		/**  As of PHP 5.3.0  */
		/**
		 * run($url, $callback, $method = 'GET')
		 * menjalankan method yang dipanggil dari class static Router
		 *
		 *
		 *	Router::get('user/edit/{id}','user@edit')
		 *	Router::post('user/view/{id}','user@view')
		 *
		 *	method otomatis sudah di strupper
		 *
		 * @param  (URL url, Callback callback, Method method)
		 * @return String
		 */
		public static function run($_url, $_callback, $_method = 'GET')
		{            //   jika reuqest method tidak sama dengan method maka tidak dieksekusi
			if($_SERVER['REQUEST_METHOD'] == $_method)
			{

				//  temukan url
				$request_uri = self::parseURL();
				
				//bersihkah  logic url dulu
				$_url = self::cleanURL($_url);
				$explode_url_for_params = explode('/', $_url);


				//mencari parameter
				foreach($explode_url_for_params as $param)
				{ 
					$has_parameter = preg_match("@^{([0-9a-z]+)}$@", $param, $params);
					
					if(!$has_parameter)
					continue;

					//  Url akan menghasilkan /user/edit/([0-9a-zA-Z_]+)
					$_url = str_replace($param, "([0-9a-zA-Z_]+)", $_url);

				}

				//  hilangkan string dibelakang tanda '?'
				$request_uri = explode('?', $request_uri);
				$request_uri = $request_uri[0];
				//  Diger hersey
				if($_url == '*')
				{
					$_url = $request_uri;
				}
				
				//  membandingkan url query string dengan yang ada di url route
				if(preg_match("@^" .$_url. "$@", $request_uri, $parameters))
				{
					self::$_pagefound = true;
					//  Penghapusan parameter pertama
					unset($parameters[0]);

					if(!isset($middleware_instance))
					{
						$middleware_instance = new Middleware();
					}

					//setiap baris route dicegat dengan middleware
					if(count(self::$middleware) > 0)
					{

						foreach (self::$middleware as  $value) 
						{

							foreach ($value as $method => $params) {
								# code...
								# code...
								//periksa apakah ada method key di middleware 
								if(method_exists($middleware_instance, $method))
								{
									//jalankan method, jika tiada ada error maka bernilai true
									//jika ada error maka bernilai false;
									$middleware_response = [];//berupa array 
									$middleware_response = call_user_func([$middleware_instance, $method], $params);

									if($middleware_response['status'] == 'success')
									{
										//lolos midllerware
										//echo "middleware sukses";
										//next 
										//jika sebuah fungsi
										if(is_callable($_callback))
										{
											call_user_func_array($_callback, $parameters);
											die();
											return;
										}
										else
										{
											$object_parse_name = explode("@", $_callback);
					
											//  jika terdapat 2 array yaitu controller dan method
											// jika hanya terdapat controller maka method default nya index
											if(count($object_parse_name) == 2)
											{
												$controller = $object_parse_name[0];
												$_method     = $object_parse_name[1];
											}//end if count
											else
											{
												$controller = $_callback;
												$_method     = 'index';
											}//end else count

											$controller_file = BASE_ROOT . '/app/controllers/' . $controller . '.php';

										}
									}
									else
									{
										
										//mengeksekusi error. meredirect
										$middleware_instance->closure();                                
									}
								}
								else
								{
									//jika middleware tidak mempunyai method
									if(!$middleware_instance->hasMethod($middleware_instance,$method))
									{
										$middleware_instance->closure();   
									}
								}

							}//pecah semua isi middleware baik key nya auth / has-role / has-permission
						}


					}//end count middleware

					//jika lolos middleware / tidak ada lagi middleware

					//  jika ada fungsi
					if(is_callable($_callback))
					{

						call_user_func_array($_callback, $parameters);
						die();
						return;
					}//end if is_callable
					//jika tidak ada fungsi pasti isinya adalah controller beserta method yang dipisahkan oleh tanda @
					//jika tidak ada @ maka method default adalah index

					$object_parse = explode("@", $_callback);
					//  jika terdapat 2 array yaitu controller dan method
					// jika hanya terdapat controller maka method default nya index
					if(count($object_parse) == 2)
					{
						$controller = $object_parse[0];
						$_method     = $object_parse[1];
					}//end if count
					else
					{
						$controller = $_callback;
						$_method     = 'index';
					}//end else count

					//check apakah file controller ada di path?
					$controller_file = BASE_ROOT . '/app/controllers/' . $controller . '.php';
					//  jika file controller ada baru di require
					if(file_exists($controller_file))
					{  
						require_once($controller_file);
						
						//  memastikan tidak ada tanda "/" di url route
						$controller_bol = explode('/', $controller);
						$controller = $controller_bol[count($controller_bol) - 1];


						//jika controller tidak mempunyai method. menggunakan fungsi dari middleware
						if(!$middleware_instance->hasMethod(new $controller,$_method))
						{
							$middleware_instance->closure();   
						}
						else
						{
							
							//next
						}
						//  memanggil method dari controller 
						// disini tugas app/controller dimulai                   
						call_user_func_array([new $controller, $_method], $parameters);

						//tugas router selesai. 
						die();
					}//end file_exists
					else
					{
						$middleware_instance->setResponse	([
																"status"=>"error",
																"desc"=>"file controller <b>$controller</b> tidak ada",
															]);
						$middleware_instance->closure();

					}
					self::resetMiddleware();

				}//end preg_match
				else
				{
					//jika tidak sama dengan url query string dan url array route
					self::resetMiddleware();
					//return;

				}//end else preg_match
			}//end if SERVER['method'] == $_method
			if(count(self::$middleware) > 0)
			{
				self::resetMiddleware();
			}				
		}//end method run


        /**
         * middleware(Array role)
         * Berfungsi mengisi middleware dengan array role
         * $_role = 	[
         *					"auth"				=> "true", //jika true harus login jika false maka guest
         *					"hasRole" 			=>	[
         *												"ADMINITRATOR",
         *												"HELPDESK",
         *												"MANAJER",
         *											],
         *					"hasPermission" 	=>	[
         *												"USER-EDIT",
         *												"USER-DELETE",
         *												"USER-VIEW",
         *											],
         *				];//end array role
         *
         *	Router::middleware($_role)->get/post(...)
         *
         *
         * @param Array role
         * @return instance Router
         */
		public static function middleware($_role = null)
		{           
			if(!empty($_role))
			{
				self::$middleware[] = $_role; 
			}
			if (self::$_instance === null) {
				self::$_instance = new self;
			}
			return self::$_instance;
		}//end method middleware

		/**
		 * resetMiddleware()
		 * mereset properti middleware
		 *
		 * @param  ($_name, $_arguments)
		 * @return this
		 */
		private static function resetMiddleware()
		{
			self::$middleware = []; 
			return;		
		}//end method resetMiddleware


		/**
		 * checkFoundPage()
		 * mengembalikan error halaman tidak ditemukan
		 *
		 * @param  ()
		 * @return tidak ada
		 */
		public static function checkFoundPage()
		{
			if(!isset(self::$_pagefound))
			{
				$middleware_instance = new Middleware();
				$middleware_instance->setResponse	([
																"status"=>"error",
																"desc"=>"Halaman tidak ditemukan",
															]);
				$middleware_instance->closure();
			}
		}
	}