<?php
	namespace core;
	class Role 
	{

		// query link untuk database
		public static $db;
		public static $controller;
		public static $config_role_table;

		private static $roles = [];
		private static $permission = [];
		private static function init()
		{
			self::$db  =  new  DB();
			if(!isset(self::$controller))
			{
	        	self::$controller = new Controller();
			}	        
			$config_role = self::$controller->config('role.cfg');
	        self::$config_role_table =$config_role['table']; 
		}

		private static function fetch()
		{
		}

		public static  function getAuthMenu()
		{
		}

		public static function getAuthRole()
		{
		}

		//return array of role
		public static function getUserRole(String $_user_id)
		{
			$role = [];
			if(!isset(self::$db))
			{
				self::init();
			}
			else
			{
				self::$db->reset();
			}
			$role =  self::$db
        							->from(self::$config_role_table['model-has-role'])
	        						->join(self::$config_role_table['role'],"`".self::$config_role_table['role']."`.`id` =  `".self::$config_role_table['model-has-role']."`.`role_id`")
	        						//->where("`{$config_role_table['menu']}`.`menu_id` IS NOT NULL")
	        						->where([
	        								"`".self::$config_role_table['model-has-role']."`.`model_id`"=>$_user_id,
	        							])//end where

	        						->pluck("id","name");
			return $role;

		}

		public static function getAllRole()
		{

			$permissions = [];

			if(!isset(self::$db))
			{
				self::init();
			}
			else
			{
				self::$db->reset();
			}

			return self::$db->from(self::$config_role_table['role'])->pluck("id","name");
		}

		public static function syncRole($_model_id,$_role = [] )
		{
			if(!isset(self::$db))
			{
				self::init();
			}
			else
			{
				self::$db->reset();
			}
			self::$db->delete(self::$config_role_table['model-has-role'])->where("model_id",$_model_id)->execute();
			$data = [];
			foreach ($_role as $value ) 
			{
				$data = 	[
								"role_id" => $value,
								"model_type" => "app/user",
								"model_id" => $_model_id,
							];
				
				self::$db->insert(self::$config_role_table['model-has-role'],$data);
			}
			return true;
			
		}

		public static function hasRole($_roles = [])
		{
		}

		public static function getAuthPermission()
		{
			$permissions = [];

			if(!isset(self::$db))
			{
				self::init();
			}
			else
			{
				self::$db->reset();
			}


	        $data_permissions = self::$db
        							->from(self::$config_role_table['model-has-role'])
	        						->join(self::$config_role_table['role'],"`".self::$config_role_table['role']."`.`id` =  `".self::$config_role_table['model-has-role']."`.`role_id`")
	        						->join(self::$config_role_table['role-has-permissions'],"`".self::$config_role_table['role-has-permissions']."`.`role_id` =  `".self::$config_role_table['role']."`.`id`")
	        						->join(self::$config_role_table['permissions'],"`".self::$config_role_table['permissions']."`.`id` =  `".self::$config_role_table['role-has-permissions']."`.`permission_id`")
	        						//->where("`{$config_role_table['menu']}`.`menu_id` IS NOT NULL")
	        						->where([
	        								"`".self::$config_role_table['model-has-role']."`.`model_id`"=>$_SESSION['USER']['id'],
	        							])//end where

	        						->fetch();
			self::$db->reset();
			if(count($data_permissions) > 0)
			{
				foreach ($data_permissions as $key => $value) {
					# code...
					if(isset($data_permissions[$key]['name']))
					{
						$permissions[] = $data_permissions[$key]['name'];
					}
					
				}
			}
			//echo "<pre>";
			//print_r($permissions);
			//echo "</pre>";
			//exit();
			return $permissions;
		}

		public static function getRolePermission($_role)
		{

			if(!isset(self::$db))
			{
				self::init();
			}
			else
			{
				self::$db->reset();
			}

	        $data_permissions = self::$db

	        						->from(self::$config_role_table['role-has-permissions'])
        							->join(self::$config_role_table['permissions'],self::$config_role_table['permissions'].".`id` =  ". self::$config_role_table['role-has-permissions'].".`permission_id`")
	        						->join(self::$config_role_table['menu'],self::$config_role_table['menu'].".`menu_id` =  ".self::$config_role_table['permissions'].".`menu_id`")
	        						->where(self::$config_role_table['role-has-permissions'].".`role_id`",$_role)
	        						->fetch();
			return $data_permissions;
		}
		public static function getAllPermission()
		{
		}


		public static function hasPermission($_permissions )
		{
			$return_bool = true;
	        $permissions = is_array($_permissions)
	            ? $_permissions
	            : explode('|', $_permissions);
            if(count($permissions) <= 0)
            {
            	$return_bool = false;
            }
            else
            {
            	foreach ($permissions as $key => $value) {
	            	# code...
	            	if(!in_array($value, self::getAuthPermission()))
	            	{
	            		$return_bool = false;
	            	}
	            }
            }
	            
            return $return_bool;

		}
	}