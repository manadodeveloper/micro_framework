<?php

    /**
     * Validator
     * Berfungsi sebagai validasi input
     * Sumber kode : https://github.com/davidecesarano/Validation/blob/master/readme.md
     */


    /**************************************************************
    Contoh penggunaan :

        $data = [ 
                    'password' => 'pass123456789',
                    'test' => 'http:/a.com',
                ];
        $rules = [
            'username' => ['required', 'min-length' => 6,'maxLen' => 150, 'alpha'],
            'password' => ['required', 'min-length' => 8],
            'email' => [ 'min-length' => 8, 'alpha'],
            'test' => ['date-dmy']
        ];
        //opsional
        $alias = [

            'username' => 'nama login',
            'password' => 'passwordnya ',
            'email' => 'emailnyo',
        ];
        $v = new Validator($rules,$data,$alias);
        if($v->error()){
            print_r($v->error());
        } else{
            echo 'no error';
        }
    ***************************************************************/
    

    namespace core;
    class Validator extends Controller
    {

        //private $errors = [];
        private $rules = [];
        private $alias = [];
        private $requests = [];

        public $patterns = array(
            'date-dmy'          => '[0-9]{1,2}\-[0-9]{1,2}\-[0-9]{4}',
            'date-ymd'          => '[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}'
        );

        public function __construct($_rules = [] ,$_requests = [],$_alias = [])
        {
            $this->rules = $_rules;
            $this->requests = $_requests;
            $this->alias = $_alias;
            $this->validate();
        }


        private function validate()
        {

            foreach($this->rules as $input_name => $rule)
            {

                //jika rule validasinya  required
                if(empty($this->requests[$input_name]) && !isset($this->requests[$input_name]))
                {
                    $this->requests[$input_name] = null;
                    //jika tidak required dan inputnya kosong, agar tidak error waktu validasi role selanjutnya
                }

                if(in_array('required', $rule))
                {
                    if(is_null($this->requests[$input_name]) ||  $this->requests[$input_name] == "")
                        $this->addError($input_name,"<< $input_name >> harus diisi");
                }     

                if(key_exists('confirm', $rule))
                {
                    if($this->requests[$input_name] !=  $this->requests[$rule['confirm']] )
                    {
                        $this->addError($input_name, "<< $input_name >> tidak sama dengan    {$rule['confirm']} " );
                    }
                } 

                if(key_exists('min-length', $rule))
                {
                    if(strlen($this->requests[$input_name]) < $rule['min-length'])
                    {
                        $this->addError($input_name, "<< $input_name >> minimal  {$rule['min-length']}  karakter" );
                    }
                } 
                if(key_exists('max-length', $rule))
                {
                    if(strlen($this->requests[$input_name]) > $rule['max-length'])
                    {
                        $this->addError($input_name, "<< $input_name >> maksimal  {$rule['max-length']}  karakter" );
                    }
                }
                if(key_exists('min', $rule))
                {
                    if($this->requests[$input_name] < $rule['min'])
                    {
                        $this->addError($input_name, "<< $input_name >> minimal  {$rule['min']}  " );
                    }
                }
                if(key_exists('max', $rule))
                {
                    if($this->requests[$input_name] > $rule['max'])
                    {
                        $this->addError($input_name, "<< $input_name >> maksimal  {$rule['max']} " );
                    }
                }

                if(in_array('string', $rule))
                {
                    //echo " saya".$this->requests[$input_name];exit();
                    if(filter_var($this->requests[$input_name], FILTER_SANITIZE_STRING) != $this->requests[$input_name]) 
                    {

                        $this->addError($input_name, "<< $input_name >> harus berupa string dan bebas dari  tag html " );
                    }
                }

                if(in_array('alphanumeric', $rule))
                {
                    if(!preg_match("/^[a-zA-Z]+[a-zA-Z0-9._]+$/", $this->requests[$input_name])) 
                    {
                        $this->addError($input_name, "<< $input_name >> harus berupa angka / huruf. tidak boleh ada spasi." );
                    }
                }
                //harus berupa  kata huruf a-z, A-Z
                if(in_array('alpha', $rule))
                {
                    if(!preg_match("/^[a-zA-Z]+$/", $this->requests[$input_name])) 
                    {
                        $this->addError($input_name, "<< $input_name >> harus berupa kata [a-z] atau [A-Z]" );
                    }
                }
                if(in_array('integer', $rule))
                {
                    if(!filter_var($this->requests[$input_name], FILTER_VALIDATE_INT) === 0 || filter_var($this->requests[$input_name], FILTER_VALIDATE_INT) === false)
                    {

                        $this->addError($input_name, "<< $input_name >> harus berupa integer " );
                    }
                }

                if(in_array('float', $rule))
                {
                    if(!filter_var($this->requests[$input_name], FILTER_VALIDATE_FLOAT))
                    {
                        $this->addError($input_name, "<< $input_name >> harus berupa float " );

                    }
                }
                //bisa ip4 ataupun ip 6
                if(in_array('ip', $rule))
                {
                    if(filter_var($this->requests[$input_name], FILTER_VALIDATE_IP) === false) 
                    {

                        $this->addError($input_name, "<< $input_name >> harus berupa angka " );
                    }
                }
                if(in_array('url', $rule))
                {
                    if(filter_var($this->requests[$input_name], FILTER_VALIDATE_URL) === false) 
                    {

                        $this->addError($input_name, "<< $input_name >> harus berupa URL " );
                    }
                }
                if(in_array('boolean', $rule))
                {
                    if(!is_bool(filter_var($this->requests[$input_name], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) )
                    {

                        $this->addError($input_name, "<< $input_name >> harus berupa boolean " );
                    }
                }
                if(in_array('email', $rule))
                {
                    //echo " saya".$this->requests[$input_name];exit();
                    if(filter_var($this->requests[$input_name], FILTER_VALIDATE_EMAIL) === false) 
                    {
                        $this->addError($input_name, "<< $input_name >> harus berformat email " );
                    }
                }

                if(in_array('date-ymd', $rule))
                {
                    $regex = '/^('.$this->patterns['date-ymd'].')$/u';
                    if(!preg_match($regex , $this->requests[$input_name])) 
                    {
                        $this->addError($input_name, "<< $input_name >> harus berupa tanggal berformat  tahun[4]-bulan[2]-tanggal[2]" );
                    }
                }
                if(in_array('date-dmy', $rule))
                {
                    $regex = '/^('.$this->patterns['date-dmy'].')$/u';
                    if(!preg_match($regex , $this->requests[$input_name])) 
                    {
                        $this->addError($input_name, "<< $input_name >> harus berupa tanggal berformat  tanggal[2]-bulan[2]-tahun[4]" );
                    }
                }


            }

            
        }
        protected function addError($_request_name = '', $_error = ''){
            if(isset($this->alias[$_request_name]))
            {
                $_error = str_replace("<< $_request_name >>", $this->alias[$_request_name], $_error);
            }
            
            $_error = strip_tags($_error);
            $this->errors[] = $_error;
        }


    }
