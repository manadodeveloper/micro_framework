<?php

/**
 * TRIMS Framework
 *
 * @author 		basarudin
 * @copyright 	2020
 *
 *
 * Berfungsi untuk mendapatkan variable BASE_ROOT dan BASE_URL yang akan digunakan untuk global
 * @param 		tidak ada
 * @return 		BASE_ROOT dan BASE_URL
 */


	$filename 	= '/index.php';


	$ssl      	= ( ! empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' );
	$sp       	= strtolower( $_SERVER['SERVER_PROTOCOL'] );
	$protocol 	= substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
	$port     	= $_SERVER['SERVER_PORT'];
	$port     	= ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
	$host     	= ( true && isset( $_SERVER['HTTP_X_FORWARDED_HOST'] ) ) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : ( isset( $_SERVER['HTTP_HOST'] ) ? $_SERVER['HTTP_HOST'] : null );
	$host     	= isset( $host ) ? $host : $_SERVER['SERVER_NAME'] . $port;


	//digunakan untuk include
	$BASE_ROOT 	=  str_replace('/framework/init/server.php', '', __FILE__);
	//digunakan untuk file static seperti JS / CSS
	$BASE_URL 	= $protocol . '://' . $host. str_replace($filename, '/', $_SERVER['SCRIPT_NAME']);

	
	define('BASE_URL',$BASE_URL);
	define('BASE_ROOT',$BASE_ROOT); 
