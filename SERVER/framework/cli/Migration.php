<?php

	namespace cli;
    use core\DB;
	class Migration extends BaseCli
	{
        private    static $db;

		public function index()
		{

			echo "Tidak ada parameter:\n";
			echo "------------------------------------------------\n";
			echo "================================================\n";
		}

		//return array query
		private function getContent($_base_path)
		{

			//sebagai output
			$query = [];
			$files = array_diff(scandir($_base_path), array('.', '..'));

			if(count($files)>0)
			{
				foreach ($files as $key => $value) 
				{
					// Temporary variable, used to store current query
					$templine = '';
					// Read in entire file
					$lines = file($_base_path.$value);
					// Loop through each line
					foreach ($lines as $line)
					{
						// Skip it if it's a comment
						if (substr($line, 0, 2) == '--' || $line == '')
						    continue;

						// Add this line to the current segment
						$templine .= $line;
						// If it has a semicolon at the end, it's the end of the query
						if (substr(trim($line), -1, 1) == ';')
						{
						    // Perform the query
						    //mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
						    // Reset temp variable to empty
						    $query[] = $templine;
						    $templine = '';
						}
					}

				}


			}

			return $query;

		}

		public function structure()
		{

			$blueprint_path =  self::config('blueprint.cfg');
			//diganti
			$structure_path = $blueprint_path['TABLE']['table-create'];


		
			echo "\n";
			echo "*> membuat structure:\n";
			echo "------------------------------------------------\n";
			$query = self::getContent($structure_path);
			if(count($query) > 0)
			{
				$db = new DB();
				if($db->trans_query($query))
				{
					echo  self::green("pembuatan struktur berhasil ") . "\n";	
					echo self::green(implode(" \n", array_diff(scandir($structure_path), array('.', '..'))) ."\n");
				}
				else
				{
					echo self::red(implode("\n", $db->error())."\n");
					echo  "\n".self::yellow(" pembuatan struktur di rollback ") . "\n";	
				}

			}
			else
			{
				echo  self::green(' tidak ada file struktur') . "\n";
			}
			echo "\n";
		}

		public function drop()
		{
			$blueprint_path =  self::config('blueprint.cfg');
			//diganti
			$structure_path =  $blueprint_path['TABLE']['table-drop'];
		
			echo "\n";
			echo "*> menghapus struktur:\n";
			echo "------------------------------------------------\n";
			$query = self::getContent($structure_path);
			if(count($query) > 0)
			{
				$db = new DB();
				if($db->trans_query($query))
				{
					echo  self::green(" menghapus berhasil ") . "\n";	
					echo self::green(implode(" \n", array_diff(scandir($structure_path), array('.', '..'))) ."\n");
				}
				else
				{
					echo self::red(implode("\n", $db->error())."\n");
					echo  "\n".self::yellow(" query di rollback ") . "\n";	
				}

			}
			else
			{
				echo  self::green(' tidak ada file drop struktur') . "\n";
			}
			echo "\n";
		}



		public function insert()
		{
			$blueprint_path =  self::config('blueprint.cfg');
			//diganti
			$structure_path =  $blueprint_path['TABLE']['table-insert'];
		
			echo "\n";
			echo "*> memasukan data:\n";
			echo "------------------------------------------------\n";
			$query = self::getContent($structure_path);
			if(count($query) > 0)
			{
				$db = new DB();
				if($db->trans_query($query))
				{
					echo  self::green(" menambah data berhasil ") . "\n";	
					echo self::green(implode(" \n", array_diff(scandir($structure_path), array('.', '..'))) ."\n");
				}
				else
				{
					echo self::red(implode("\n", $db->error())."\n");
					echo  "\n".self::yellow(" query di rollback ") . "\n";	
				}

			}
			else
			{
				echo  self::green(' tidak ada file sql') . "\n";
			}
			echo "\n";
		}

		public function empty()
		{
			$blueprint_path =  self::config('blueprint.cfg');
			//diganti
			$structure_path =  $blueprint_path['TABLE']['table-empty'];
		
			echo "\n";
			echo "*> mengosongkan data:\n";
			echo "------------------------------------------------\n";
			$query = self::getContent($structure_path);
			if(count($query) > 0)
			{
				$db = new DB();
				if($db->trans_query($query))
				{
					echo  self::green("mengosongkan data berhasil ") . "\n";	
					echo self::green(implode(" \n", array_diff(scandir($structure_path), array('.', '..'))) ."\n");
				}
				else
				{
					echo self::red(implode("\n", $db->error())."\n");
					echo  "\n".self::yellow(" query di rollback ") . "\n";	
				}

			}
			else
			{
				echo  self::green(' tidak ada file sql') . "\n";
			}
			echo "\n";
		}

		public function reset()
		{
			self::empty();
			self::drop();

			self::structure();
			self::insert();
		}

	}