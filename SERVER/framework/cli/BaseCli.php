<?php

	namespace cli;
	use core\Controller;

	class BaseCli  extends Controller
	{

		public static $output = "";
		public function red($message) {
			// return $message;
			return "\033[0;31m$message\033[0m";
		}

		public function green($message) {
			// return $message;
			return "\033[0;32m$message\033[0m";
		}

		public function yellow($message) {
			// return $message;
			return "\033[0;33m$message\033[0m";
		}


	}