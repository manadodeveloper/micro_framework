<?php
	namespace cli;
	class Crud extends BaseCli
	{

		public static $TAG_BEGIN 	= "#BEGIN-BLOCK-CRUD-";
		public static $TAG_END 		= "#END-BLOCK-CRUD-";
		public static $PATH 		= [
										"table" 		=> 	[
																"table-create"	=>BASE_ROOT."/app/migration/structure/create/",
																"drop"		=>BASE_ROOT."/app/migration/structure/drop/",
															],
										"route" 		=>	[
																"web"		=> BASE_ROOT."/app/routes/web.php",
																//"api"	=> BASE_ROOT."/app/routes/api.php",
															],
										"controller" 	=> BASE_ROOT."/app/controllers/<<MODUL_NAME>>Controller.php",
										"model" 		=> BASE_ROOT."/app/models/<<MODUL_NAME>>Model.php",
										"view" 			=> 	[
																"index"		=>BASE_ROOT."/app/views/<<LOWER_MODUL_NAME>>/index.php",
																"create"	=>BASE_ROOT."/app/views/<<LOWER_MODUL_NAME>>/create.php",
																"edit"	=>BASE_ROOT."/app/views/<<LOWER_MODUL_NAME>>/edit.php",
															],

										"blueprint"		=>	[
																"web"		=> BASE_ROOT."system/cli/blueprint/route.web.blueprint",
																"controller"=> BASE_ROOT."system/cli/blueprint/controller.blueprint",
																"model"		=> BASE_ROOT."system/cli/blueprint/model.blueprint",

																"index"		=> BASE_ROOT."system/cli/blueprint/view.index.blueprint",

																"create"	=> BASE_ROOT."system/cli/blueprint/view.create.blueprint",
																"edit"	=> BASE_ROOT."system/cli/blueprint/view.edit.blueprint",

																"table-create"	=> BASE_ROOT."system/cli/blueprint/table.create.blueprint",
																"table-drop"	=> BASE_ROOT."system/cli/blueprint/view.edit.blueprint",
																//"api"	=> BASE_ROOT."/app/routes/api.php",
															],
							  		  ];


		public static $modul_name ;

		//sebagai rujukan adalah web route
		public static $argv;

		public function index()
		{
			self::clear();
		}

		private function getArgv()
		{
        	self::$argv = $_SERVER['argv'];
        	if(isset(self::$argv[2])) 
        	{
        		self::$modul_name = ucfirst(self::$argv[2]);
				self::$modul_name = str_replace(" ","",self::$modul_name);
        	}
        	else
        	{
				echo  self::red(' tidak ada parameter nama modul') . "\n";
        		exit();
        	}
		}
		private function replaceCode($_code)
		{

			$lower_modul_name 	= strtolower(self::$modul_name);
			$code 				= str_replace("<<LOWER_MODUL_NAME>>", $lower_modul_name, $_code);
			$code 				= str_replace("<<MODUL_NAME>>", self::$modul_name, $code);
			return $code;
		}


		private function pushScriptRoute($_key)
		{

			$blueprint_path =  self::config('blueprint.cfg');

			$push_script = file_get_contents($blueprint_path['BLUEPRINT'][$_key]);
			$push_script = self::replaceCode($push_script);
			return $push_script;

		}
		public function createTable()
		{
			self::getArgv();
			$blueprint_path =  self::config('blueprint.cfg');

			echo "membuat table :\n";
			echo "------------------------------------------------\n";

			//dicari apakah ada script migration modul nya?
			$found = false;
			$base_path =  $blueprint_path['TABLE']['table-create'];
			$files = array_diff(scandir($base_path), array('.', '..'));
			if(count($files) > 0)
			{
				foreach ($files as $key => $value) 
				{
					$script = file_get_contents($base_path.$value);
					$regex = "/"."".$blueprint_path['TAG-BEGIN'].self::$modul_name."\n"."(.*?)".$blueprint_path['TAG-END'].self::$modul_name."/is";
				    $hasil = [];
				    preg_match_all($regex, $script, $hasil);
					//echo $base_path.$value."\n";
					//echo $regex."\n";
				    //print_r($hasil);
				    if(isset($hasil[0][0]))
				    {
				    	$found  = true;
						echo self::red(" [error]:");
						echo  self::red( $base_path.$value ).  "\n";
						echo  ' table  sudah ada' . "\n";
				    }
					//$content = get
				}
			}
		
			if(!$found)
			{
				try 
				{
					//menambah push script di route
					$filename =  str_pad(count($files)+1,2,"0",STR_PAD_LEFT)."_".strtolower(self::$modul_name).".sql";
					$filepath = $blueprint_path['TABLE']['table-create'].$filename;

					$code_file_route .= "\n\n\t".$blueprint_path['TAG-BEGIN'].self::$modul_name."\n";

					$code_file_route .= self::pushScriptRoute("table-create");
					$code_file_route .= "\n\t".$blueprint_path['TAG-END'].self::$modul_name;
				    file_put_contents($filepath,$code_file_route);

					echo  self::green(" [success]:")." script table -> '". self::green($filepath).  "' berhasil dibuat \n";
				}
				catch (Exception $e) 
				{
					echo  self::red( $value ).  "\n";
					echo  ' file route gagal ditambahkan' . "\n";
				    echo $e->getMessage() . "\n";
				}
			}
		}
		public function destroyTable()
		{
			self::getArgv();
			$blueprint_path =  self::config('blueprint.cfg');


			echo "menghapus table :\n";
			echo "------------------------------------------------\n";

			//dicari apakah ada script migration modul nya?
			$found = false;
			$base_path =  $blueprint_path['TABLE']['table-create'];
			$files = array_diff(scandir($base_path), array('.', '..'));
			if(count($files) > 0)
			{
				foreach ($files as $key => $value) 
				{
					$script = file_get_contents($base_path.$value);
					$regex = "/"."".$blueprint_path['TAG-BEGIN'].self::$modul_name."\n"."(.*?)".$blueprint_path['TAG-END'].self::$modul_name."/is";
				    $hasil = [];
				    preg_match_all($regex, $script, $hasil);
					//echo $base_path.$value."\n";
					//echo $regex."\n";
				    //print_r($hasil);
				    if(isset($hasil[0][0]))
				    {
				    	$found  = true;

						unlink($base_path.$value);

						echo self::green(" [success]:");
						echo  self::green( $base_path.$value ).  "\n";
						echo  ' file sudah dihapus ' . "\n";
				    }
					//$content = get
				}
				if(!$found)
				{

					echo self::red(" [error]:");
					echo  'file  table tidak ditemukan' . "\n";
				}
			}
		}

		public function createRoute()
		{
			self::getArgv();
			$blueprint_path =  self::config('blueprint.cfg');

			echo "membuat route :\n";
			echo "------------------------------------------------\n";



			foreach ($blueprint_path['ROUTE'] as $key => $value) 
			{
				//check file exist
				if(file_exists($value))
				{
					$code_file_route = file_get_contents($value);
					if(strpos($code_file_route, self::$modul_name."Controller") !== false)
					{
						echo  self::red( " modul '".self::$modul_name."' sudah ada" ).  "\n";
					} 
					else
					{
						try 
						{
							//menambah push script di route

							$code_file_route .= "\n\n\t".$blueprint_path['TAG-BEGIN'].self::$modul_name."\n";

							$code_file_route .= self::pushScriptRoute($key);

							//menghapus script khusus
							$special_script = "Router::checkFoundPage();";
							$code_file_route = str_replace($special_script, "", $code_file_route);

							$code_file_route .= "\n\t".$blueprint_path['TAG-END'].self::$modul_name;
							$code_file_route .= "\n\t".$special_script;


	

						    file_put_contents($value,$code_file_route);
							echo  self::green(' '. $value ).  "\n";
							echo ' file route berhasil ditambahkan. Harap mengganti parameter middleware untuk keamanan yang dibutuhkan.'.  "\n";
						}
						catch (Exception $e) 
						{
							echo  self::red( $value ).  "\n";
							echo  ' file route gagal ditambahkan' . "\n";
						    echo $e->getMessage() . "\n";
						}
					}
				}
				else
				{
					echo  self::red( "file $value tidak ada." ).  "\n";
				}


			}

			//backup web route
		}
		public function destroyRoute()
		{
			self::getArgv();
			$blueprint_path =  self::config('blueprint.cfg');

			echo "menhapus route :\n";
			echo "------------------------------------------------\n";

			foreach ($blueprint_path['ROUTE'] as $key => $value) 
			{
				//check file exist
				if(file_exists($value))
				{
					$code_file_route = file_get_contents($value);
				    //$regex = "#"."\n\n\t".$blueprint_path['TAG-BEGIN'].self::$modul_name."\n"."(.*?)".$blueprint_path['TAG-END'].self::$modul_name."#is";
				    //$regex = "#BEGIN(.*?)END#is";
				    //$regex = "#/\*.+?\*/#s";
				    
					$regex = "/"."".$blueprint_path['TAG-BEGIN'].self::$modul_name."\n"."(.*?)".$blueprint_path['TAG-END'].self::$modul_name."/is";
					//$regex = "/#"."(.*?)"."#/is";

				    


				    $hasil = [];
				    
				    preg_match_all($regex, $code_file_route, $hasil);
				    if(isset($hasil[0][0]))
				    {
				    	try 
						{
							//menghapus block
							$code_file_route = str_replace($hasil[0][0], "",  $code_file_route);
						    file_put_contents($value,$code_file_route);
							echo  self::green(" modul '".self::$modul_name.  "' berhasil dihapus dari {$value} \n");
						}
						catch (Exception $e) 
						{
							echo  self::red( $value ).  "\n";
							echo  ' modul gagal dihapus' . "\n";
						    echo $e->getMessage() . "\n";
						}
				    }
				    else
				    {
						echo  self::red(" modul '".self::$modul_name.  "' tidak ditemukan di route file {$value} \n");
				    }
				}
			}
		}

		public function createController()
		{
			self::getArgv();

			echo "membuat controller:\n";
			echo "------------------------------------------------\n";

			$blueprint_path =  self::config('blueprint.cfg');

			$file_path = str_replace("<<MODUL_NAME>>",  self::$modul_name , $blueprint_path['CONTROLLER']);
			if(file_exists($file_path))
			{
				echo self::red(" [error]:");
				echo  " file controller sudah ada ".self::red( $file_path).  "\n";
				echo  ' file tidak ditimpa demi keamaan' . "\n";
			}
			else
			{

				$code_file = file_get_contents($blueprint_path['BLUEPRINT']['controller']);
				$code_file = self::replaceCode($code_file);
				try 
				{
				    file_put_contents($file_path,$code_file);
					echo  self::green(" [success]:")." controller -> '". self::green($file_path).  "' berhasil dibuat \n";
				}
				catch (Exception $e) 
				{
					echo self::red(" [error]:");
					echo  self::red( $file_path ).  "\n";
					echo  ' controller gagal dibuat' . "\n";
				    echo $e->getMessage() . "\n";
				}

			}
		}
		public function destroyController()
		{
			self::getArgv();
			echo "menghapus controller:\n";
			echo "------------------------------------------------\n";

			$file_path = str_replace("<<MODUL_NAME>>",  self::$modul_name , self::$PATH['controller']);
			if(file_exists($file_path))
			{
				try 
				{
					unlink($file_path);
					echo  self::green(" [success]:")." controller -> '". self::green($file_path).  "' berhasil dihapus \n";
				}
				catch (Exception $e) 
				{
					echo self::red(" [error]:");
					echo  self::red( $file_path ).  "\n";
					echo  ' controller gagal dibuat' . "\n";
				    echo $e->getMessage() . "\n";
				}
			}
			else
			{
				echo  self::yellow(" [warning]:").' file '.self::yellow($file_path).' sudah tidak ada' . "\n";
			}	
		}

		public function createModel()
		{

			self::getArgv();
			$blueprint_path =  self::config('blueprint.cfg');

			echo "membuat model:\n";
			echo "------------------------------------------------\n";

			$file_path = str_replace("<<MODUL_NAME>>",  self::$modul_name , $blueprint_path['MODEL']);
			if(file_exists($file_path))
			{

				echo self::red(" [error]:");
				echo  " file model sudah ada ".self::red( $file_path).  "\n";
				echo  ' file tidak ditimpa demi keamaan' . "\n";
			}
			else
			{

				$code_file = file_get_contents($blueprint_path['BLUEPRINT']['model']);
				$code_file = self::replaceCode($code_file);
				try 
				{
				    file_put_contents($file_path,$code_file);
					echo  self::green(" [success]:")." model -> '". self::green($file_path).  "' berhasil dibuat \n";
				}
				catch (Exception $e) 
				{
					echo self::red(" [error]:");
					echo  self::red( $file_path ).  "\n";
					echo  ' model gagal dibuat' . "\n";
				    echo $e->getMessage() . "\n";
				}

			}
		}
		public function destroyModel()
		{
			self::getArgv();
			echo "menghapus controller:\n";
			echo "------------------------------------------------\n";

			$file_path = str_replace("<<MODUL_NAME>>",  self::$modul_name , self::$PATH['model']);
			if(file_exists($file_path))
			{
				try 
				{
					unlink($file_path);
					echo  self::green(" [success]:")." model -> '". self::green($file_path).  "' berhasil dihapus \n";
				}
				catch (Exception $e) 
				{
					echo self::red(" [error]:");
					echo  self::red( $file_path ).  "\n";
					echo  ' model gagal dihapus' . "\n";
				    echo $e->getMessage() . "\n";
				}
			}
			else
			{
				echo  self::yellow(" [warning]:").' file '.self::yellow($file_path).' sudah tidak ada' . "\n";
			}	
		}

		public function createView()
		{

			self::getArgv();
			$blueprint_path =  self::config('blueprint.cfg');

			echo "membuat view:\n";
			echo "------------------------------------------------\n";
			foreach ($blueprint_path['VIEW'] as $key => $value) 
			{
				$file_path = self::replaceCode($value);
				$dir_path = dirname(self::replaceCode($file_path));
				if(!is_dir($dir_path))
				{
					@mkdir($dir_path);
				}
				$code_file = file_get_contents($blueprint_path['BLUEPRINT'][$key]);	
				$code_file = self::replaceCode($code_file);
				try 
				{
				    file_put_contents($file_path,$code_file);
					echo  self::green(" [success]:")." view -> '". self::green($file_path).  "' berhasil dibuat \n";
				}
				catch (Exception $e) 
				{
					echo self::red(" [error]:");
					echo  self::red( $file_path ).  "\n";
					echo  ' view gagal dibuat' . "\n";
				    echo $e->getMessage() . "\n";
				}		
			}
		}



		


	}
?>