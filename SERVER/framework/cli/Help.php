<?php
	
	namespace cli;
	class Help extends BaseCli
	{
		public function index()
		{
			
			echo "PERINTAH YANG TERSEDIA:\n";
			echo "------------------------------------------------\n";
			echo "\n";
			echo "- " .self::green('Cache'). "@".self::yellow('clear') . "\n";
			echo "\n";
			echo "- " .self::green('Migration'). "@". self::yellow('reset') . "\n";
			echo "- " .self::green('Migration'). "@". self::yellow('structure') . "\n";
			echo "- " .self::green('Migration'). "@". self::yellow('drop') . "\n";
			echo "- " .self::green('Migration'). "@". self::yellow('insert') . "\n";
			echo "- " .self::green('Migration'). "@". self::yellow('empty') . "\n";
			echo "\n";
			echo "- " .self::green('Crud'). "@". self::yellow('createTable') . " <<modul_name>>\t\t\t contoh: Crud@createTable Siswa\n";
			echo "- " .self::green('Crud'). "@". self::yellow('destroyTable') . " <<modul_name>>\t\t\t contoh: Crud@destroyTable Siswa\n";
			echo "- " .self::green('Crud'). "@". self::yellow('createRoute') . " <<modul_name>>\t\t\t contoh: Crud@createRoute Siswa\n";
			echo "- " .self::green('Crud'). "@". self::yellow('destroyRoute') . " <<modul_name>>\t\t\t contoh: Crud@destroyRoute Siswa\n";
			echo "- " .self::green('Crud'). "@". self::yellow('createController') . " <<modul_name>>\t\t\t contoh: Crud@createController Siswa\n";
			echo "- " .self::green('Crud'). "@". self::yellow('destroyController') . " <<modul_name>>\t\t\t contoh: Crud@destroyController Siswa\n"; 
			echo "- " .self::green('Crud'). "@". self::yellow('createModel') . " <<modul_name>>\t\t\t contoh: Crud@createModel Siswa\n";
			echo "- " .self::green('Crud'). "@". self::yellow('destroyModel') . " <<modul_name>>\t\t\t contoh: Crud@destroyModel Siswa\n"; 
			echo "- " .self::green('Crud'). "@". self::yellow('createView') . " <<modul_name>>\t\t\t contoh: Crud@createView Siswa\n";
			echo "\n";
			echo "================================================\n";
		}

	}
?>