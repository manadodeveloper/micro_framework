<?php
	use core\DB;
    use helper\ArrayHelper;
    use core\Role;

    class UserModel extends core\Controller
	{
        use helper\ArrayHelper;
		private 	$db;
		private 	$table = "users";//<-- edit
        private     $columns = 'id ,nip,name, full_name , email, wilayah,status';
        private     $datatables     =   [
                                            "draw"              => null,
                                            "record-id"			=> "id",//<-- edit //wajib ada untuk id show/update/delete
                                            "record-name"		=> "full_name",//<-- edit

                                            "search"            =>  [
																		"full_name"       =>"",

                                                                    ],//<-- edit //column db , value
                                            "filter"            =>  [

                                                                        "status"          => "",
                                                                    ],//<-- edit //column db , value
                                            "length"            => 10,// Ambil data limit per page
                                            "start"             => 0,// Ambil data start
                                            "order-column"      => "nip",//<-- edit //harusnya array colum sama order digabung
                                            "order-ascdesc"     => "ASC",//<-- edit //harusnya array
                                        ];//end datatables



		public function __construct()
		{

			$this->db = new DB();
			if(isset($_POST['draw']))
			{
				$this->datatables['draw'] = $_POST['draw'] ;    
			}
			if(isset($_POST['length']))
			{
				$this->datatables['length']= $_POST['length'];    
			}
			if(isset($_POST['start']))
			{
				$this->datatables['start'] = $_POST['start'];    
			}

            if(isset($_POST['order'][0]['column']))
            {
                $this->datatables['order-column'] = $_POST['order'][0]['column'];
            }
        	if(isset($_POST['columns'][$this->datatables['order-column']]['data']))
            {
                $this->datatables['order-column'] = $_POST['columns'][$this->datatables['order-column']]['data'];            
            }
            if(isset($_POST['order'][0]['dir']))
            {
             	$this->datatables['order-ascdesc'] = $_POST['order'][0]['dir'];
            }

		}

        //biasanya dipakai untuk modul komponen select2
		public function pluck()
		{
			return $this->db->from($this->table)->pluck($this->datatables['record-id'],$this->datatables['record-name']);
		}



        //return model
        //sudah terdapat fasilitas filter
        private function list()
        {

            $model = $this->db->select($this->columns)->from($this->table); //default table, bisa juga memakai join
            //=====================begin filtering/searching sql========================
            //<-- ganti sini
            foreach ($this->datatables['filter'] as $key => $value) 
            {
                if(!empty($_POST[$key])  || $_POST[$key] == "0")
                {
                    $this->datatables['filter'][$key] =  $_POST[$key];

                    $model = $model->where($key,$this->datatables['filter'][$key]);
                }
            }

            foreach ($this->datatables['search'] as $key => $value) 
            {
                if(!empty($_POST[$key]))
                {
                    $this->datatables['search'][$key] =  $_POST[$key];

                    $model = $model->like($key,$this->datatables['search'][$key]);
                }
            }
            return $model;
        }

        public function getData()
        {
            $model = $this->list();
            return $model->fetch();
        }


        public function datatables()
        {
            //dapatkan semua data termasuk sudah disearch/difilter
            $model = $this->list();
            $model->execute(); 
            $total = $model->affected_rows;
            
            //total setelah difilter
            $data = [];
            if($total > 0)
            {               

                $model =    $model
                                ->order_by($this->datatables['order-column'],$this->datatables['order-ascdesc'])
                                ->limit($this->datatables['length'],$this->datatables['start'])
                                ->execute();
                $data = $model->fetch();

                for($i=0;$i<count($data);$i++)
                {
                	foreach ($data[$i] as $key => $value) 
                	{
                		if(isset($this->datatables['record-name']))
                		{
                			if($key == $this->datatables['record-name'])
	                		{
	                			$data[$i][$this->datatables['record-name']] = "<p class='record-name'>{$data[$i][$this->datatables['record-name']]} </p>";	
	                		}


                		}
                    }
                    //masukkan button disini


                    $role = Role::getUserRole($data[$i]['id']);
                    $data[$i]['role'] = "";
                    foreach ($role as  $value) 
                    {
                        $data[$i]['role'] .= "<span class='badge badge-count'>".strtoupper($value)."</span>";
                    }
                    switch ($data[$i]['status']) 
                    {
                        case '0':
                                $data[$i]['status']     =   "<span class='text-warning'>Belum Aktif</span>";    
                                $button_action          =   "<a class='button-active  dropdown-item'  record-id='{$data[$i]['id']}' >Aktifkan</a>"; 
                            break;
                        case '1':
                                $data[$i]['status']     =   "<span class='text-success'>Aktif</span>";
                                $button_action          =   "
                                                                    <a class='dropdown-item button-block' record-id='{$data[$i]['id']}' >Blokir</a>
                                                                    <a class='dropdown-item button-edit' record-id='{$data[$i]['id']}'  >Ubah</a>
                                                                    <a class='dropdown-item' href='' >Ganti Photo Profile</a>
                                                            ";  
                            break;
                        case '2':

                                $data[$i]['status']     =   "<span class='text-danger'>Diblokir</span>";
                                $button_action          =   "<a class='button-active  dropdown-item'  record-id='{$data[$i]['id']}' >Aktifkan</a>";
                            break;
                    }
                    

                    $data[$i]['button'] =     "
                                        <div class='btn-group'>
                                            <button type='button' class='btn btn-info btn-flat btn-xs ' data-toggle='dropdown' >
                                                <i class='fa fa-cog'></i>
                                            </button>
                                            <button type='button' class='btn btn-info btn-flat btn-xs  dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                                                <span class='sr-only'>Toggle Dropdown</span>
                                            </button>
                                            <div class='dropdown-menu' role='menu' x-placement='bottom-start' style='position: absolute; transform: translate3d(87px, 43px, 0px); top: 0px; left: 0px; will-change: transform;'>
                                                {$button_action}
                                            </div>
                                        </div>
                                                ";
                }

                $model->reset();//menghapus string query dan data.jika mau query ulang bisa mulai dari instance new DB()
                $this->response =   [
                                        "draw" => $this->datatables['draw'],
                                    ];
            }

            
            $this->response =   [
                                    "recordsTotal"=> $total,
                                    "recordsFiltered"=> $total,
                                    "data" => $data,
                                ];//end response

            return $this->response;
        }

        public function isExists($_model_id)
        {
            $model = $this  ->db
                            ->select($this->columns)
                            ->from($this->table)
                            ->where($this->datatables['record-id'],$_model_id)
                            ; //default table, bisa juga memakai join
            $model->execute(); 
            $total = $model->affected_rows;
            $ouput = false;
            if($total > 0)
            {
                $ouput = true;
            }
            return $ouput;
        }

        public function detail(String $_model_id)
        {
            if($this->isExists($_model_id))
            {
                $model = $this->db->select($this->columns)->from($this->table)->where($this->datatables['record-id'],$_model_id)->first()->fetch(); //default table, 
                $this->response = [
                                "status" => 'success',
                                "desc" => "data berhasil difetch",
                                "data" => $model[0],
                            ];//end response
            }
            else
            {
                $this->response = [
                                "status" => 'error',
                                "desc" => "user tidak terdaftar",
                            ];//end response
            }
            return $this->response;

        }

        public function destroy(String $_model_id)
        {

            $this->db->reset();
            $this->db
                ->delete($this->table)
                ->where($this->datatables['record-id'],$_model_id)
                ->execute();
            $delete =  $this->db->affected_rows ;
            if($delete > 0)
            {
                $this->response = [
                                "status" => 'success',
                                "desc" => "berhasil dihapus",
                            ];//end response
            }
            else
            {

                $this->response = [
                                "status" => 'error',
                                "desc" => "gagal dihapus",
                            ];//end response
            }
            $this->db->reset();
            return $this->response;
        }

        public function create()
        {

            $post = $_POST;
            $rules =    [
                            //'siswa-id' => ['required','string','min-length' =>4],
                            'nip' => ['required','string','min-length' =>3],
                            'username' => ['required','string','min-length' =>6],
                            'email' => ['required','email'],
                            'password' => ['required','string'],
                            'password-confirm' => ['required','confirm'=>'password'],
                            'full-name' => ['required','string'],
            ];//end rules
            //opsional alias
            $alias = [
                    'nip' => 'kode user',
                    'username' => 'username',
                    'email' => 'email',
                    'password' => 'password',
                    'password-confirm' => 'password konfirmasi',
                    'full-name' => 'nama lengkap',
            ];//end alias

            $validate = new core\Validator($rules,$post,$alias);
            if($validate->error())
            {
                foreach ($validate->error() as $error) 
                {
                    $this->response['desc'] .= $error."<br />";
                }
            }
            else
            {

                $config_hash = $this->config('hash.cfg');
                $post['password'] = password_hash($post['password'], $config_hash['type'] , $config_hash['options']);
                //input_name_post => field_db
                $index_transform =  [
                                        "nip"=>'nip',
                                        "username"=>'name',
                                        "email"=>'email',
                                        "password"=>'password',
                                        "full-name"=>'full_name',
                                        "wilayah"=>'wilayah',
                                    ];
                $data  = $this->transform($index_transform,$post);

                $role = isset($post['role']) ? $post['role'] : [];

                $user_id = $this->db->insert($this->table,$data);
                Role::syncRole($user_id,$role);
                //echo $this->db->getStringQuery();
                if($this->db->error())
                {
                    //$this->response['desc'] .= implode("<br />", $this->db->error());
                    $this->response['desc'] = "data gagal dimasukan";
                }
                else
                {
                    $this->response['status'] = "success";
                    $this->response['desc'] = "data sudah berhasil dimasukan";

                }
            }
            return $this->response;
        }



        public function update()
        {

            $post = $_POST;

            $rules =    [
                            'id'        => ['required','string'],
                            'full_name' => ['required','string','min-length' =>5],
                            'role'      => ['required'],
            ];//end rules
            //opsional alias
            $alias = [
                    'id'        => 'id pengguna',
                    'full_name' => 'nama pengguna ',
                    'role'      => 'hak akses ',
            ];//end alias


            $validate = new core\Validator($rules,$post,$alias);
            if($validate->error())
            {
                //print_r($validate->error());exit();
                //$this->response['desc'] =  implode($validate->error(), "<br />");

                foreach ($validate->error() as $error) 
                {
                    $this->response['desc'] .= $error."<br />";
                }
            }
            else
            {

                //input_name_post => field_db
                $index_transform =  [
                                        "id"=>'id',
                                        "full_name"=>'full_name',
                                        "wilayah"=>'wilayah',
                                    ];
                $data  = $this->transform($index_transform,$post);
                $role = $post['role'];

                $where = [$this->datatables['record-id'] =>$post[$this->datatables['record-id']] ];

                $update_model = $this->db->where($where)->update($this->table,$data);
                $update_role = Role::syncRole($post[$this->datatables['record-id']],$role);
                if($update_model )
                {
                    $this->response =     [
                                        "status" => 'success',
                                        "desc" => "data berhasil diupdate",
                                    ];
                }
                else
                {
                    $this->response =     [
                                        "status" => 'error',
                                        "desc" => "data gagal diupdate",
                                    ];
                }
                
            }
            return $this->response;
        }

        public function block($_user_id)
        {
            if($this->isExists($_user_id))
            {
                $data = [
                            "status" =>2,
                        ];
                $this->db->update($this->table,$data)->execute();
                $this->response = [
                                "status" => 'success',
                                "desc" => "berhasil diblok",
                            ];//end response
            }
            else
            {
                $this->response = [
                                "status" => 'error',
                                "desc" => "user tidak terdaftar",
                            ];//end response
            }
            return $this->response;
        }

        public function active($_user_id)
        {
            if($this->isExists($_user_id))
            {
                $data = [
                            "status" =>1,
                        ];
                $this->db->update($this->table,$data)->execute();
                $this->response = [
                                "status" => 'success',
                                "desc" => "berhasil diaktifkan",
                            ];//end response
            }
            else
            {
                $this->response = [
                                "status" => 'error',
                                "desc" => "user tidak terdaftar",
                            ];//end response
            }
            return $this->response;
        }

	}