<?php
	use core\DB;
    use helper\ArrayHelper;
    class WilayahModel extends core\Controller
	{
        use helper\ArrayHelper;
		private 	$db;
		private 	$table = "wilayah";//<-- edit
        private     $columns = '*';//<-- edit
        private     $datatables     =   [
                                            "draw"              => null,
                                            "record-id"			=> "wilayah_id",//<-- edit //wajib ada untuk id show/update/delete
                                            "record-name"		=> "wilayah_nama",//<-- edit

                                            "search"            =>  [
																		"wilayah_id"     =>"",
																		"wilayah_nama"   =>"",

                                                                    ],//<-- edit //column db , value
                                            "filter"            =>  [],//<-- edit //column db , value
                                            "length"            => 10,// Ambil data limit per page
                                            "start"             => 0,// Ambil data start
                                            "order-column"      => "wilayah_id",//<-- edit //harusnya array colum sama order digabung
                                            "order-ascdesc"     => "ASC",//<-- edit //harusnya array
                                        ];//end datatables



		public function __construct()
		{

			$this->db = new DB();
			if(isset($_POST['draw']))
			{
				$this->datatables['draw'] = $_POST['draw'] ;    
			}
			if(isset($_POST['length']))
			{
				$this->datatables['length']= $_POST['length'];    
			}
			if(isset($_POST['start']))
			{
				$this->datatables['start'] = $_POST['start'];    
			}

            if(isset($_POST['order'][0]['column']))
            {
                $this->datatables['order-column'] = $_POST['order'][0]['column'];
            }
        	if(isset($_POST['columns'][$this->datatables['order-column']]['data']))
            {
                $this->datatables['order-column'] = $_POST['columns'][$this->datatables['order-column']]['data'];            
            }
            if(isset($_POST['order'][0]['dir']))
            {
             	$this->datatables['order-ascdesc'] = $_POST['order'][0]['dir'];
            }

		}

        //biasanya dipakai untuk modul komponen select2
		public function pluck()
		{
			return $this->db->from($this->table)->pluck($this->datatables['record-id'],$this->datatables['record-name']);
		}



        //return model
        //sudah terdapat fasilitas filter
        private function list()
        {

            $model = $this->db->select($this->columns)->from($this->table); //default table, bisa juga memakai join
            //=====================begin filtering/searching sql========================
            //<-- ganti sini
            foreach ($this->datatables['filter'] as $key => $value) 
            {
                if(!empty($_POST[$key])  || $_POST[$key] == "0")
                {
                    $this->datatables['filter'][$key] =  $_POST[$key];

                    $model = $model->where($key,$this->datatables['filter'][$key]);
                }
            }

            foreach ($this->datatables['search'] as $key => $value) 
            {
                if(!empty($_POST[$key]))
                {
                    $this->datatables['search'][$key] =  $_POST[$key];

                    $model = $model->like($key,$this->datatables['search'][$key]);
                }
            }
            return $model;
        }

        public function getData()
        {
            $model = $this->list();
            return $model->fetch();
        }


        public function datatables()
        {
            //dapatkan semua data termasuk sudah disearch/difilter
            $model = $this->list();
            $model->execute(); 
            $total = $model->affected_rows;
            
            //total setelah difilter
            $data = [];
            if($total > 0)
            {               

                $model =    $model
                                ->order_by($this->datatables['order-column'],$this->datatables['order-ascdesc'])
                                ->limit($this->datatables['length'],$this->datatables['start'])
                                ->execute();
                $data = $model->fetch();

                for($i=0;$i<count($data);$i++)
                {
                	foreach ($data[$i] as $key => $value) 
                	{
                		if(isset($this->datatables['record-name']))
                		{
                			if($key == $this->datatables['record-name'])
	                		{
	                			$data[$i][$this->datatables['record-name']] = "<p class='record-name'>{$data[$i][$this->datatables['record-name']]} </p>";	
	                		}


                		}
                    }
                    //masukkan button disini
                    $button_action =    "
                                                    <a class='dropdown-item button-destroy' record-id='{$data[$i][$this->datatables['record-id']]}' >Hapus</a>
                                            ";  

                    $data[$i]['button'] =     "
                                        <div class='btn-group'>
                                            <button type='button' class='btn btn-info btn-flat btn-xs ' data-toggle='dropdown' >
                                                <i class='fa fa-cog'></i>
                                            </button>
                                            <button type='button' class='btn btn-info btn-flat btn-xs  dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                                                <span class='sr-only'>Toggle Dropdown</span>
                                            </button>
                                            <div class='dropdown-menu' role='menu' x-placement='bottom-start' style='position: absolute; transform: translate3d(87px, 43px, 0px); top: 0px; left: 0px; will-change: transform;'>
                                                {$button_action}
                                            </div>
                                        </div>
                                                ";
                }

                $model->reset();//menghapus string query dan data.jika mau query ulang bisa mulai dari instance new DB()
                $this->response =   [
                                        "draw" => $this->datatables['draw'],
                                    ];
            }

            
            $this->response =   [
                                    "recordsTotal"=> $total,
                                    "recordsFiltered"=> $total,
                                    "data" => $data,
                                ];//end response

            return $this->response;
        }

        public function isExists($_model_id)
        {
            $model = $this  ->db
                            ->select($this->columns)
                            ->from($this->table)
                            ->where($this->datatables['record-id'],$_model_id)
                            ; //default table, bisa juga memakai join
            $model->execute(); 
            $total = $model->affected_rows;
            $ouput = false;
            if($total > 0)
            {
                $ouput = true;
            }
            return $ouput;
        }

        public function detail(String $_model_id)
        {
            if($this->isExists($_model_id))
            {
                $model = $this->db->select($this->columns)->from($this->table)->where($this->datatables['record-id'],$_model_id)->first()->fetch(); //default table, 
                $this->response = [
                                "status" => 'success',
                                "desc" => "data berhasil difetch",
                                "data" => $model[0],
                            ];//end response
            }
            else
            {
                $this->response = [
                                "status" => 'error',
                                "desc" => "user tidak terdaftar",
                            ];//end response
            }
            return $this->response;

        }

        public function destroy(String $_model_id)
        {

            $this->db->reset();
            $this->db
                ->delete($this->table)
                ->where($this->datatables['record-id'],$_model_id)
                ->execute();
            $delete =  $this->db->affected_rows ;
            if($delete > 0)
            {
                $this->response = [
                                "status" => 'success',
                                "desc" => "berhasil dihapus",
                            ];//end response
            }
            else
            {

                $this->response = [
                                "status" => 'error',
                                "desc" => "gagal dihapus",
                            ];//end response
            }
            $this->db->reset();
            return $this->response;
        }

        public function create()
        {

            $post = $_POST;
            $rules =    [
                            //'siswa-id' => ['required','string','min-length' =>4],
                            'wilayah-nama' => ['required','string','min-length' =>6],
            ];//end rules
            //opsional alias
            $alias = [
                    'wilayah-id' => 'kode wilayah ',
                    'wilayah-nama' => 'nama wilayah ',
            ];//end alias

            $validate = new core\Validator($rules,$post,$alias);
            if($validate->error())
            {
                foreach ($validate->error() as $error) 
                {
                    $this->response['desc'] .= $error."<br />";
                }
            }
            else
            {
                //input_name_post => field_db
                $index_transform =  [
                                        "wilayah-id"    =>  'wilayah_id',
                                        "wilayah-nama"  =>  'wilayah_nama'
                                    ];
                $data  = $this->transform($index_transform,$post);

                $this->db->insert($this->table,$data);
                //echo $this->db->getStringQuery();
                if($this->db->error())
                {
                    //$this->response['desc'] .= implode("<br />", $this->db->error());
                    $this->response['desc'] = "data gagal dimasukan";
                }
                else
                {
                    $this->response['status'] = "success";
                    $this->response['desc'] = "data sudah berhasil dimasukan";

                }
            }
            return $this->response;
        }




	}