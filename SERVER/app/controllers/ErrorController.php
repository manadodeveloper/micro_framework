<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * ErrorController
 * Berfungsi sebagai controller error.
 */

	use core\Auth;

	class ErrorController extends core\Controller
	{

                /**
                 * index()
                 * Berfungsi menampilkan output konten halaman error
                 *
                 * @param tidak ada
                 * @return output konten halaman errorpage
                 */
        	public function index()
        	{
        		$this->view('error/index.tmpl')->render();
        	}

	}
?>