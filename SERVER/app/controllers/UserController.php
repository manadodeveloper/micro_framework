<?php
	use core\Auth;

	use Mpdf\QrCode\Output;


	class UserController extends core\Controller
	{

		use helper\ArrayHelper;
		private $model;

		public function __construct()
		{
			$this->model = $this->model("UserModel");
		}


		/**
		* index()
		* Berfungsi menampilkan output konten halaman user
		*
		* @param tidak ada
		* @return output konten halaman userpage
		*/
		public function index()
		{
			$this->view('user/index.tmpl')->render();
		}


		/**
		 * list()
		 * Berfungsi untuk mengabil data dari model kemudian dirender dengan format json datatables
		 *
		 * @param tidak ada     
		 *
		 * @return json datatables;    
		 */
		public function list()
		{
			$this->toJSON($this->model->datatables())->render();
		}


		/**
		 * create()
		 * berfungsi untuk menampilkan halaman create
		 *
		 * @param tidak ada     
		 *
		 * @return view;    
		 */
		public function create()
		{	
			$this->view('user/create.tmpl')->render();
		}


		/**
		 * store()
		 * berfungsi input data
		 *
		 * @param tidak ada     
		 * input diambil dari $_POST array.script ada di file model
		 * validasi masuk kedalam file model
		 * 
		 * @return json;    
		 */
		public function store()
		{	
			$this->toJSON($this->model->create())->render();
		}


		/**
		 * edit(String $_model_id)
		 * berfungsi untuk menampilkan halaman edit dengan paramater model ID
		 *
		 * @param String $_model_id     
		 *
		 * @return view;    
		 */
		public function edit($_model_id)
		{
			$data = $this->model->detail($_model_id);
			if($data['status'] == "success")
			{
				$this->view('user/edit.tmpl',$data['data'])->render();
			}
			else
			{
				$this->view('error/index',$data)->render();
			}
		}


		/**
		 * update()
		 * berfungsi update data
		 *
		 * @param tidak ada     
		 * input diambil dari $_POST array.script ada di file model
		 * validasi masuk kedalam file model
		 * 
		 * @return json;    
		 */
		public function update()
		{
			$this->toJSON($this->model->update())->render();
		}


		/**
		 * destroy(String $_model_id)
		 * berfungsi untuk menampilkan halaman edit dengan paramater model ID
		 *
		 * @param String $_model_id     
		 *
		 * @return view;    
		 */
		public function destroy($_model_id)
		{
			$this->toJSON($this->model->destroy($_model_id))->render();
		}


		/**
		 * pdf()
		 * generate pdf
		 * untuk paramter post sudah proses di file model
		 *
		 * @param tidak ada     
		 *
		 * @return pdf file;    
		 */
		public function pdf()
		{
			//debug model
			//print_r($this->model->getData());exit();

		    $component_table =  [
		                            "id"        => "table",
		                            "class"     => "",
		                            "other-options" 	=> " border='1' width='100%' autosize='1' cellspacing='0' cellpadding='0'",
		                            "head"      =>  [
		                                                ["title"     => "KODE",	"style"     => "border='1px solid black'",],
		                                                ["title"     => "NAMA",	"style"     => "border='1px solid black'",],
		                                                /*....
	                                                 	tambah kolom disini
		                                                ...*/
		                                            ],//end head
                                    "data"		=>	$this->model->getData(),
		                        ];//end table

			$mpdf = new \Mpdf\Mpdf();
			$mpdf->WriteHTML(core\Form::datatable($component_table));
			$mpdf->shrink_tables_to_fit = 1;
			$mpdf->Output();
		}

		/**
		 * csv()
		 * generate csv
		 * untuk paramter post sudah proses di file model
		 *
		 * @param tidak ada     
		 *
		 * @return csv file;    
		 */
		public function csv()
		{
			//print_r($this->model->getData());exit();
			$title[0] = ["KODE","NAMA"];
			$data = $this->model->getData();
			$this->toCSV("csvku.csv", $data,$title);
		}

		
		public function block($_user_id)
		{
			$model = $this->model("UserModel");
		 	$this->toJSON($model->block($_user_id))->render();
		}


		public function active($_user_id)
		{
			$model = $this->model("UserModel");
		 	$this->toJSON($model->active($_user_id))->render();
		}
		

		/**
		 * formTheme()
		 * berfungsi untuk menampilkan form theme
		 *
		 * @param tidak ada     
		 *
		 * @return view;    
		 */
		public function formTheme()
		{	
			$this->view('user/theme.tmpl')->render();
		}


	}