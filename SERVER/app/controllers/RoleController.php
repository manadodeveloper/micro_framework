<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * RoleController
 * Berfungsi sebagai controller role
 */


	use core\Auth;
	use core\Role;

	class RoleController extends core\Controller
	{

		/**
		* index()
		* Berfungsi menampilkan output konten halaman user
		*
		* @param tidak ada
		* @return output konten halaman userpage
		*/
		public function index()
		{
			$data = [];
			$roles = Role::getAllRole();
			foreach ($roles as $role_key => $role_value) 
			{
				# code...
				$permissions = Role::getRolePermission($role_key);
				$string_permission = "";
				$string_menu= "";
				if(count($permissions)> 0)
				{

					foreach ($permissions as $permission_key => $permission_value) 
					{

						//$data[$role_key]['permissions'][$permission_value['name']] = $permission_value['menu_title'];
						$string_permission .= "<span class='badge badge-count'>{$permission_value['name']}</span> ";
						if($permission_value['menu_title'] != "")
						{
							$string_menu .= "<span class='badge badge-count'>{$permission_value['menu_title']}</span> ";
						}
					}
				}

				$data[] =  [$role_value, $string_permission,$string_menu];

				//$data['roles'][] = $value;
				//$data['permissions'][] = $permissions;
				//$data[$key]['permissions'] = $permissions;
			}

			$this->view('role/index.tmpl',$data)->render();
		}
	}