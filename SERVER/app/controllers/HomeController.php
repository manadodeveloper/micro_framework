<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * HomeController
 * Berfungsi sebagai controller home.
 */
	use core\Auth;

	class HomeController extends core\Controller
	{

		/**
		 * index()
		 * Berfungsi menampilkan output konten halaman home
		 *
		 * @param tidak ada
		 * @return output konten halaman homepage
		 */
		public function index()
		{			
			$this->view('home/index.tmpl')->render();
		}
		

		/**
		 * blankPage()
		 * Berfungsi menampilkan output konten testing blank page
		 *
		 * @param tidak ada
		 * @return output konten halaman blankpage
		 */
		public function blankPage()
		{
			$this->view('home/blankpage.tmpl')->render();
		}


		public function hash()
		{

            $config_hash = $this->config('Hash');
            $pasword = '123456';
			$password = password_hash($pasword, $config_hash['type'] , $config_hash['options']);
		}

	}
?>