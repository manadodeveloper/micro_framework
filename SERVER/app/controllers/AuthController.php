<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * AuthController
 * Berfungsi sebagai controller authentifikasi
 */

	use core\Validator;
	use core\Auth;

	class AuthController extends core\Controller
	{

        /**
         * index()
         * Berfungsi menampilkan output konten halaman login
         *
         * @param tidak ada
         * @return output konten halaman login
         */
		public function index()
		{
			//mencari nama aplikasi
			$config_system = $this->config('system.cfg');

			//tanpa template dengan parameter nama aplikasi
			$this->view('login/index.tmpl',$config_system)->render();
			//$this->view("login/index")->render();
		}


        /**
         * login()
         * Berfungsi menghasilkan response  hasil login dengan format json
         *
         * @param tidak ada
         * @return output konten halaman login
         */
		public function login()
		{
	        $response = [
	            'status'     => 'error',
	            'desc'    => '',		
	        ];//end response

			$post = $_POST;
			$rules = 	[
							'email' => ['required','email','min-length' =>3],
							'password' => ['required'],
			];//end rules
			//opsional alias
			$alias = [
				    'email' => 'E-mail',
				    'password' => 'Password ',
			];//end alias


			$validate = new Validator($rules,$post,$alias);
			if($validate->error())
			{
				$response['status'] = "error";
				foreach ($validate->error() as $error) 
				{
					# code...
					$response['desc'] .= $error."<br />";
				}
			}
			else
			{
				//login menggunakan email dan password
				$auth_data = [
					'email'=>$post['email'],
					'password'=>$post['password'],
				];//end auth_data
	 			Auth::attempt($auth_data);
				if(Auth::status())
				{
			        $response = [
			            'status'     => 'success',
			            'desc'    => 'login berhasil',		
			        ];//end response
				}
				else
				{
					$response['desc'] .= Auth::error();
				}
			}
			$this->toJSON($response)->render();
		}


        /**
         * logout()
         * Berfungsi untuk logout. menghapus semua session dan di direct ke login
         *
         * @param tidak ada
         * @return output konten halaman login
         */
		public function logout()
		{
			if(Auth::status())
			{
				Auth::logout();
			}
			$this->redirect('/login');
		}
	}
?>