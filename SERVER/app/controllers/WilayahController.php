<?php
	use core\Auth;

	use Mpdf\QrCode\Output;


	class WilayahController extends core\Controller
	{

		use helper\ArrayHelper;
		private $model;

		public function __construct()
		{
			$this->model = $this->model("WilayahModel");
		}


		/**
		* index()
		* Berfungsi menampilkan output konten halaman wilayah
		*
		* @param tidak ada
		* @return output konten halaman wilayahpage
		*/
		public function index()
		{
			$this->view('wilayah/index.tmpl')->render();
		}


		/**
		 * list()
		 * Berfungsi untuk mengabil data dari model kemudian dirender dengan format json datatables
		 *
		 * @param tidak ada     
		 *
		 * @return json datatables;    
		 */
		public function list()
		{
			$this->toJSON($this->model->datatables())->render();
		}


		/**
		 * create()
		 * berfungsi untuk menampilkan halaman create
		 *
		 * @param tidak ada     
		 *
		 * @return view;    
		 */
		public function create()
		{	
			$this->view('wilayah/create.tmpl')->render();
		}


		/**
		 * store()
		 * berfungsi input data
		 *
		 * @param tidak ada     
		 * input diambil dari $_POST array.script ada di file model
		 * validasi masuk kedalam file model
		 * 
		 * @return json;    
		 */
		public function store()
		{	
			$this->toJSON($this->model->create())->render();
		}


		/**
		 * edit(String $_model_id)
		 * berfungsi untuk menampilkan halaman edit dengan paramater model ID
		 *
		 * @param String $_model_id     
		 *
		 * @return view;    
		 */
		public function edit($_model_id)
		{
			$data = $this->model->detail($_model_id);
			if($data['status'] == "success")
			{
				$this->view('wilayah/edit.tmpl',$data['data'])->render();
			}
			else
			{
				$this->view('error/index',$data)->render();
			}
		}


		/**
		 * update()
		 * berfungsi update data
		 *
		 * @param tidak ada     
		 * input diambil dari $_POST array.script ada di file model
		 * validasi masuk kedalam file model
		 * 
		 * @return json;    
		 */
		public function update()
		{
			$this->toJSON($this->model->update())->render();
		}


		/**
		 * destroy(String $_model_id)
		 * berfungsi untuk menampilkan halaman edit dengan paramater model ID
		 *
		 * @param String $_model_id     
		 *
		 * @return view;    
		 */
		public function destroy($_model_id)
		{
			$this->toJSON($this->model->destroy($_model_id))->render();
		}


		

	}