INSERT INTO `menu` (`menu_id`, `menu_parent_id`, `menu_icon`, `menu_title`, `menu_url`, `menu_order`, `menu_keterangan`, `menu_status`) VALUES
(1, 0, ' fa fas fa-cubes ', 'MASTER', '#', '0', '', 1),
(2, 1, '', 'WILAYAH KERJA', '/wilayah', '1', '', 1),
(3, 1, '', 'PENGGUNA', '/user', '4', '', 1),
(4, 1, '', 'MOBILE DEVICE', '/mobile', '5', '', 1),
(5, 1, '', 'HAK AKSES', '/role', '2', '', 1),
(6, 0, ' fa fas fa-chart-line ', 'DASHBOARD', '/role', '99', '', 1);
