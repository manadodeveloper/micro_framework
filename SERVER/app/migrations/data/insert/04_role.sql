


INSERT INTO `permissions` (`id`, `name`, `menu_id`, `category`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'master-menu', 1, 'menu-helper', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(2, 'wilayah-list', 2, 'wilayah', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(3, 'wilayah-edit', NULL, 'wilayah', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(4, 'wilayah-delete', NULL, 'wilayah', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(5, 'user-list', 3, 'user', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(6, 'user-edit', NULL, 'user', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(7, 'user-delete', NULL, 'user', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(8, 'user-aktifasi', NULL, 'user', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(9, 'user-hak-akses', NULL, 'user', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(10, 'user-upload-photo', NULL, 'user', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(11, 'user-ganti-password', NULL, 'user', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(12, 'mobile-list', 4, 'mobile', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(13, 'mobile-delete', NULL, 'mobile', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(14, 'mobile-register', NULL, 'mobile', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(15, 'mobile-aktivasi', NULL, 'mobile', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(16, 'mobile-test-notifikasi', NULL, 'mobile', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(17, 'role-list', 5, 'role', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(18, 'dashboard-list', 6, 'dashboard', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41'),
(19, 'mode-edit', NULL, 'mode', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41');

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41');



INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1);


INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES (1, 'App\\User', 1) ;


INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(2, 'HELPDESK', 'web', '2020-07-01 06:38:41', '2020-07-01 06:38:41');
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(2, 2),
(3, 2);


