
CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'id permission',
  `name` varchar(255)  NOT NULL COMMENT 'nama permission',
  `menu_id` smallint(6) DEFAULT NULL COMMENT 'foreign key menu_id',
  `category` varchar(255)  DEFAULT NULL COMMENT 'kategori',
  `guard_name` varchar(255)  NOT NULL COMMENT 'nama guard',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB ;


CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'roles id',
  `name` varchar(255)  NOT NULL COMMENT 'nama roles',
  `guard_name` varchar(255)  NOT NULL COMMENT 'nama guard',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB ;


CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL COMMENT 'foreign key permission_id',
  `role_id` int(10) UNSIGNED NOT NULL COMMENT 'foreign key role_id'
) ENGINE=InnoDB ;


CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL COMMENT 'foreign key role_id',
  `model_type` varchar(255)   NULL COMMENT 'keterangan tipe',
  `model_id` bigint(20) UNSIGNED NOT NULL COMMENT 'foreign key model_id'
) ENGINE=InnoDB  ;


ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);



ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);


ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `spatie_roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
