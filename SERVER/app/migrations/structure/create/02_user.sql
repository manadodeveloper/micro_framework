
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'id pengguna',
  `nip` varchar(30)  NOT NULL COMMENT 'nomor induk pegawai',
  `name` varchar(255)  NOT NULL COMMENT 'nama pengguna',
  `full_name` varchar(34)  NOT NULL COMMENT 'nama lenggkap',
  `email` varchar(255)  NOT NULL COMMENT 'email',
  `password` varchar(255)  NOT NULL COMMENT 'password yang telah dienkripsi',
  `avatar` varchar(255)  DEFAULT NULL COMMENT 'alamat url avatar',
  `theme` varchar(10)  NOT NULL DEFAULT "1" COMMENT '0:dark,1:ligth',
  `wilayah` varchar(10)  NOT NULL COMMENT 'kode wilayah',
  `firebase_id` varchar(255)  DEFAULT NULL COMMENT 'id firebase',
  `session_id` varchar(255)  DEFAULT NULL COMMENT 'nilai session waktu login',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0:belum aktif, 1:aktif, 2:diblokir',
  `remember_token` varchar(100)  DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB ;

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

  ALTER TABLE `users` CHANGE `id` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT; 