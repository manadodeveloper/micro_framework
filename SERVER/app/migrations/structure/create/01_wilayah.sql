
CREATE TABLE IF NOT EXISTS `wilayah` (
  `wilayah_id` VARCHAR(10) NOT NULL COMMENT 'id/kode wilayah',
  `wilayah_nama` VARCHAR(60) NULL COMMENT 'nama wilayah',
  `wilayah_parent` VARCHAR(10) NULL COMMENT 'parent wilayah',
  `wilayah_kontak` VARCHAR(50) NULL COMMENT 'alamat / telp wilayah',
  PRIMARY KEY (`wilayah_id`))
ENGINE = InnoDB;
