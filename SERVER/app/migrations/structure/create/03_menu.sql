
CREATE TABLE `menu` (
  `menu_id` smallint(5) UNSIGNED NOT NULL  COMMENT 'id menu',
  `menu_parent_id` smallint(6) NOT NULL DEFAULT 0 COMMENT 'parent menu id',
  `menu_icon` varchar(255) NOT NULL DEFAULT '' COMMENT 'class css icon',
  `menu_title` varchar(255) NOT NULL COMMENT 'judul menu',
  `menu_url` varchar(255) NOT NULL COMMENT 'url menu',
  `menu_order` varchar(255) NOT NULL DEFAULT '0' COMMENT 'urutan menu',
  `menu_keterangan` varchar(255) DEFAULT NULL COMMENT 'keterangan menu',
  `menu_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0:disable,1:aktif'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;