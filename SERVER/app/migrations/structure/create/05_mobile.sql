
-- -----------------------------------------------------
-- `mobile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mobile` (
  `mobile_id` INT(6) NOT NULL AUTO_INCREMENT COMMENT 'id mobile device',
  `user_id` VARCHAR(20) NOT NULL COMMENT 'id model / user',
  `app_id` VARCHAR(45) NULL DEFAULT NULL COMMENT 'app id nama aplikasi',
  `app_version` TINYINT(2) NULL DEFAULT NULL COMMENT 'versi aplikasi',
  `firebase_token` VARCHAR(255) NOT NULL COMMENT 'token firebase',
  `device_model` VARCHAR(63) NOT NULL COMMENT 'model mobile',
  `device_merk` VARCHAR(16) NOT NULL COMMENT 'merk mobile',
  `device_sdk` TINYINT(2) NOT NULL COMMENT 'versi SDK development compile',
  `device_system_version` VARCHAR(45) NULL DEFAULT NULL COMMENT 'versi IOS/ANDROID',
  `device_id` VARCHAR(45) NULL DEFAULT NULL COMMENT 'IMEI device, kalau tidak ada memakai UUID',
  `device_uuid` VARCHAR(45) NULL DEFAULT NULL COMMENT 'UUID device',
  `language` VARCHAR(45) NULL DEFAULT NULL COMMENT 'bahasa',
  `country` VARCHAR(45) NULL DEFAULT NULL COMMENT 'negara',
  `carrier` VARCHAR(45) NULL DEFAULT NULL COMMENT 'GSM/CDMA',
  `carrier_id` VARCHAR(45) NULL DEFAULT NULL COMMENT 'IMEI',
  `screen_size` DECIMAL(4,2) NULL DEFAULT NULL COMMENT 'ukuran layar dalam inch',
  `screen_resolution` VARCHAR(45) NULL DEFAULT NULL COMMENT 'resolusi layar',
  `screen_dpi` DECIMAL(3,2) NULL DEFAULT NULL COMMENT 'kedalaman pixel',
  `date_register` DATETIME NOT NULL COMMENT 'tanggal register',
  `register_status` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '0 baru masuk; 1 aktif;2:disable; 9:blokir',
  PRIMARY KEY (`mobile_id`))
ENGINE = InnoDB;