<?php

/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * Berfungsi untuk memgembalikan array mysqli config
 */

	return 
		[//begin return
			'user' 		=> 'root',
			'password' 	=> '',
			'host' 		=> '127.0.0.1',
			'name' 		=> 'latsar',
			'port' 		=> '3306',
			'debug' 	=> true,
		];//end return