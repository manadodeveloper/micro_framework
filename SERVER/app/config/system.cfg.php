<?php

/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * Berfungsi untuk memgembalikan system role config
 */

	return 
		[//begin return
            'name'      => 'TRIMS',
            'template'  => strtolower('atlantis'), 
            'author'    => 'ahmad basarudin',
            'email'     => 'ahmad.basarudin@gmail.com',
            'version'   => "1.0",
		];//end return