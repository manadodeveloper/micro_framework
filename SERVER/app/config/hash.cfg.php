<?php

/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * configurasi hash
 */


	return 
		[//begin return
			'type' 		=> PASSWORD_BCRYPT,
			'options' 	=> ['cost' => 10],
		];//end return