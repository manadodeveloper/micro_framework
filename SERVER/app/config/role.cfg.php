<?php

/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * Berfungsi untuk memgembalikan array role config
 */
	return 
	[
		'table' =>
		[
			'model-has-role' 					=> 'model_has_roles',
			'role' 								=> 'roles',
			'role-has-permissions' 				=> 'role_has_permissions',
			'permissions' 						=> 'permissions',
			'menu' 								=> 'menu',
		],//end table


	];//end return