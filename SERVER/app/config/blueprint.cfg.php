<?php

/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * Berfungsi untuk memgembalikan blueprint config
 * Digunakan pada saat CLI Crud
 */

	return 
		[//begin return
            'TAG-BEGIN'		=> 	'#BEGIN-BLOCK-CRUD-',
            'TAG-END'  		=> 	'#END-BLOCK-CRUD-', 
            'TABLE'    		=> 	[
            						"table-create"	=> BASE_ROOT."/app/migrations/structure/create/",
            						"table-drop"	=> BASE_ROOT."/app/migrations/structure/drop/",
            						"table-insert"	=> BASE_ROOT."/app/migrations/data/insert/",
            						"table-empty"	=> BASE_ROOT."/app/migrations/data/empty/",

            					],
			'ROUTE'			=>	[
									"route-web"		=> BASE_ROOT."/app/routes/web.php",
								],
			'CONTROLLER'	=>	BASE_ROOT."/app/controllers/<<MODUL_NAME>>Controller.php",
			'MODEL'			=>	BASE_ROOT."/app/models/<<MODUL_NAME>>Model.php",
			'VIEW'			=>	[
									"view-index"	=> BASE_ROOT."/app/views/<<LOWER_MODUL_NAME>>/index.tmpl.php",
									"view-create"	=> BASE_ROOT."/app/views/<<LOWER_MODUL_NAME>>/create.tmpl.php",
									"view-edit"		=> BASE_ROOT."/app/views/<<LOWER_MODUL_NAME>>/edit.tmpl.php",
								],
			'BLUEPRINT'		=>	[
									'table-create'	=> BASE_ROOT."/app/blueprints/table.create.blueprint",
									'table-drop'	=> BASE_ROOT."/app/blueprints/table.drop.blueprint",
									'route-web'		=> BASE_ROOT."/app/blueprints/route.web.blueprint",
									'controller'	=> BASE_ROOT."/app/blueprints/controller.blueprint",
									'model'			=> BASE_ROOT."/app/blueprints/model.blueprint",
									'view-index'	=> BASE_ROOT."/app/blueprints/view.index.blueprint",
									'view-create'	=> BASE_ROOT."/app/blueprints/view.create.blueprint",
									'view-edit'		=> BASE_ROOT."/app/blueprints/view.edit.blueprint",
								],
		];//end return