<?php
/**
 * TRIMS Framework
 *
 * @author 		basarudin
 * @copyright 	2020
 */
	
	use core\Router;

	/*
		contoh dengan parameter
		Router::get('/salam/edit/{id}/{kedua}/{ketiga}', function($a,$b,$c){
			echo 'hello world' .$b.$c;
		});

		Router::get('/user/login/{id}/{nama}', 'users@index');
		diclass user method index perlu 2 parameter
		public function index($_id,$_nama)
	*/

	Router::get('/error', 'ErrorController'); //error

	Router::get('/', 'AuthController'); //view login	
	Router::get('/login', 'AuthController'); //view login	
	Router::post('/login', 'AuthController@login'); //json login
	Router::get('/logout', 'AuthController@logout'); //controller logout


	Router::middleware(['auth'=>true])->get('/home', 'HomeController'); //halaman utama setelah login
	Router::middleware(['auth'=>true])->get('/blankpage', 'HomeController@blankPage'); 


	Router::middleware(['auth'=>true,"hasPermission"=>["user-list",],])->get('/user', 'UserController'); //view table user
	Router::post('/user/list', 'UserController@list'); //json data user
	Router::middleware(['auth'=>true])->get('/user/edit/{id}', 'UserController@edit'); //json response user active
	Router::post('/user/update', 'UserController@update'); //json response user active
	Router::get('/user/create', 'UserController@create'); //json response user active
	Router::post('/user/create', 'UserController@store'); //json response user active

	Router::middleware(['auth'=>true])->get('/user/theme', 'UserController@formTheme'); //form theme
	Router::post('/user/theme', 'UserController@storeTheme'); //store theme
	Router::post('/user/block/{id}', 'UserController@block'); //json response user active
	Router::post('/user/active/{id}', 'UserController@active'); //json response user active



	Router::get('/role', 'RoleController'); //halaman utama role
	Router::post('/role/list', 'RoleController@list'); //json data role


	Router::get('/mobile', 'MobileController'); //halaman utama role
	Router::post('/mobile/list', 'MobileController@list'); //json data role


	Router::get('/dashboard', 'DashboardController'); //halaman utama role

	/*--------------BEGIN EXTENDED MODUL--------------*/



	#BEGIN-BLOCK-CRUD-Wilayah
	Router::get('/wilayah', 'WilayahController'); //halaman utama Wilayah
	Router::post('/wilayah/list', 'WilayahController@list'); //json backend untuk feed datatables
	Router::get('/wilayah/create', 'WilayahController@create'); //halaman form tambah Wilayah
	Router::post('/wilayah/create', 'WilayahController@store'); //json backend create Wilayah
	Router::post('/wilayah/destroy/{id}', 'WilayahController@destroy'); //json backend hapus berdasarkan model id
	Router::get('/wilayah/pdf', 'WilayahController@pdf'); //generate pdf file Wilayah
	Router::get('/wilayah/csv', 'WilayahController@csv'); //generate csv file Wilayah
	#END-BLOCK-CRUD-Wilayah
	


	
	Router::checkFoundPage();