<?php

        
    /**
     * BEGIN Config
     */ 

    /**
     * judul halaman
     */ 
    $main_title = "Wilayah";


    /**
     * URL datatable beserta button
     */ 
    $URL =  [

                "index"      => "/wilayah",
                "list"      => "/wilayah/list",
                "create"    => "/wilayah/create",
                "edit"      => "/wilayah/edit",
                "destroy"   => "/wilayah/destroy",
            ];
        
    /**
     * kolom data yang ditampilkan
     */ 
    $AJAX_FIELD = ["wilayah_id","wilayah_nama","button"];


    /**
     * searching / filtering jquery id input
     * key => value dimana:
     *      key = nama kolom sql. boleh di edit di modelnya
     *      value = id input untuk jquery
     */ 
    $AJAX_INPUT =   [
                        "wilayah_id"    => "kode",
                        "wilayah_nama"    => "nama",
                    ];

    $component_table =  [
                            "id"        => "table",
                            "class"     => "table table-bordered table-head-bg-gray  table-hover",
                            "head"      =>  [
                                                ["title"     => "KODE","style"     => "",],
                                                ["title"     => "NAMA","style"     => "",],
                                                ["title"     => "","style"     => "width:10px;",],
                                            ],//end head
                            "selection" =>  [
                                                "<div class='form-group'> <input id='kode' type='text' class='form-control'   ></div>",
                                                "<div class='form-group'> <input id='nama' type='text' class='form-control'  ></div>",
                                                "",
                                                "",
                                            ],
                        ];//end table


    $swal_destroy_success   =   [
                            "type"      => "success", //success atau error
                            "message"   => "data berhasil dihapus",//string boleh berupa tag html
                            "onclose"   => "document.location='".$URL['index']."';",//harus diakhiri dengan semicolon ;
                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                        ];
    $swal_destroy_error     =   [
                            "type"      => "error", //success atau error
                            "message"   => "msg.desc",
                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                        ];
    $js_ajax        =   [
                            "url"       => "'{$URL['destroy']}/'+record_id",//url untuk store data
                            "data"      => "''",//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert($swal_destroy_success),
                            "onerror"   => core\Template::sweetalert($swal_destroy_error),
                        ];

    /**
     * END Config
     */ 
    $string_js_column = "";
    $string_js_input = "";
    foreach ($AJAX_FIELD as $value ) 
    {
        $string_js_column .= "{'data': '{$value}' },";
    }
    foreach ($AJAX_INPUT as $key => $value) 
    {
        $string_js_input .= "'{$key}': $('#{$value}').val() ,";
    }


?> 

{% extends themes/template %}

{% block main-button %}
    <button id='button-create' class='btn btn-success  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-plus'></i>
        </span>
        Tambah 
    </button>
{% endblock %}
{% block main-title %}
    {{$main_title}}
{% endblock %}

{% block main-content %}
    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>
                <div class='row'>

                    <div class='col-md-12'>
                        <div class='card'>
                            <div class='card-body'>
                                <!-- mulai table -->
                                <div >
                                    {{ core\Form::datatable($component_table) }}
                                    
                                </div>
                                <!-- end table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{% block css %}
{% endblock %}

{% block javascript %}

    <script src='{{ BASE_URL }}assets/js/plugin/datatables/datatables.min.js'></script>

    <script >

        $(document).ready(function() 
        {        
            
            if($('#button-create').length)
            {
                $('#button-create').click(function() 
                {
                    document.location='{{$URL['create']}}';
                });
            };
            datatablesDraw();

            //jika input searching/filtering ditekan enter
            if($('input').length)
            {
                $('input').keypress(function(e) 
                {
                    if (e.which == 13)
                    {
                        datatablesDraw();
                        return false;
                    }
                });
            }
            //jika select2 berubah nilai
            if($('.select2').length)
            {

            }
                $('.select2').change(function () 
                {
                    datatablesDraw();
                });


            function datatablesDraw()
            {

                if (  $.fn.DataTable.isDataTable( '#table' ) ) 
                {
                    $('#table').DataTable().destroy();
                }

                $('#table').DataTable( {
                    "processing"    : true,
                    "serverSide"    : true,    
                    "searching"     : false,  
                    'lengthMenu'    : [
                                        [5, 10, 25, 50, -1], 
                                        [5, 10, 25, 50, 'All']
                                      ],/*end lengthMenu*/
                    "ajax"          : 
                                        {
                                            "url"   : "{{$URL['list']}}",
                                            "type"  : "POST",
                                            "data"  : 
                                            {   
                                                {{$string_js_input}}

                                            }
                                        },/*end ajax*/
                    "columns"       : [ 
                                        {{$string_js_column}} 
                                      ],/*end columns*/
                    "order"         : [[0, 'asc']],/*end order*/
                    "columnDefs"    : [
                                        { targets: [0], orderable: true},
                                        { targets: '_all', orderable: false }
                                      ],/*end columnDefs*/
                    error:function(err, status){
                        alert(err);
                        },

                } );
            }

            $('#table tbody').on( 'click', '.button-edit', function () 
            {
                record_id = $(this).attr('record-id');
                document.location="{{$URL['edit']}}/"+record_id;

            });

            $('#table tbody').on( 'click', '.button-destroy', function () 
            {
                record_id = $(this).attr('record-id');
                record_name = $(this).attr('record-name');
                record_name = $(this).parent().parent().parent().parent().find(".record-name").html(); 


                Swal.fire({
                        title: 'Konfirmasi',
                        html: "Apakah  yakin  objek <b>"+record_name +"</b> dihapus?",
                        type: 'warning',
                        onOpen: () => {
                            var zippi = new Audio('{{ BASE_URL }}assets/sound/warning.mp3')
                            zippi.play();
                        },
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, dihapus saja',
                        cancelButtonText: 'Batal'
                    }).then((result) => {
                        if (result.value) 
                        {

                            {{core\Template::ajax($js_ajax)}}
                        }
                });

            });
        } );

        
    </script>
{% endblock %}