<?php

        
    /**
     * -------------------BEGIN CONFIG-------------------
     */ 

    /**
     * judul halaman
     */ 
    $main_title = "Tambah Wilayah";


    /**
     * URL 
     */ 
    $URL =  [
                "store"    => "/wilayah/create",//alamat memasukan data
                "index"    => "/wilayah",//jika sudah berhasil, maka halaman akan dialihkan
            ];
    $swal_success   =   [
                            "type"      => "success", //success atau error
                            "message"   => "data berhasil dimasukan",//string boleh berupa tag html
                            "onclose"   => "document.location='".$URL['index']."';",//harus diakhiri dengan semicolon ;
                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                        ];
    $swal_error     =   [
                            "type"      => "error", //success atau error
                            "message"   => "msg.desc",
                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                        ];
    $js_ajax        =   [
                            "url"       => "'{$URL['store']}'",//url untuk store data
                            "data"      => '$("#form-create").serialize()',//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert($swal_success),
                            "onerror"   => core\Template::sweetalert($swal_error),
                        ];

    /**
     * masukan configurasi komponen disini 
     */ 
    $controller = new core\Controller;
    $model = $controller->model("WilayahModel");
    $wilayah = $model->pluck();
    $component_wilayah_parent   = [
                                    "id"        => 'wilayah-parent',
                                    "name"      => 'wilayah-parent',
                                    "class"     => ' select2 form-control ',
                                    "prefix"    =>   [""=>"--tanpa induk--"],
                                    "data"      =>   $wilayah,
                                    "selected"  => "",
                                  ];//end array status
    $component_wilayah_id   = [
                                    "name"      => 'wilayah-id',
                                    "class"     => 'form-control',
                              ];
    $component_wilayah_name     = [
                                    "name"      => 'wilayah-nama',
                                    "class"     => 'form-control',
                                  ];

    /**
     * -------------------END CONFIG-------------------
     */ 
?> 
{% extends themes/template %}

{% block main-title %}
    {{$main_title}}
{% endblock %}

{% block main-button %}

    <button id='button-back' class='  btn btn-warning  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-caret-left'></i>
        </span>
        Kembali
    </button>
    <button id='button-store' class='btn btn-success  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-plus'></i>
        </span>
        Tambah 
    </button>   
{% endblock %}


{% block css %}
    <!-- Select2 -->
    <link href="{{ BASE_URL }}assets/js/plugin/select2/css/select2.min.css" rel="stylesheet">
{% endblock %}

{% block javascript %}
    <script src="{{ BASE_URL }}assets/js/plugin/select2/js/select2.full.min.js"></script>
    <script >
        $(document).ready(function() 
        {
            if($('.select2').length)
            {
                $('.select2').select2();
            };
            if($('#button-back').length)
            {
                $('#button-back').click(function() 
                {
                    document.location="{{$URL['index']}}";
                });
            };
            if($('#button-store').length)
            {
                $('#button-store').click(function() 
                {
                    {{core\Template::ajax($js_ajax)}}
                });
            };

        });
    </script>
{% endblock %}

{% block main-content %}
    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>

                <div class='row'>

                    <div class='col-md-12'>
                        <div class='card bg-warning text-white'>
                            <div class='card-body'>
                                <div >
                                    <h4> Petunjuk Pengisian</h4>
                                    <b>kode wilayah</b> parent merupakan induk dari wilayah yang akan diisi. dibiarkan kosong jika wilayah yang akan diisi merupakan wilayah induk<br />
                                    <b>kode wilayah</b> diisi dengan 6 angka  berdasarkan kode dari masing masing wilayah.<br />
                                    <b>nama wilayah</b> diisi dengan alpanumerik yang merupakan nama dari wilayah tersebut.<br />
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class='card'>
                            <div class='card-header'>
                                <div class='card-title'>Form Wilayah Kerja</div>
                            </div>
                            <div class='card-body'>
                                <div >
                                    
                                    <form id='form-create' action='' method='post'><div >
                                        <!-- /.box-header -->
                                        <div class='box-body'>
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                            <label>Wilayah Induk:</label>
                                                        <div class='input-group mb-3'>
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="fa fas fa-home"></i>
                                                                    </span>
                                                            </div>

                                                            {{ core\Form::select($component_wilayah_parent) }}

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='col-md-4'>
                                                    <div class="form-group">
                                                        <label>Kode Wilayah:</label>

                                                        <div class='input-group mb-3'>
                                                            <div class='input-group-prepend'>
                                                                <span class='input-group-text' >
                                                                    <i class='fa fas fa-key'></i>
                                                                </span>
                                                            </div>

                                                            {{ core\Form::text($component_wilayah_id) }}

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='col-md-8'>
                                                    <div class="form-group">
                                                            <label>Nama Wilayah:</label>
                                                        <div class='input-group mb-3'>
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="fa fas fa-text-width"></i>
                                                                    </span>
                                                            </div>

                                                            {{ core\Form::text($component_wilayah_name) }}

                                                        </div>
                                                    </div>
                                                </div>
                                                

                                                
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}