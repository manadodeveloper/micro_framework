{% extends themes/template %}



{% block main-content %}


    <!-- BEGIN PAGE CONTENT -->
        <div class='main-panel'>
            <div class='content'>
                <div class='page-inner'>
                    <div class='row'>
                        <div id='myCarousel' class='carousel slide' data-ride='carousel'>
                            <div class='carousel-inner'>
                                <div class='carousel-item active'>
                                    <div class='item item1 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-4'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/presenter.png' alt='sofa' class='w-100 img img-responsive' />
                                                        </div>
                                                    </div>
                                                    <div class='col-md-8'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                <h2  mx-auto'>Selamat datang  {{ Auth::user()['full_name'] }}</h2>
                                                                <p  mx-auto'>Selamat berkarya, jadikan perkerjaan anda berkah. Kontribusi anda akan sangat bermanfaat bagi perusahaan. Semoga aktifitas kita hari ini dicatat sebagai amal kebaikan. amien</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                                <div class='carousel-item'>

                                    <div class='item item2 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-12'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/app.png' alt='sofa' class='img img-responsive' class='w-100 img img-responsive'/>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-12'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                

                                                                <h2>Simple Usefull Powerfull</h2>
                                                                <p>Sistem ini dibuat sesederhana mungkin yang ditujukan untuk membantu perkerjaan administrasi komputer dan layanan helpdesk. </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                    
                                </div>
                                <div class='carousel-item'>

                                    
                                    <div class='item item3 active' >
                                        <div class='fill'>
                                            <div class='inner-content '>
                                                <div class='row'>
                                                    <div class='col-md-12'>

                                                        <div class='carousel-img'>
                                                            <img src='assets/img/slider/helpdesk.png' alt='sofa' class='img img-responsive' class='w-100 img img-responsive'/>
                                                        </div>
                                                    </div>
                                                    <div class='col-md-12'>
                                                        <div class='row h-100'>
                                                            <div class='carousel-desc col-sm-12 my-auto'>

                                                                <h2>Anda Butuh Bantuan?</h2>
                                                                <p>Silahkan menghubungi administrator jika anda mengalami kesulitan dalam mengoperasikan aplikasi sistem ini.</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                            </div>
                                        </div>  
                                    </div>
                                    
                                </div>

                                <a class='carousel-control-prev' href='#myCarousel' role='button' data-slide='prev'>
                                    <span class='carousel-control-prev-icon' aria-hidden='true'></span>
                                    <span class='sr-only'>Previous</span>
                                </a>
                                <a class='carousel-control-next' href='#myCarousel' role='button' data-slide='next'>
                                    <span class='carousel-control-next-icon' aria-hidden='true'></span>
                                    <span class='sr-only'>Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  	<!-- END PAGE CONTENT -->
{% endblock %}

{% block css %}
	<style type="text/css">
		

		.mid-content{
			height: 100%;
			width: 100%;
				margin: 10% 0 0 0;
				text-align: center;
				float: none
		}
		.page-wrapper.toggled .page-content {
		    height: 90%;
		    overflow: hidden;
		}


		.carousel-inner,.carousel,.item,.container,.fill {
			height:100%;
			width:100%;
			background-position:center center;
			background-size:cover;
		}
		.slide-wrapper{
			display:inline;
		}
		.slide-wrapper .container{
			padding:0px;
		}

		/*------------------------------ vertical bootstrap slider----------------------------*/

		.carousel-inner> .item.next ,  .carousel-inner > .item.active.right{ transform: translateY(100%); -webkit-transform: translateY(100%); -ms-transform: translateY(100%);
		-moz-transform: translateY(100%); -o-transform: translateY(100%);  top: 0;left:0;}
		.carousel-inner > .item.prev ,.carousel-inner > .item.active.left{ transform: translateY(-100%); -webkit-transform: translateY(-100%);  -moz-transform: translateY(-100%);
		-ms-transform: translateY(-100%); -o-transform: translateY(-100%); top: 0; left:0;}
		.carousel-inner > .item.next.left , .carousel-inner > .item.prev.right , .carousel-inner > .item.active{transform:translateY(0); -webkit-transform:translateY(0);
		-ms-transform:translateY(0);-moz-transform:translateY(0); -o-transform:translateY(0); top:0; left:0;}

		/*------------------------------- vertical carousel indicators ------------------------------*/
		.carousel-indicators{
		position:absolute;
		top:0;
		bottom:0;
		margin:auto;
		height:20px;
		right:10px; left:auto;
		width:auto;
		}
		.carousel-indicators li{display:block; margin-bottom:5px; border:1px solid #00a199; }
		.carousel-indicators li.active{margin-bottom:5px; background:#00a199;}
		/*-------- Animation slider ------*/

		.animated{
			animation-duration:3s;
			-webkit-animation-duration:3s;
			-moz-animation-duration:3s;
			-ms-animation-duration:3s;
			-o-animation-duration:3s;
			visibility:visible;
			opacity:1;
			transition:all 0.3s ease;
		}
		.carousel-img{   
			 display: inline-block;
				margin: 0 auto;
				width: 100%;
				text-align: center;
			}
		.item img{
			margin:auto;
			visibility:hidden; 
			opacity:0; 
			transition:all 0.3s ease; 
			-webkit-transition:all 0.3s ease; 
			-moz-transition:all 0.3s ease; 
			-ms-transition:all 0.3s ease; 
			-o-transition:all 0.3s ease;
		}
		.item2 .carousel-img img , .item1.active .carousel-img img{
			max-height:300px;
		}
		.item2.active .carousel-img img.animated{visibility:visible; opacity:1; transition:all 1s ease; -webkit-transition:all 1s ease; -moz-transition:all 1s ease; -ms-transition:all 1s ease; -o-transition:all 1s ease;
		animation-duration:2s; -webkit-animation-duration:2s; -moz-animation-duration:2s; -ms-animation-duration:2s; -o-animation-duration:2s; animation-delay:0.3s ; -webkit-animation-delay:0.3s;
		-moz-animation-delay:0.3s;-ms-animation-delay:0.3s; }
		.item .carousel-desc{color:#333; text-align:center;}
		.item  h2{font-size:50px; animation-delay:1.5s;animation-duration:1s; }
		.item  p{animation-delay:2.5s;animation-duration:1s; width:50%; margin:auto;}

		.item3 .carousel-img img , .item2.active .carousel-img img{max-height:300px;}
		.item3.active .carousel-img img.animated{visibility:visible; opacity:1; transition:all 0.3s ease; animation-duration:3s; animation-delay:0.3s}
		.item3 h2 , item2.active h2{visibility:hidden; opacity:0; transition:all 5s ease;}
		.item3.active h2.animated{visibility:visible; opacity:1;  animation-delay:3s;}

		.item .fill{padding:0px 30px; display:table; }
		.item .inner-content{display: table-cell;vertical-align: middle;}
		.item1 .col-md-6{float:none; display:inline-block; vertical-align:middle; width:49%;}



		.item1 .carousel-img img , .item1.active .carousel-img img{
			max-height:500px;
		}
		.item1.active .carousel-img img.animated{visibility:visible; opacity:1; transition:all 0.3s ease; animation-duration:2s; animation-delay:0.3s}
		.item1 h2 , item3.active h2{visibility:hidden; opacity:0; transition:all 5s ease; }
		.item.item1.carousel-desc{text-align:left;}
		.item1.active h2.animated{visibility:visible; opacity:1;  animation-delay:1.5s}
		.item1 p , item3.active p{visibility:hidden; opacity:0; transition:all 5s ease; width:100%;  }
		.item1.active p.animated{visibility:visible; opacity:1;  animation-delay:2.5s;}

		@media(max-width:991px)
		{
			.item .carousel-desc , .item.item1 .carousel-desc{text-align:center;}
			.item .carousel-desc p {width:80%;}
			.item1 .col-md-6{width:100%; text-align:center;}
		}
		@media(max-width:768px)
		{
		.item .carousel-img img, .item.active .carousel-img img{max-height:155px;}
		.item  h2{font-size:30px; margin-top:0px;}
		.item .carousel-desc p{width:100%; font-size:12px;}
		}
		@media(max-width:480px)
		{
		.item  h2{font-size:30px;}
		.item .carousel-desc p{width:100%;}
		}

	</style>
{% endblock %}

{% block javascript %}
	<script type="text/javascript">
		
		// scroll slides on mouse scroll 
		$('#myCarousel').bind('mousewheel DOMMouseScroll', function(e){

	        if(e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
	            $(this).carousel('prev');
	        }
	        else{
	            $(this).carousel('next');
	        }
	    });

		//scroll slides on swipe for touch enabled devices 

	 	$("#myCarousel").on("touchstart", function(event){
	        var yClick = event.originalEvent.touches[0].pageY;
	    	$(this).one("touchmove", function(event){
		        var yMove = event.originalEvent.touches[0].pageY;
		        if( Math.floor(yClick - yMove) > 1 ){
		            $(".carousel").carousel('next');
		        }
		        else if( Math.floor(yClick - yMove) < -1 ){
		            $(".carousel").carousel('prev');
		        }
			});
		    $(".carousel").on("touchend", function(){
		            $(this).off("touchmove");
		    });
		});

		//to add  start animation on load for first slide 
		$(function(){
			$.fn.extend({
				animateCss: function (animationName) {
					var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
					this.addClass('animated ' + animationName).one(animationEnd, function() {
						$(this).removeClass(animationName);
					});
				}
			});
			$('.item1.active img').animateCss('slideInDown');
			$('.item1.active h2').animateCss('zoomIn');
			$('.item1.active p').animateCss('fadeIn');
					 
		});

	 
	 	$("#myCarousel").on('slide.bs.carousel', function () {
			$.fn.extend({
				animateCss: function (animationName) {
					var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
					this.addClass('animated ' + animationName).one(animationEnd, function() {
						$(this).removeClass(animationName);
					});
				}
			});
		
	// add animation type  from animate.css on the element which you want to animate

			$('.item1 img').animateCss('slideInDown');
			$('.item1 h2').animateCss('zoomIn');
			$('.item1 p').animateCss('fadeIn');

			
			$('.item2 img').animateCss('zoomIn');
			$('.item2 h2').animateCss('zoomIn');
			$('.item2 p').animateCss('fadeIn');
			
			$('.item3 img').animateCss('fadeInLeft');
			$('.item3 h2').animateCss('fadeInDown');
			$('.item3 p').animateCss('fadeIn');
	    });
	</script>
{% endblock %}


