{% extends themes/template %}



{% block main-content %}

    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>
                <div class='row'>

                    <div class='col-md-12'>
                        <div class='card'>
                            <div class='card-header'>
                                <div class='card-title'>Card Title</div>
                                <div class='card-category'>Card Category</div>
                            </div>
                            <div class='card-body'>
                                Card Body
                            </div>
                            <div class='card-footer'>
                                <hr>
                                Card Footer
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
