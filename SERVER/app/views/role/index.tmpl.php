<?php
/**
 * TRIMS Framework
 *
 * @author      basarudin
 * @copyright   2020
 * 
 * index.tmpl.php
 * Berfungsi sebagai index view role
 */
        
    

    $main_title = "Hak Akses";

    $component_table =  [
                            "id"        => "table",
                            "class"     => "table table-bordered table-head-bg-gray  table-hover",
                            "head"      =>  [
                                                ["title"     => "Role","style"     => "",],
                                                ["title"     => "Permissions","style"     => "",],
                                                ["title"     => "Menu","style"     => "",],
                                            ],//end head
                            "data"      => $data,
                        ];//end table


?> 

{% extends themes/template %}

{% block main-button %}
{% endblock %}
{% block main-title %}
    {{$main_title}}
{% endblock %}

{% block main-content %}
    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>
                <div class='row'>

                    <div class='col-md-12'>
                        <div class='card'>
                            <div class='card-body'>
                                <!-- mulai table -->
                                <div >
                                    {{ core\Form::datatable($component_table) }}
                                    
                                </div>
                                <!-- end table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{% block css %}
{% endblock %}

{% block javascript %}

    <script src='{{ BASE_URL }}assets/js/plugin/datatables/datatables.min.js'></script>

    <script >

        $(document).ready(function() 
        {        
            
            if($('#button-create').length)
            {
                $('#button-create').click(function() 
                {
                    document.location='{{$URL['create']}}';
                });
            };
            datatablesDraw();

            //jika input searching/filtering ditekan enter
            if($('input').length)
            {
                $('input').keypress(function(e) 
                {
                    if (e.which == 13)
                    {
                        datatablesDraw();
                        return false;
                    }
                });
            }
            //jika select2 berubah nilai
            if($('.select2').length)
            {

            }
                $('.select2').change(function () 
                {
                    datatablesDraw();
                });


            function datatablesDraw()
            {

                if (  $.fn.DataTable.isDataTable( '#table' ) ) 
                {
                    $('#table').DataTable().destroy();
                }

                $('#table').DataTable( {
                    "processing"    : true,
                    "serverSide"    : true,    
                    "searching"     : false,  
                    'lengthMenu'    : [
                                        [5, 10, 25, 50, -1], 
                                        [5, 10, 25, 50, 'All']
                                      ],/*end lengthMenu*/
                    "ajax"          : 
                                        {
                                            "url"   : "{{$URL['list']}}",
                                            "type"  : "POST",
                                            "data"  : 
                                            {   
                                                {{$string_js_input}}

                                            }
                                        },/*end ajax*/
                    "columns"       : [ 
                                        {{$string_js_column}} 
                                      ],/*end columns*/
                    "order"         : [[0, 'asc']],/*end order*/
                    "columnDefs"    : [
                                        { targets: [0], orderable: true},
                                        { targets: '_all', orderable: false }
                                      ],/*end columnDefs*/
                    error:function(err, status){
                        alert(err);
                        },

                } );
            }

            $('#table tbody').on( 'click', '.button-edit', function () 
            {
                record_id = $(this).attr('record-id');
                document.location="{{$URL['edit']}}/"+record_id;

            });

            $('#table tbody').on( 'click', '.button-destroy', function () 
            {
                record_id = $(this).attr('record-id');
                record_name = $(this).attr('record-name');
                record_name = $(this).parent().parent().parent().parent().find(".record-name").html(); 


                Swal.fire({
                        title: 'Konfirmasi',
                        html: "Apakah  yakin  objek <b>"+record_name +"</b> dihapus?",
                        type: 'warning',
                        onOpen: () => {
                            var zippi = new Audio('{{ BASE_URL }}assets/sound/warning.mp3')
                            zippi.play();
                        },
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, dihapus saja',
                        cancelButtonText: 'Batal'
                    }).then((result) => {
                        if (result.value) 
                        {

                            {{core\Template::ajax($js_ajax)}}
                        }
                });

            });
        } );

        
    </script>
{% endblock %}