<?php

        
    /**
     * -------------------BEGIN CONFIG-------------------
     */ 

    /**
     * judul halaman
     */ 
    $main_title = "Theme User";


    /**
     * URL 
     */ 
    $URL =  [
                "store"    => "/user/theme",//alamat memasukan data
                "index"    => "/user/theme",//jika sudah berhasil, maka halaman akan dialihkan
            ];
    $swal_success   =   [
                            "type"      => "success", //success atau error
                            "message"   => "data berhasil dimasukan",//string boleh berupa tag html
                            "onclose"   => "document.location='".$URL['index']."';",//harus diakhiri dengan semicolon ;
                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                        ];
    $swal_error     =   [
                            "type"      => "error", //success atau error
                            "message"   => "msg.desc",
                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                        ];
    $js_ajax        =   [
                            "url"       => "'{$URL['store']}'",//url untuk store data
                            "data"      => '$("#form-create").serialize()',//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert($swal_success),
                            "onerror"   => core\Template::sweetalert($swal_error),
                        ];

    
?> 
{% extends themes/template %}

{% block main-title %}
    {{$main_title}}
{% endblock %}

{% block main-button %}

    <button id='button-back' class='  btn btn-warning  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-caret-left'></i>
        </span>
        Kembali
    </button>
    <button id='button-store' class='btn btn-success  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-plus'></i>
        </span>
        Tambah 
    </button>   
{% endblock %}


{% block css %}
{% endblock %}

{% block javascript %}
{% endblock %}

{% block main-content %}

    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>
                <div class='row'>

                    <!--mulai  content-->
                    <div class='col-md-12'>

                        <!--mulai main-card -->
                        <div class='card'>
                            <div class='card-header'>
                                <div class='card-title'>Form Tambah</div>
                            </div>
                            <div class='card-body'>

                                <!--mulai table -->
                                <div >

                                    <form id='form-create' method="post">

                                        <div >
                                            <div class='row'>
                                                <div class='col-md-6'>
                                                    <button type='button' class='button-theme btn btn-lg btn-primary btn-block' record-id='light'>TERANG</button>
                                                       
                                                </div>
                                                <div class='col-md-6'>
                                                    <button type='button' class='button-theme btn btn-lg btn-dark btn-block' record-id='dark'>GELAP</button>
                                                       
                                                </div>
                                                
                                                
                                                
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                    </form>
                                </div>
                                <!--selesai table -->


                            </div>
                        </div>
                        <!--selesai main-card -->
                    </div>
                    <!--selesai  content-->

                </div>
            </div>
        </div>
    </div>
{% endblock %}