<?php

        
    /**
     * BEGIN Config
     */ 

    /**
     * judul halaman
     */ 
    $main_title = "User";


    /**
     * URL datatable beserta button
     */ 
    $URL =  [
                "index"         => "/user",
                "list"          => "/user/list",
                "create"        => "/user/create",
                "edit"          => "/user/edit",
                "destroy"       => "/user/destroy",
                "active"        => "/user/active",
                "block"         => "/user/block",
            ];
        
    /**
     * kolom data yang ditampilkan
     */ 
    $AJAX_FIELD = ["full_name","role","status","button"];


    /**
     * searching / filtering jquery id input
     * key => value dimana:
     *      key = nama kolom sql. boleh di edit di modelnya
     *      value = id input untuk jquery
     */ 
    $AJAX_INPUT =   [
                        "full_name"     => "nama",
                        "status"        => "status",
                    ];


    $component_status =  [
                            "id" => 'status',
                            "name" => 'status',
                            "class" => ' select2  ',
                            "data" =>   [
                                            "" => "Semua Status",
                                            "0" => "Belum Aktif",
                                            "1" => "Aktif",
                                            "2" => "Diblokir",
                                        ],
                            "selected" => "",
                        ];//end array status
    $component_table =  [
                            "id"        => "table",
                            "class"     => "table table-bordered table-head-bg-gray  table-hover",
                            "head"      =>  [
                                                ["title"     => "NAMA",         "style"     => "",],
                                                ["title"     => "HAK AKSES",    "style"     => "",],
                                                ["title"     => "STATUS",       "style"     => "",],
                                                ["title"     => "",             "style"     => "width:10px;",],
                                            ],//end head
                            "selection" =>  [
                                                "<div class='form-group'> <input id='nama' type='text' class='form-control'  ></div>",
                                                "",
                                                core\Form::select($component_status),

                                                "",
                                            ],
                        ];//end table



    $swal_destroy_success   =   [
                            "type"      => "success", //success atau error
                            "message"   => "data berhasil dihapus",//string boleh berupa tag html
                            "onclose"   => "document.location='".$URL['index']."';",//harus diakhiri dengan semicolon ;
                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                        ];
    $swal_destroy_error     =   [
                            "type"      => "error", //success atau error
                            "message"   => "msg.desc",
                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                        ];
    $js_ajax        =   [
                            "url"       => "'{$URL['destroy']}/'+record_id",//url untuk store data
                            "data"      => "''",//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert($swal_destroy_success),
                            "onerror"   => core\Template::sweetalert($swal_destroy_error),
                        ];

    $js_ajax_block  =   [
                            "url"       => "'{$URL['block']}/'+record_id",//url untuk store data
                            "data"      => "''",//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert(   [
                                                                            "type"      => "success", //success atau error
                                                                            "message"   => "data berhasil diblok",//string boleh berupa tag html
                                                                            "onclose"   => "datatablesDraw();",//harus diakhiri dengan semicolon ;
                                                                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                                                                        ]),
                            "onerror"   => core\Template::sweetalert(   [
                                                                            "type"      => "error", //success atau error
                                                                            "message"   => "msg.desc",
                                                                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                                                                        ]),
                        ];

    $js_ajax_active =   [
                            "url"       => "'{$URL['active']}/'+record_id",//url untuk store data
                            "data"      => "''",//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert(   [
                                                                            "type"      => "success", //success atau error
                                                                            "message"   => "data berhasil diaktifkan",//string boleh berupa tag html
                                                                            "onclose"   => "datatablesDraw();",//harus diakhiri dengan semicolon ;
                                                                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                                                                        ]),
                            "onerror"   => core\Template::sweetalert(   [
                                                                            "type"      => "error", //success atau error
                                                                            "message"   => "msg.desc",
                                                                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                                                                        ]),
                        ];

    /**
     * END Config
     */ 
    $string_js_column = "";
    $string_js_input = "";
    foreach ($AJAX_FIELD as $value ) 
    {
        $string_js_column .= "{'data': '{$value}' },";
    }
    foreach ($AJAX_INPUT as $key => $value) 
    {
        $string_js_input .= "'{$key}': $('#{$value}').val() ,";
    }


?> 

{% extends themes/template %}

{% block main-button %}
    <button id='button-create' class='btn btn-success  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-plus'></i>
        </span>
        Tambah 
    </button>
{% endblock %}
{% block main-title %}
    {{$main_title}}
{% endblock %}

{% block main-content %}
    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>
                <div class='row'>

                    <div class='col-md-12'>
                        <div class='card'>
                            <div class='card-header'>

                                <div class='card-category'>Export : 
                                    <button id='button-csv' class="btn btn-info btn-xs">CSV</button>
                                    <button id='button-pdf' class="btn btn-info btn-xs">PDF</button>
                                </div>
                            </div>
                            <div class='card-body'>
                                <!-- mulai table -->
                                <div >
                                    {{ core\Form::datatable($component_table) }}
                                    
                                </div>
                                <!-- end table -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{% block css %}
    <!-- Select2 -->
    <link href="{{ BASE_URL }}assets/js/plugin/select2/css/select2.min.css" rel="stylesheet">
{% endblock %}

{% block javascript %}
    <script src="{{ BASE_URL }}assets/js/plugin/select2/js/select2.full.min.js"></script>

    <script src='{{ BASE_URL }}assets/js/plugin/datatables/datatables.min.js'></script>

    <script >

        $(document).ready(function() 
        {        
            
            if($('#button-create').length)
            {
                $('#button-create').click(function() 
                {
                    document.location='{{$URL['create']}}';
                });
            };
            datatablesDraw();

            //jika input searching/filtering ditekan enter
            if($('input').length)
            {
                $('input').keypress(function(e) 
                {
                    if (e.which == 13)
                    {
                        datatablesDraw();
                        return false;
                    }
                });
            }
            //jika select2 berubah nilai
            if($('.select2').length)
            {
                $('.select2').select2();
                $('.select2').change(function () 
                {
                    datatablesDraw();
                });

            }


            function datatablesDraw()
            {

                if (  $.fn.DataTable.isDataTable( '#table' ) ) 
                {
                    $('#table').DataTable().destroy();
                }

                $('#table').DataTable( {
                    "processing"    : true,
                    "serverSide"    : true,    
                    "searching"     : false,  
                    'lengthMenu'    : [
                                        [5, 10, 25, 50, -1], 
                                        [5, 10, 25, 50, 'All']
                                      ],/*end lengthMenu*/
                    "ajax"          : 
                                        {
                                            "url"   : "{{$URL['list']}}",
                                            "type"  : "POST",
                                            "data"  : 
                                            {   
                                                {{$string_js_input}}

                                            }
                                        },/*end ajax*/
                    "columns"       : [ 
                                        {{$string_js_column}} 
                                      ],/*end columns*/
                    "order"         : [[0, 'asc']],/*end order*/
                    "columnDefs"    : [
                                        { targets: [0], orderable: true},
                                        { targets: '_all', orderable: false }
                                      ],/*end columnDefs*/
                    error:function(err, status){
                        alert(err);
                        },

                } );
            }

            $('#table tbody').on( 'click', '.button-edit', function () 
            {
                record_id = $(this).attr('record-id');
                document.location="{{$URL['edit']}}/"+record_id;

            });

            $('#table tbody').on( 'click', '.button-block', function () 
            {
                record_id = $(this).attr('record-id');
                {{core\Template::ajax($js_ajax_block)}}
            });

            $('#table tbody').on( 'click', '.button-active', function () 
            {
                record_id = $(this).attr('record-id');
                {{core\Template::ajax($js_ajax_active)}}
            });
            $('#table tbody').on( 'click', '.button-destroy', function () 
            {
                record_id = $(this).attr('record-id');
                record_name = $(this).attr('record-name');
                record_name = $(this).parent().parent().parent().parent().find(".record-name").html(); 

                Swal.fire({
                        title: 'Konfirmasi',
                        html: "Apakah  yakin  objek <b>"+record_name +"</b> dihapus?",
                        type: 'warning',
                        onOpen: () => {
                            var zippi = new Audio('{{ BASE_URL }}assets/sound/warning.mp3')
                            zippi.play();
                        },
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, dihapus saja',
                        cancelButtonText: 'Batal'
                    }).then((result) => {
                        if (result.value) 
                        {

                            {{core\Template::ajax($js_ajax)}}
                        }
                });

            });
        } );

        
    </script>
{% endblock %}