<?php

        
    /**
     * -------------------BEGIN CONFIG-------------------
     */ 

    /**
     * judul halaman
     */ 
    $main_title = "Tambah User";


    /**
     * URL 
     */ 
    $URL =  [
                "store"    => "/user/create",//alamat memasukan data
                "index"    => "/user",//jika sudah berhasil, maka halaman akan dialihkan
            ];
    $swal_success   =   [
                            "type"      => "success", //success atau error
                            "message"   => "data berhasil dimasukan",//string boleh berupa tag html
                            "onclose"   => "document.location='".$URL['index']."';",//harus diakhiri dengan semicolon ;
                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                        ];
    $swal_error     =   [
                            "type"      => "error", //success atau error
                            "message"   => "msg.desc",
                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                        ];
    $js_ajax        =   [
                            "url"       => "'{$URL['store']}'",//url untuk store data
                            "data"      => '$("#form-create").serialize()',//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert($swal_success),
                            "onerror"   => core\Template::sweetalert($swal_error),
                        ];

    /**
     * masukan configurasi komponen disini 
     */ 


    $controller = new core\Controller;
    $model = $controller->model("WilayahModel");

    $wilayah = $model->pluck();
    $component_wilayah =  [
                            "id" => 'wilayah',
                            "name" => 'wilayah',
                            "class" => ' select2 form-control ',
                            "data" =>   $wilayah,
                            "selected" => "",
                        ];//end array status

    $role = core\Role::getAllRole();
    $component_role =  [
                            "multiple" => 'multiple',
                            "id" => 'role',
                            "name" => 'role[]',
                            "class" => ' select2 form-control ',
                            "data" =>   $role,
                        ];//end array status

    /**
     * -------------------END CONFIG-------------------
     */ 
?> 
{% extends themes/template %}

{% block main-title %}
    {{$main_title}}
{% endblock %}

{% block main-button %}

    <button id='button-back' class='  btn btn-warning  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-caret-left'></i>
        </span>
        Kembali
    </button>
    <button id='button-store' class='btn btn-success  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-plus'></i>
        </span>
        Tambah 
    </button>   
{% endblock %}


{% block css %}
    <!-- Select2 -->
    <link href="{{ BASE_URL }}assets/js/plugin/select2/css/select2.min.css" rel="stylesheet">
{% endblock %}

{% block javascript %}
    <script src="{{ BASE_URL }}assets/js/plugin/select2/js/select2.full.min.js"></script>
    <script >
        $(document).ready(function() 
        {
            if($('.select2').length)
            {
                $('.select2').select2();
            };
            if($('#button-back').length)
            {
                $('#button-back').click(function() 
                {
                    document.location="{{$URL['index']}}";
                });
            };
            if($('#button-store').length)
            {
                $('#button-store').click(function() 
                {
                    {{core\Template::ajax($js_ajax)}}
                });
            };

        });
    </script>
{% endblock %}

{% block main-content %}

    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>
                <div class='row'>

                    <!--mulai  content-->
                    <div class='col-md-12'>

                        <!--mulai main-card -->
                        <div class='card'>
                            <div class='card-header'>
                                <div class='card-title'>Form Tambah</div>
                            </div>
                            <div class='card-body'>

                                <!--mulai table -->
                                <div >

                                    <form id='form-create' method="post">

                                        <div >
                                            <div class='box-body'>
                                                <div class='row'>

                                                    <div class='col-md-4'>
                                                        <div class='input-group mb-6'>
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text" >
                                                                        NIP
                                                                    </span>
                                                            </div>
                                                        <input name='nip' type='text' class='form-control'   >
                                                        </div>
                                                    </div>
                                                    <div class='col-md-4'>
                                                        <div class='input-group mb-6'>
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text" >
                                                                        USERNAME
                                                                    </span>
                                                            </div>
                                                        <input name='username' type='text' class='form-control'   >
                                                        </div>
                                                    </div>

                                                    <div class='col-md-4'>
                                                        <div class='input-group mb-6'>
                                                            <div class="input-group-prepend">

                                                                <span class="input-group-text" >
                                                                    EMAIL
                                                                </span>
                                                            </div>
                                                            <input name='email' type='text' class='form-control'  >
                                                        </div>
                                                    </div>


                                                    <div class='col-md-12'>
                                                        <div class=" separator-dashed"></div>
                                                    </div>
                                                    <div class='col-md-6'>
                                                        <div class='input-group mb-6'>
                                                            <div class="input-group-prepend">

                                                                <span class="input-group-text" >
                                                                    PASSWORD
                                                                </span>
                                                            </div>
                                                            <input name='password' type='text' class='form-control'  >
                                                        </div>
                                                    </div>

                                                    <div class='col-md-6'>
                                                        <div class='input-group mb-6'>
                                                            <div class="input-group-prepend">

                                                                <span class="input-group-text" >
                                                                    KONFIRMASI-PASSWORD
                                                                </span>
                                                            </div>
                                                            <input name='password-confirm' type='text' class='form-control'  >
                                                        </div>
                                                    </div>

                                                    <div class='col-md-12'>
                                                        <div class=" separator-dashed"></div>
                                                    </div>

                                                    <div class='col-md-6'>
                                                        <div class="form-group">
                                                            <label>Nama Lengkap</label>
                                                            <div class='input-group mb-3'>
                                                                <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            <i class="fa fas fa-text-width"></i>
                                                                        </span>
                                                                </div>
                                                                <input name='full-name' type='text' class='form-control' name='full-name' placeholder='nama' >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class='col-md-6'>
                                                        <div class="form-group">
                                                            <label>Wilayah Kerja</label>
                                                            <div class='input-group mb-3'>
                                                                <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            <i class="fa fas fa-home"></i>
                                                                        </span>
                                                                </div>
                                                                {{ core\Form::select($component_wilayah) }}
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class='col-md-12'>
                                                        <div class="form-group">
                                                            <label>Hak Akses</label>

                                                            <div class='input-group mb-3'>
                                                                <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            <i class="fa fas fa-user"></i>
                                                                        </span>
                                                                </div>
                                                                {{ core\Form::select($component_role) }}

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </form>
                                </div>
                                <!--selesai table -->


                            </div>
                        </div>
                        <!--selesai main-card -->
                    </div>
                    <!--selesai  content-->

                </div>
            </div>
        </div>
    </div>
{% endblock %}