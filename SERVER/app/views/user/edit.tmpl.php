<?php
        
    /**
     * -------------------BEGIN CONFIG-------------------
     */ 

    /**
     * judul halaman
     */ 
    $main_title = "Ubah User";


    /**
     * URL 
     */ 
    $URL =  [
                "edit"    => "/user/update",//alamat mengupdate data
                "index"    => "/user",//jika sudah berhasil, maka halaman akan dialihkan
            ];
    $swal_success   =   [
                            "type"      => "success", //success atau error
                            "message"   => "data berhasil diubah",//string boleh berupa tag html
                            "onclose"   => "document.location='".$URL['index']."';",//harus diakhiri dengan semicolon ;
                            "timer"      => "1200",//jika otomatis close maka harus ada variable timer
                        ];
    $swal_error     =   [
                            "type"      => "error", //success atau error
                            "message"   => "msg.desc",
                            "onclose"   => "",//jika kosong tidak dikasih semicolon ;
                        ];
    $js_ajax        =   [
                            "url"       => "'{$URL['edit']}'",//url untuk store data
                            "data"      => '$("#form-edit").serialize()',//form data input
                            "debug"     => false, //default false.jika false mendesable dataType:json dan mengaktifkan alert msg. jika aktif sebaliknya
                            "onsuccess" => core\Template::sweetalert($swal_success),
                            "onerror"   => core\Template::sweetalert($swal_error),
                        ];

    /**
     * masukan configurasi komponen disini 
     */ 

    $controller = new core\Controller;
    $model = $controller->model("WilayahModel");

    $wilayah = $model->pluck();
    $component_wilayah =  [
                            "id" => 'wilayah',
                            "name" => 'wilayah',
                            "class" => ' select2 form-control ',
                            "data" =>   $wilayah,
                            "selected" => $data['wilayah'],
                        ];//end array status

    $role = core\Role::getAllRole();

    $user_role = core\Role::getUserRole($data['id']);
    $selected_role = [];
    foreach ($user_role as $key => $value) 
    {
        $selected_role[$key] = $key;
    }
    $component_role =  [
                            "multiple" => 'multiple',
                            "id" => 'role',
                            "name" => 'role[]',
                            "class" => ' select2 form-control ',
                            "data" =>   $role,
                            "selected" => $selected_role,
                        ];//end array status
    /**
     * -------------------END CONFIG-------------------
     */ 
?> 
{% extends themes/template %}

{% block main-title %}
    {{$main_title}}
{% endblock %}

{% block main-button %}

    <button id='button-back' class='  btn btn-warning  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-caret-left'></i>
        </span>
        Kembali
    </button>
    <button id='button-update' class='btn btn-success  btn-round'>
        <span class='btn-label'>
            <i class='fa fas fa-save'></i>
        </span>
        Simpan 
    </button>   
{% endblock %}


{% block css %}
    <!-- Select2 -->
    <link href="{{ BASE_URL }}assets/js/plugin/select2/css/select2.min.css" rel="stylesheet">
{% endblock %}

{% block javascript %}
    <script src="{{ BASE_URL }}assets/js/plugin/select2/js/select2.full.min.js"></script>
    <script >
        $(document).ready(function() 
        {
            if($('.select2').length)
            {
                $('.select2').select2();
            };
            if($('#button-back').length)
            {
                $('#button-back').click(function() 
                {
                    document.location="{{$URL['index']}}";
                });
            };
            if($('#button-update').length)
            {
                $('#button-update').click(function() 
                {
                    {{core\Template::ajax($js_ajax)}}
                });
            };

        });
    </script>
{% endblock %}

{% block main-content %}

    <div class='main-panel'>
        <div class='content'>
            <div class='page-inner'>
                <div class='row'>

                    <!--mulai  content-->
                    <div class='col-md-12'>

                        <!--mulai main-card -->
                        <div class='card'>
                            <div class='card-header'>
                                <div class='card-title'>Form Edit</div>
                            </div>
                            <div class='card-body'>

                                <!--mulai table -->
                                <div >

                                    <form id='form-edit' method="post" action="/user/update">
                                        <input type="hidden" name="id" value="<?= $data['id']?>"> 

                                        <div >
                                            <div class='box-body'>
                                                <div class='row'>

                                                    <div class='col-md-4'>
                                                        <div class='input-group mb-6'>
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text" >
                                                                        NIP
                                                                    </span>
                                                            </div>
                                                        <input type='text' class='form-control'  disabled value="<?= $data['nip']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class='col-md-4'>
                                                        <div class='input-group mb-6'>
                                                            <div class="input-group-prepend">
                                                                    <span class="input-group-text" >
                                                                        USERNAME
                                                                    </span>
                                                            </div>
                                                        <input type='text' class='form-control'  disabled value="<?= $data['name']; ?>">
                                                        </div>
                                                    </div>

                                                    <div class='col-md-4'>
                                                        <div class='input-group mb-6'>
                                                            <div class="input-group-prepend">

                                                                <span class="input-group-text" >
                                                                    EMAIL
                                                                </span>
                                                            </div>
                                                            <input type='text' class='form-control'  disabled value="<?= $data['email']; ?>">
                                                        </div>
                                                    </div>

                                                    <div class='col-md-12'>
                                                        <div class=" separator-dashed"></div>
                                                    </div>

                                                    <div class='col-md-6'>
                                                        <div class="form-group">
                                                            <label>Nama Lengkap</label>
                                                            <div class='input-group mb-3'>
                                                                <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            <i class="fa fas fa-text-width"></i>
                                                                        </span>
                                                                </div>
                                                                <input type='text' class='form-control' name='full_name' placeholder='nama' value="<?= $data['full_name']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class='col-md-6'>
                                                        <div class="form-group">
                                                            <label>Wilayah Kerja</label>
                                                            <div class='input-group mb-3'>
                                                                <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            <i class="fa fas fa-home"></i>
                                                                        </span>
                                                                </div>
                                                                <?= core\Form::select($component_wilayah);?>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class='col-md-12'>
                                                        <div class="form-group">
                                                            <label>Hak Akses</label>

                                                            <div class='input-group mb-3'>
                                                                <div class="input-group-prepend">
                                                                        <span class="input-group-text">
                                                                            <i class="fa fas fa-user"></i>
                                                                        </span>
                                                                </div>
                                                                <?= core\Form::select($component_role);?>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- /.row -->
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </form>
                                </div>
                                <!--selesai table -->


                            </div>
                        </div>
                        <!--selesai main-card -->
                    </div>
                    <!--selesai  content-->

                </div>
            </div>
        </div>
    </div>

{% endblock %}