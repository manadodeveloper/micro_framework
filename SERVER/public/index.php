<?php
/**
 * TRIMS Framework
 *
 * @author 		basarudin
 * @copyright 	2020
 */
	session_start();
	 
    //mendapatkan variable BASE_ROOT dan BASE_URL
    require_once('../framework/init/server.php');

    //meload semua library di /system/core. lihat di /composer json
	require_once  BASE_ROOT.'/vendor/autoload.php';

    //routing berdasarkan querystring
    require_once(BASE_ROOT.'/app/routes/web.php');